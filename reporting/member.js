const Xls = require('../lib/xlsx')

function month_name(m) {
    let names = [ 'Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember']
    let i = Number(m)-1;
    return names[i];
}

function day_name(m) {
    let names = [ 'Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag' ];
    let i = Number(m);
    return names[i];
}

function createMemberJournal(db, year, output) {

    let template = __dirname + '/templates/member';
    let xls = new Xls(template, output);

    let overview = xls.worksheet('sheet1', 'Übersicht');
    var mergecells = [];

    overview.emit('HEADER', {
        title: xls.atom('Emma-POS Mitglieder: Jahresübersicht '+year)
    });
    let odd_row = true;
    let row = 9;
    var balance = 0;
    db.each('select id, name from customer order by name', [],
        (member) => {
            let debit = db.get("select sum(amount) as debit from coop_journal "+
                    " where amount>0 and strftime('%Y', date)=? and customer=?",
                    [ year, member.id ] 
            );
            debit = debit.debit || 0;
            let credit = db.get("select sum(amount) as credit from coop_journal "+
                    " where amount<0 and strftime('%Y', date)=? and customer=?",
                    [ year, member.id ] 
            );
            credit = credit.credit || 0;
            balance += debit;
            balance += credit;
            var s = odd_row ? 'ODD-ROW':'EVEN-ROW';
            odd_row = !odd_row;
            overview.emit(s, {
                name: xls.atom(member.name || member.id),
                debit: debit, 
                credit: credit, 
                balance: debit+credit,
                row: row++
            });

            let odd_month = true;
            db.each( "select distinct strftime('%m', date) as month from coop_journal " +
                     " where strftime('%Y', date)=? and customer=? order by month", [ year, member.id ] ,
                ( mrec ) => {
                    let mdebit = db.get("select sum(amount) as debit from coop_journal "+
                            " where amount>0 and strftime('%m', date)=? and strftime('%Y', date)=? and customer=?",
                            [ mrec.month, year, member.id ] 
                    );
                    mdebit = mdebit.debit || 0;
                    let mcredit = db.get("select sum(amount) as credit from coop_journal "+
                            " where amount<0 and strftime('%m', date)=? and strftime('%Y', date)=? and customer=?",
                            [ mrec.month, year, member.id ] 
                    );
                    mcredit = mcredit.credit || 0;
                    var s = odd_month ? 'ODD-MONTH':'EVEN-MONTH';
                    odd_month = !odd_month;
                    overview.emit(s, {
                        month: xls.atom( month_name(mrec.month)+' '),
                        debit: mdebit, 
                        credit: mcredit,
                        row: row++
                    });
        
                }, 
                () => { 
                    // nothing to do
                });
        }, () => {
            overview.emit('SUMMARY', {
                balance: balance, row: row++
            });
            overview.emit('FOOTER');
            overview.finish();
        }
    );

    db.each('select id, name from customer order by name', [],
        (member) => {
            var sheet = xls.worksheet('sheet2', member.name||member.id);
            let balance = db.get("select sum(amount) as balance from coop_journal "+
                    " where strftime('%Y', date)=? and customer=? ",
                    [ year, member.id ] 
            );
            balance.balance = balance.balance || 0;
            sheet.emit('HEADER', {
                title: xls.atom('Jahresübersicht '+(member.name||member.id)+' '+year),
                balance: balance.balance
            });
            mergecells = [];
            mergecells.push('<mergeCell ref="G7:H7"/>');

            row=8; 
            odd_month=false;

            db.each("select distinct strftime('%m', date) as month from coop_journal " +
                    "  where strftime('%Y', date)=? and customer=? " +
                    "  order by month ",
                [ year, member.id ],
                (mrec) => {
                    let s = odd_month ? "ODD-MONTH":"EVEN-MONTH";
                    odd_month = !odd_month;

                    let mcredit = db.get(
                        "select sum(amount) as v from coop_journal" +
                        " where amount<0 and strftime('%m', date)=? and strftime('%Y', date)=? and customer=?", 
                        [ mrec.month, year, member.id ]);
                    mcredit = mcredit.v || 0;

                    let mdebit = db.get(
                        "select sum(amount) as v from coop_journal" +
                        " where amount>0 and strftime('%m', date)=? and strftime('%Y', date)=? and customer=?", 
                        [ mrec.month, year, member.id ]);
                    mdebit = mdebit.v || 0;
                    mergecells.push('<mergeCell ref="G'+row+':H'+row+'"/>');
                    sheet.emit(s, {
                        month: xls.atom( month_name(mrec.month) ),
                        credit: mcredit,
                        debit: mdebit,
                        row: row++
                    });

                    odd_row=false;
                    db.each("select * from coop_journal "+
                            " where strftime('%m', date)=? and strftime('%Y', date)=? and customer=? " +
                            " order by date", 
                        [ mrec.month, year, member.id ],
                        (rec) => {
                            let s = odd_row ? "ODD-ROW":"EVEN-ROW";
                            odd_row = !odd_row;

                            mergecells.push('<mergeCell ref="E'+row+':H'+row+'"/>');
                            sheet.emit(s, {
                                date: xls.date(rec.date),
                                credit: rec.amount<0 ? rec.amount:'',
                                debit: rec.amount>=0 ? rec.amount:'',
                                subject: xls.atom(rec.description||''),
                                row: row++
                            });
                        },
                        () => {
                        }
                    );
                },
                () => {
                    sheet.emit('FOOTER', {
                        mergecount: mergecells.length,
                        mergecells: mergecells.join('')
                    });
                    sheet.finish();        
                }
            );
        }, 
        () => {
            xls.finish();
        }
    );    
}


module.exports = createMemberJournal;
