
const Xls = require('../lib/xlsx')


function month_name(m) {
    let names = [ 'Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember']
    let i = Number(m)-1;
    return names[i];
}

function day_name(m) {
    let names = [ 'Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag' ];
    let i = Number(m);
    return names[i];
}

const debug_level = 10;

function createCartJournal(db, year, output) {
    let template = __dirname + '/templates/verkaeufe';
    let xls = new Xls(template, output);

    overview_page();

    db.each("select distinct strftime('%m', date) as month from shop_carts " +
            " where strftime('%Y', date)=? "+
            " order by month", [ year ],
        (month) => {
            monthly_page(month.month);
        }, () => {
        }
    );

    function overview_page() {
        let overview = xls.worksheet('sheet1', 'Jahresübersicht');
        overview.emit('HEADER', {
            title: xls.atom('Emma-POS Verkäufe: Jahresübersicht '+ year)
        });

        let row = 9;
        let total = 0;
        let odd_row = true;
        db.each("select distinct strftime('%m', date) as month from shop_carts " + 
                " where strftime('%Y', date)=? order by month", 
            [ year ], 
            (month) => {
                var sum=0, cash=0, pp=0;
                db.each("SELECT total, payment from shop_carts " + 
                        " where strftime('%Y', date)=? and strftime('%m', date)=?", [ year, month.month ],
                    (rec) => {
                        total += rec.total;
                        sum += rec.total;
                        if (rec.payment=='prepaid')
                            pp+= rec.total;
                        else if (rec.payment=='cash')
                            cash += rec.total;
                    }, () => {
                        let section = odd_row ? 'ODD-MONTH' : 'EVEN-MONTH';
                        odd_row = !odd_row;
                        overview.emit(section, {
                            month: xls.atom( month_name(month.month) ),
                            cash: cash, prepaid: pp, sum: sum,
                            row: row++
                        });
                    }
                );
            }, ()=>{
                overview.emit('MONTH-FOOTER', {
                    row: row++, 
                    sum: total
                })
            }
        );

        row += 3;
        overview.emit('WEEK-HEADER', {
            row: row++, row3: row+2
        });

        row +=3;
        total = 0;
        odd_row = true;
        db.each("select distinct strftime('%W', date) as week from shop_carts " +
                " where strftime('%Y', date)=? order by week", 
                [ year ], 
            (week) => {
                var sum=0, cash=0, pp=0;
                db.each("SELECT total, payment from shop_carts " + 
                        " where strftime('%Y', date)=? and strftime('%W', date)=?", [ year, week.week ],
                    (rec) => {
                        total += rec.total;
                        sum += rec.total;
                        if (rec.payment=='prepaid')
                            pp+= rec.total;
                        else if (rec.payment=='cash')
                            cash += rec.total;
                    }, () => {
                        let section = odd_row ? 'ODD-WEEK' : 'EVEN-WEEK';
                        odd_row = !odd_row;
                        overview.emit(section, {
                            week: xls.atom( 'WE'+(Number(week.week)+1) ),
                            cash: cash, prepaid: pp, sum: sum,
                            row: row++
                        });

                        var odd_day = true;
                        db.each("SELECT distinct strftime('%w', date) as day from shop_carts " + 
                            " where strftime('%Y', date)=? and strftime('%W', date)=? order by day", 
                            [ year, week.week ],
                            (day) => {
                                var dsum=0, dcash=0, dpp=0;
                                db.each("SELECT total, payment from shop_carts " + 
                                        " where strftime('%Y', date)=? and strftime('%W', date)=? " +
                                        " and strftime('%w', date)=?", 
                                        [ year, week.week, day.day ],
                                    (rec) => {
                                        dsum += rec.total;
                                        if (rec.payment=='prepaid')
                                            dpp+= rec.total;
                                        else if (rec.payment=='cash')
                                            dcash += rec.total;
                                    }, () => {
                                        let section = odd_row ? 'ODD-WEEK' : 'EVEN-WEEK';
                                        if (odd_day) section += '-ODD-DAY';
                                        else section += '-EVEN-DAY';
                                        odd_day = !odd_day;
                                        overview.emit(section, {
                                            day: xls.atom( day_name(day.day) ),
                                            cash: dcash, prepaid: dpp, sum: dsum,
                                            row: row++
                                        });
                                    }
                                );
                            }, () => {
                                // nothing to do
                            }
                        );
                    }
                );
            }, ()=>{
                overview.emit('MONTH-FOOTER', {
                    row: row++, 
                    sum: total
                })
            }
        );
        overview.emit('FOOTER');
        overview.finish();
    }

    function monthly_page(month) {
        let monthly = xls.worksheet('sheet2', month_name(month));
        let cash = db.get("select sum(total) as sum from shop_carts "+
                            " where strftime('%Y', date)=? and strftime('%m', date)=? " +
                            " and payment='cash'", [year, month ]
        );
        let pp = db.get("select sum(total) as sum from shop_carts "+
                            " where strftime('%Y', date)=? and strftime('%m', date)=? " +
                            " and payment='prepaid'", [year, month ]
        );
        monthly.emit('HEADER', {
            title: xls.atom(month_name(month) + ' ' + year),
            subtitle: xls.atom('Warenkörbe im Monat ' + month_name(month)),
            pp: pp.sum, cash: cash.sum
        });

        var row = 8;
        let odd_row = true;
        var mergecells = [ '<mergeCell ref="G7:I7"/>' ];

        db.each("select distinct strftime('%d', date) as day from shop_carts " +
                " where strftime('%Y', date)=? and strftime('%m', date)=? " +
                " order by day",
            [ year, month ],
            (dayrec) => {
                let sum = 0, cash = 0, pp = 0; 
                db.each("select total, payment from shop_carts " +
                        " where strftime('%Y', date)=? and strftime('%m',date)=? and strftime('%d',date)=? ",
                    [ year, month, dayrec.day ],
                    (rec) => {
                        sum += rec.total;
                        if (rec.payment == 'cash')
                            cash += rec.total;
                        else if (rec.payment == 'prepaid')
                            pp += rec.total;
                    },
                    () => {
                        let section = odd_row ? 'ODD-DAY' : 'EVEN-DAY';
                        odd_row = !odd_row;
                        let datestr = year+'-'+month+'-'+dayrec.day+'T00:00:00Z'
                        mergecells.push('<mergeCell ref="G'+row+':I'+row+'"/>');
                        monthly.emit(section, {
                            row: row++,
                            subject: '', // xls.atom(datestr + ' ' + new Date(datestr).toString()), 
                            date: xls.date(datestr),
                            cash: cash>0?cash:'', prepaid: pp>0?pp:'', sum: sum>0?sum:''
                        });
                    }
                );

                db.each("select id, date, total, payment, customer from shop_carts " +
                        " where strftime('%Y', date)=? and strftime('%m',date)=? and strftime('%d',date)=? " +
                        " order by date",
                    [ year, month, dayrec.day ],
                    (cart) => {
                        var s = false, c = '', p = '';
                        if (cart.payment=='cash') {
                            s='Bareinkauf';
                            c=cart.total;
                        }
                        if (cart.payment=='prepaid') {
                            s='Prepaid-Einkauf '+cart.customer;
                            p=cart.total;                            
                        }
                        if (s) {
                            mergecells.push('<mergeCell ref="G'+row+':I'+row+'"/>');
                            monthly.emit('CART', {
                                time: xls.date(cart.date), 
                                cash: c, prepaid: p,
                                //subject: xls.atom(cart.date.toString()), 
                                subject: xls.atom(s),
                                date: xls.atom(cart.date.toString()),
                                row: row++
                            });    
                            
                            monthly.emit('CART-ITEM-HEADER', {
                                row: row++
                            });
                            var odd_item = true;
                            db.each("select name, amount, price from shop_cartitems " +
                                        " where cart=?", [ cart.id],
                                (cart_item) => { 
                                    let section = odd_item ? 'ODD-CART-ITEM':'EVEN-CART-ITEM';
                                    odd_item = !odd_item;
                                    mergecells.push('<mergeCell ref="D'+row+':F'+row+'"/>');
                                    monthly.emit(section, {
                                        row: row++,
                                        product: xls.atom(cart_item.name),
                                        amount: cart_item.amount,
                                        price: cart_item.price,
                                        total: cart_item.amount * cart_item.price
                                    });
                                }, () => {}
                            );
                        }
                    },
                    () => {}
                );
            },
            () => {}
        );


        monthly.emit('FOOTER', {
            mergecount: mergecells.length,
            mergecells: mergecells.join('')
        });
        monthly.finish();
    }



    xls.finish();
}

module.exports = createCartJournal;

