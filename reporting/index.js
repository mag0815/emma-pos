#!/usr/bin/env node

const sqlite = require('../lib/sqlite3-sync');
const Xlsx = require('../lib/xlsx');

const createCartJournal = require('./carts');
const createMemberJournal = require('././member');

let db=new sqlite(__dirname + '/../db/catalog.sqlite');
let outdir = __dirname + '/../reports/';

db.each("select distinct strftime('%Y', date) as year from shop_carts", [],
    (rec) => {
        createCartJournal(db, rec.year, outdir + 'Verkaeufe-' + rec.year +'.xlsx');
        createMemberJournal(db, rec.year, outdir + 'Mitglieder-' + rec.year +'.xlsx');
    },
    () => {});

