const path = require('path');
const https = require('https');
const fs=require('fs');
const util = require('./lib/util');

const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cookieparser = require('cookie-parser');
const fileupload = require('express-fileupload');

require('./lib/express-install-routes');

const model = require ('./model');

var app = express();

app.set('views', __dirname + '/views');
app.set('view engine', 'pug');

app.run = function(config, callback) {
    config=config || {};
    config.tls = config.tls || {};
    config.model = config.model || {};
    config.develop = config.develop || {};

    app.config=config;

    // app.use(logger('dev'));

    // use cookies
    app.use(cookieparser());

    // parse POST requests as urlencoded data
    app.use(bodyParser.urlencoded({ extended: true }));

    // enable http file upload
    app.use(fileupload({
        limits: { fileSize: 50 * 1024 * 1024 },
      }));

    // use on-the-fly scsc compiler
    if (config.develop && config.develop.compile_scss) {
        const sass = require('@gompa/node-sass-middleware');
        console.info('using on-the-fly scss compiler')
        app.use(sass({
            src: path.join(__dirname, 'sass'),
            dest: path.join(__dirname, 'public_html/css'),
            debug: false,
            outputStyle: 'compressed',
            sourceMap: true,
            prefix:  '/css'  // Where prefix is at <link rel="stylesheets" href="css/style.css"/>
        }));
    }

    // parse optional GET payload, check user credentials and ninja-patching of response.render
    app.use( (request, response, next) => {

        response.old_render = response.render;
        response.render = function(file, vars) {

            vars.t = request.t;
            vars.lang = request.lang;
            vars.theme = request.theme;
            vars.customer = request.customer;
            vars.old_vars = response.old_vars||'';
            response.old_vars=vars;

            vars.vars = JSON.stringify({ query:request.query, vars: vars, cookies: request.cookies });

            this.old_render(file, vars);
        }

        if (request.query.payload) {
            try {
                var payload=JSON.parse(new Buffer(request.query.payload,'base64').toString());
                request.query.payload = payload;
            } catch(error) {

            }
        } else request.query.payload={};

        var pin = request.cookies.adminpin;
        request.customer = {
            id: 'anonymous', 
            name: 'anonymous', 
            balance: 0, 
            roles: ['anonymous'] 
        }
        if (pin) {
            model.coop.get_customer_by_pin(pin, (err, customer) => {
                if (err) return next(new Error(err));
                request.customer=customer;
                next();
            });    
        } else {
            request.cookies.adminpin = '';
            next();
        }
    });

    // on-the-fly translation of pug text fragments
    app.use(require('./i18n').translate_pug);

    // install appilcation routes
    app.use('/', require('./routes'));

    // serve static content
    app.use('/reports', express.static(path.join(__dirname, 'reports')));
    app.use('/reports', require("serve-index")(path.join(__dirname, 'reports'), {
        stylesheet: path.join(__dirname, 'public_html/css/dir-index.css'),
        template: function (locals, callback) {
            let markup = '';
            markup += '<html><head><meta charset="utf-8"> <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />';
            markup += '<title>listing directory ' + locals.directory + '</title>';
            markup += '<style>' + locals.style + '</style>';
            markup += '</head><body class="directory"><div id="wrapper">';
              
            let ip;
            try {
                let exec = require('./lib/node-exec-sync');
                let cmd = "ifconfig wlan0|grep inet|head -1|sed 's/[ ][ ]*/\\t/g'|cut -f3";
                ip = exec(cmd).trim();    
            } catch (e) {
                ip = "localhost";
            }

            markup += '<h1>http://' + ip +':' + config.port + locals.directory + '</h1><ul>';

            for (let n=0; n<locals.fileList.length; ++n) {
                let e = locals.fileList[n];
                markup += '<li><a href="' + e.name +'">';
                markup += '<span class="filename">' + e.name + '</span>';
                if (e.stat.isDirectory()) {
                    markup += '<span class="diritem">&lt;DIR&gt;</span>'
                } else {
                    markup += '<span class="filesize">' + e.stat.size + '</span>';
                    markup += '<span class="filedate">' + e.stat.mtime.toLocaleDateString() + '</span>';
                }
                markup += '</a></li>';
            }

            markup += '</ul></div></body></html>';
            callback(null, markup);
        }
    }));
    app.use('/', express.static(path.join(__dirname, 'public_html')));


    // custom error-page
    app.use((error, request, response, next) => {
        vars = {
            title: 'internal error',
            buttons: [
                {},
                {},
                {},
                {},
                {   title: 'Home',
                    icon: 'coop',
                    action: '/'
                }
            ],
            error: {
                message: error.message,
                path: error.path,
                stack: error.stack
            }
        };
        response.render('error-page', vars);
    });

    // connect to database an start http/htps servers
    model.connect(config,  (err) => {
        app.listen(config.port || 8080);
        console.info('server running on port ' + app.config.port);
    
        https.createServer({
            key: fs.readFileSync(config.tls.key || 'db/key.pem'),
            cert: fs.readFileSync(config.tls.cert || 'db/cert.pem')
          }, app).listen(config.tls.port || 4343);
          console.info('ssl server running on port ' + config.tls.port || 4343);

        callback && callback(null);    
    })

}

module.exports = app;
