#### v0.0.3

This is a pre-beta release of the new Point of Sale System. Therefore, 
a lot of things wont work as expected, lots of stuff is unfinished or
inconsistent.

You may leave any remarks or ideas as a message (the lower left button), or talk
directly to Mario.
