let model = require('.')

const { insert } = require("./sqlite");


function sqlbatch() {
    this.batch = [];
}

sqlbatch.prototype.update = function(table, fields, condition, values) {
    this.batch.push({
        type: 'update',
        table: table, fields: fields,
        condition: condition, values: values
    });
}

sqlbatch.prototype.delete = function(table, condition, values) {
    this.batch.push({
        type: 'delete',
        table: table,
        condition: condition,
        values: values
    })
}

sqlbatch.prototype.sql = function(sql, values) {
    this.batch.push({
        type: 'sql',
        sql: sql, values: values
    });
}

sqlbatch.prototype.insert = function(table, values) {
    this.batch.push({
        type: 'insert', 
        table: table, values: values
    });
}

sqlbatch.prototype.execute = function(callback) {
    let n=0;
    let batch = this.batch;

    function next(err, result) {
        if (err) return callback(err);
        if (n<batch.length) {
            let job = batch[n++];
            if (job.type=='update')
                model.update(job.table, job.condition, job.values, job.fields, next);
            else if (job.type=='sql')
                model.sql(job.sql, job.values, next);
            else if (job.type=='delete') 
                model.remove(job.table, job.condition, job.values, next);
            else if (job.type=='insert')
                model.sql.insert(job.table, job.values, next);
            else {
                callback(Error("unknown sql batch type: "+job.type))
            }
        } else {
            callback(null);
        }
    }
    next(null);
}

module.exports = sqlbatch;
