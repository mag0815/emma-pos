const model=require('./index');


function get_customers(callback) {
    model.getAll(
        'customer',
        'true order by id', [],
        (err, result) => {
            if (err) return callback(err);
            callback(err, result);
        }
    );
}

function get_customer(id, callback) {
    model.getOne(
        'customer',
        'id=?', [ id ],
        (err, result) => {
            if (err) return callback(err);
            callback(err, result);
        }
    );
}

function update_customer(id, rec, callback) {
    //TODO:
}

function create_customer(id, rec, callback) {
    //TODO:
}


module.exports = {
    get_customers: get_customers,
    get_customer: get_customer,
    update_customer: update_customer,
    create_customer: create_customer,

    
};
