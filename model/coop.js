const uuid = require('uuid');

const model=require('./index');


function get_customer_by_pin(pin, callback) {
    model.getOne(
        'customer',
        'pin=?', [ pin ],
        (err, result) => {
            if (err) return callback(err);
            callback(err, result);
        }
    );
}

function get_customers(pager, callback) {
    let limit='';

    if (typeof pager=='function') {
        callback=pager; pager=undefined;
    }

    if (pager && pager.limit) {
        limit += ' limit '+pager.limit;
    } 
    if (pager && pager.skip) {
        limit += ' offset '+pager.skip;
    } 
    model.aggregate(
        "customer",
        { count: "count(*)"
        }, "is_special=0", [],
        (err, cnt) => {
            if (err)
                return callback(err);
            model.getAll(
                'customer',
                "is_special=0 order by name "+limit, [],
                (err, result) => {
                    if (err) return callback(err);
                    if (pager) {
                        pager.length = result.length;
                        pager.count = cnt.count;
                        pager.skip = Number(pager.skip||0);
                        pager.limit = Number(pager.limit);    
                    }
                    callback(err, result);
                }
            );        
        }
    );
}

function get_journal(pager, callback) {
    let limit='';

    if (typeof pager=='function') {
        callback=pager; pager=undefined;
    }

    if (pager && pager.limit) {
        limit += ' limit '+pager.limit;
    } 
    if (pager && pager.skip) {
        limit += ' offset '+pager.skip;
    } 
    model.aggregate(
        "coop_journal",
        { count: "count(*)"
        }, "", [],
        (err, cnt) => {
            if (err)
                return callback(err);
            model.getAll(
                'coop_journal',
                "true order by date "+limit, [],
                (err, result) => {
                    if (err) return callback(err);
                    if (pager) {
                        pager.length = result.length;
                        pager.count = cnt.count;
                        pager.skip = Number(pager.skip||0);
                        pager.limit = Number(pager.limit);    
                    }
                    callback(err, result);
                }
            );        
        }
    );
}

function get_customer(mid, callback) {
    model.getOne(
        'customer',
        'id=?', [ mid ],
        (err, result) => {
            if (err) return callback(err);
            callback(err, result);
        }
    );
}

function get_customer_saldo(mid, pager, callback) {
    model.getOne(
        'customer',
        'id=?', [ mid ],
        (err, customer) => {
            if (err) return callback(err);

            let limit='';
            if (pager && pager.limit) {
                limit += ' limit '+pager.limit;
            } 
            if (pager && pager.skip) {
                limit += ' offset '+pager.skip;
            } 

            model.aggregate(
                'coop_journal',
                { count: "count(*)" },
                "customer=?", [ mid ],
                (err, cnt) => {
                    if (err) return callback(err);

                    model.getAll(
                        'coop_journal',
                        "customer=? order by date desc "+limit, [ mid ],
                        (err, result) => {
                            if (err) return callback(err);

                            if (pager) {
                                pager.length = result.length;
                                pager.count = cnt.count;
                                pager.skip = Number(pager.skip||0);
                                pager.limit = Number(pager.limit);    
                            }                
                            callback(err, { customer: customer, journal: result});
                        }
                    );                
                }
            );
        }
    );
}

function overview(callback) {
    let values = {

    };
    model.shop.cash_amount( (err, amount) => {
        if (err) return callback(err);

        values.desk = amount;
        model.getAll(
            "customer",
            "true", [],
            (err, result) => {
                if (err) return callback(Error(err));

                let member_saldo = 0;
                let specials = [];

                for (let n=0; n<result.length; ++n) {
                    let customer = result[n];
                    switch(customer.id) {
                        case "emma-shop":
                            values.shop_saldo = customer.balance;
                            break;
                        case "cashdesk":
                            values.cashdesk = customer.balance;
                            break;
                        case "food-coop":
                            values.coop_saldo = customer.balance;
                            break;
                        default:
                            if (customer.is_special) {
                                specials.push({
                                    id: customer.id,
                                    name: customer.name,
                                    balance: customer.balance
                                });
                            } else {
                                member_saldo += customer.balance;
                            }
                    }
                }
                values.special_accounts = specials;
                values.member_saldo = member_saldo;

                model.shop.stock_value((err, stock) => {
                    if (err) return callbac(err);
                    
                    values.stock_value = stock;
                    callback(null, values);
                });
            }
        );
    });
}

function update_customer(cid, rec, callback) {
    model.update(
        "customer",
        "id=?", [ rec.id ],
        rec,
        (err, result) => {
            if (err) return callback(err);
            callback(err, result);
        }
    )
}

function create_customer(cid, rec, callback) {
    model.insert(
        "customer",
        rec, 
        (err) => {
            callback(err);
        }
    )
}

function add_to_journal_batched(sqlbatch, entry) {
    sqlbatch.insert("coop_journal", entry);
    sqlbatch.sql("update customer set balance=balance+? where id=?", [entry.amount, entry.customer]);
}

function add_to_journal(entry, callback) {
    model.insert(
        'coop_journal',
        entry,
        (err) => {
            if (err) 
                return callback(new Error(err));
            model.getOne(
                'customer',
                'id=?', [ entry.customer ],
                (err, customer) => {
                    if (err)
                        return callback(new Error(err));
                    customer.balance += entry.amount;
                    model.update(
                        'customer',
                        'id=?', [ entry.customer ],
                        customer,
                        (err) => {
                            if (err) 
                                return callback(new Error(err));
        
                            callback(err);
                        }
                    );
                }
            );
        }
    );
}

function create_message(message, callback) {
    model.insert(
        'messages',
        message,
        (err) => {
            if (err) 
                return callback(new Error(err));
            callback(err);
        }
    )
}

function update_message(message, callback) {
    model.update(
        'messages',
        'id=?', [ message.id],
        message,
        entry,
        (err) => {
            if (err) 
                return callback(new Error(err));
            callback(err);
        }
    )
}

function remove_message(mid, callback) {

}

function get_messages(from, count, callback) {
    model.aggregate(
        'messages',
        { count: 'count(*)' },
        'sticky=?', [ true ],
        (err, result) => {
            var sticky_from = from;

            var sticky_count = result.count - from;
            if (sticky_count<0)
                sticky_count=0;
            else if (sticky_count>count)
                sticky_count = count

            var msg_from=from - result.count;
            if (msg_from<0)
                msg_from=0;

            var msg_count=count - sticky_count;

            model.getAll(
                'messages',
                'sticky=? order by date desc limit ?,?', [ true, sticky_from, sticky_count ],
                (err, sticky) => {
                    if (err) 
                        return callback(new Error(err));
        
                    model.getAll(
                        'messages',
                        'sticky=? order by date desc limit ?,?', [ false, msg_from, msg_count ],
                        (err, msg) => {
                            if (err) 
                                return callback(new Error(err));
                            
                            var result=sticky.concat(msg);
                            callback(null, result);
                        }
                    );        
                }
            );        
        }
    );
}

function get_deliveries(pager, callback) {
    let limit='';

    if (typeof pager=='function') {
        callback=pager; pager=undefined;
    }

    if (pager && pager.limit) {
        limit += ' limit '+pager.limit;
    } 
    if (pager && pager.skip) {
        limit += ' offset '+pager.skip;
    } 
    model.aggregate(
        "delivery",
        { count: "count(*)"
        }, "", [],
        (err, cnt) => {
            if (err)
                return callback(err);
            model.getAll(
                'delivery',
                "true order by date desc "+limit, [],
                (err, result) => {
                    if (err) return callback(err);
                    if (pager) {
                        pager.length = result.length;
                        pager.count = cnt.count;
                        pager.skip = Number(pager.skip||0);
                        pager.limit = Number(pager.limit);    
                    }
                    callback(err, result);
                }
            );        
        }
    );
}

function create_delivery(callback) {
    let delivery = {
        id: uuid(),
        state: 'pending',
        clerk: ''
    };
    model.insert(
        "delivery", delivery,
        (err) => {
            callback(err, delivery);
        }
    );
}

function get_delivery(id, callback) {
    model.getOne(
        "delivery", "id=?", [id],
        (err, record) => {
            if (err) return callback(err);

            model.getAll(
                "delivery_item",
                "delivery=? order by rowid desc", [id],
                (err, items) => {
                    if (err) return callback(err);
                    callback(err, record, items);
                }
            );
        }
    );
}

function delivery_add_product(delivery_id, product_id, callback) {
    model.getOne(
        "shop_products", "id=?", [product_id],
        (err, product) => {
            if (err) return callback(Error(err))
            let item = {
                id: uuid(),
                delivery: delivery_id,
                product: product_id,
                name: product.name,
                old_stock: product.stock,
                new_stock: 0,
                old_price: product.price,
                new_price: product.price
            };
            model.insert(
                "delivery_item", item,
                (err) => {
                    if (err) return callback(Error(err))

                    model.sql(
                        "update delivery "+
                        "set total = (" +
                            "select sum(amount*new_price) from delivery_item where delivery=?"+
                        ") where id=?", [delivery_id,delivery_id],
                        (err) => {
                            if (err) return callback(Error(err))
                            return callback(err, item); 
                        }
                    );
                }
            )
        }
    )
}

function delivery_update(changeset, callback) {
    let batch = new model.sqlbatch();
    for (let item_id in changeset) {
        if (item_id == "remark") {
            batch.update("delivery", { remark: changeset.remark }, 'id=?', [changeset.id]);
        } else if (item_id != 'id') {
            batch.update("delivery_item", changeset[item_id], "id=?", [item_id]);
        }
    }
    
    batch.sql(
        "update delivery "+
        "set total = (" +
            "select sum(amount*new_price) from delivery_item where delivery=?"+
        ") where id=?", [changeset.id,changeset.id]
    );

    batch.execute(callback)
}

function delivery_finish(id, callback) {
    var batch = new model.sqlbatch();

    model.getAll(
        "delivery_item", "delivery=?", [id],
        (err, items) => {
            if (err) return callback(err);
            for(let n=0; n<items.length; ++n) {
                let item = items[n];
                if (item.amount == 0 || item.amount===null || item.new_price==0 || item.new_price == null) {
                    batch.delete(
                        "delivery_item", "id=?", [item.id]
                    )
                } else {
                    batch.sql(
                        "update shop_products" +
                        " set stock=stock+?, price=?" +
                        " where id=?",
                        [ item.amount, item.new_price, item.product ]
                    )
                }   
            }

            model.getOne(
                "delivery", "id=?", [id],
                (err, delivery) => {
                    if (err) return callback(err);

                    batch.update("delivery", { state: 'booked' }, 'id=?', [id]);

                    batch.execute((err) => {
                        if (err) return callback(err);
                        
                        // charge emma-shop, since the goods are in stock now
                        add_to_journal({
                            amount: -delivery.total,
                            customer: 'emma-shop',
                            type: 'delivery',
                            vat: 0,
                            description: delivery.remark
                        }, callback);
                    });
                }
            )
        }
    )
}

module.exports = {
    overview: overview,
    get_customer_by_pin: get_customer_by_pin,
    get_customers: get_customers,
    get_customer: get_customer,
    get_customer_saldo: get_customer_saldo,
    create_customer: create_customer,
    update_customer: update_customer,
    
    add_to_journal: add_to_journal,

    create_message: create_message,
    remove_message: remove_message,
    update_message: update_message,
    get_messages: get_messages,

    get_journal: get_journal,

    get_deliveries: get_deliveries,
    create_delivery: create_delivery,
    get_delivery: get_delivery,
    delivery_add_product: delivery_add_product,
    delivery_update: delivery_update,
    delivery_finish: delivery_finish,

};
