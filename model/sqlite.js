const sqlite3 = require('sqlite3');

var db;

function connect_sqlite(config, callback) {

    var file = (config.model || {}).database || 'db/catalog.sqlite';
    db = new sqlite3.Database(file, (err) => {
        if (err) return callback(err);

        console.info('database connection established via sqlite3.');
        callback(err, module.exports);
    });
    
}

function run() {
    return db.run.apply(db, arguments);
}

function all() {
//    console.log(arguments)
    return db.all.apply(db, arguments);
}

function get() {
//    console.log(arguments)
    return db.get.apply(db, arguments);
}

function getAll(table, condition, args, callback) {
    var query='select rowid,* from ' + table;
    if (condition) {
        query += ' where ' + condition;
    } 
    all( query, args, callback);
}

function getOne(table, condition, args, callback) {
    var query='select rowid,* from ' + table;
    if (condition) {
        query += ' where ' + condition;
    } 
    get( query, args, callback);
}

function insert( table, fields, callback) {
    var keys=[], values=[], qmarks=[];
    for (var field in fields) {
        keys.push(field);
        qmarks.push('?');
        var v=fields[field];
        if (v!==null && v.constructor==Date)
            v=v.getTime()/86400000 + 2440587.5;
        values.push(v);
    }
    var sql='insert into ' + table + '(' + keys.join(',') + ') values (' + qmarks.join(',') + ')';
    run(sql, values, callback);
}

function update(table, condition, condition_values, fields, callback) {
    var keys=[], values=[];
    for (var field in fields) {
        if (field!='id') {
            keys.push(field+'=?');
            var v=fields[field];
            if (v!==null && v.constructor==Date)
                v= v.getTime()/86400000 + 2440587.5;
            values.push(v);
        }
    }
    var sql='update ' + table + ' set ' + keys.join(',');
    if (condition) {
        sql += ' where ' + condition;
        while (condition_values.length>0)
            values.push(condition_values.shift());
    }
    
    run(sql, values, callback);
}

function aggregate(table, operations, condition, args, callback) {
    var ops=[];
    for (var op in operations) {
        ops.push(operations[op] + ' as ' + op);
    }
    var sql = 'select ' + ops.join(',') + ' from ' + table;
    if (condition) {
        sql += ' where ' + condition;
    }

    get(sql, args, callback);
}

function remove(table, condition, args, callback) {
    var sql = 'delete from ' + table + ' where ' + condition;
    run(sql, args, callback);
}

function sql(statement, args, callback) {
    console.log(statement, args)
    run(statement, args, callback);
}

module.exports = {
    connect: connect_sqlite,

    all: all,
    get: get,
    run: run,

    getOne: getOne,
    getAll: getAll,
    aggregate: aggregate,
    insert: insert, 
    update: update,
    remove: remove,
    sql: sql
};
