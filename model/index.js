


module.exports.shop = require('./shop');
module.exports.coop = require('./coop');
module.exports.cashdesk = require('./cashdesk');

module.exports.admin = require('./admin');

module.exports.connect = function(config, callback) {

    var driver = (config.model||{}).driver || 'sqlite3';
    var db;
    switch (driver) {
        case 'sqlite3':   db = require('./sqlite'); break;
        case 'dblite':    db = require('./dblite'); break;
        default:          throw "unknown driver: " + driver;
    }

    db.connect(config, (err, methods) => {
        if (err) return callback(err);

        for (var m in methods) {
            module.exports[m] = methods[m];
        }

        callback(null);
    });
}

module.exports.sqlbatch = require('./sqlbatch');

module.exports.check_access = function check_access(customer, permissions, callback) {
    var result = {};
    var p=Array.from(permissions);
    var customer_roles=customer.roles;
    check();

    function check() {
        if (p.length  == 0)
            return callback(null, result);
        
        var token = p.shift();
        result[token] = false;
        module.exports.getOne( 'permissions', 'token=?', [ token ],
            (err, perm) => {
                if (err) throw err; //return check();
                perm = perm||{ roles:'[]' };
                var roles=JSON.parse(perm.roles);
                for (var role in roles) {
                    if (customer_roles.indexOf(roles[role]) >= 0) {
                        result[token] = true;
                        break;
                    }
                }

                check();
            }
        );
    }
} 
