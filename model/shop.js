const uuid = require('uuid');
const model=require('./index');

function get_carts(callback) {
    model.getAll(
        'shop_carts', 
        'state=? order by date', ['cart'],
        (err, result) => {
            if (err) return callback(err);
            callback(err, result);
        }
    )
}

function get_cart(cid, callback) {
    model.getOne(
        'shop_carts', 
        'id=?', [ cid ],
        (err, cart) => {
            model.getAll(
                'shop_cartitems',
                "cart=?", [ cid ],
                (err, items) => {
                    if (err) return callback(new Error(err));
                    callback(err, cart, items);
                }
            )
        }
    )
}

function create_cart(clerk, callback) {
    var cid = uuid();
    model.insert(
        'shop_carts', 
        { id: cid, state: 'cart', clerk: clerk },
        (err) => {
            if (err) return callback(new Error(err));
            return get_cart(cid, callback);
        }
    );
}

function annotate_cart(cid, annotation, callback) {
    model.update(
        'shop_carts', 
        'id=?', [ cid ], 
        {
            remark: annotation
        }, 
        (err) => {
            return callback(err);
        }
    );
}

function update_cart(cart, callback) {
    model.update(
        'shop_carts', 
        'id=?', [ cart.id ], 
        cart, 
        (err) => {
            if (err) return callback(new Error(err));
            return get_cart(cart.id, callback);
        }
    );
}

function remove_cart(cid, callback) {
    model.remove(
        'shop_cartitems', 
        'cart=?', [ cid ],
        (err) => {
            if (err) 
                callback(err);
            model.remove(
                'shop_carts', 'id=?', [ cid ],
                (err) => {
                    if (err) callback(Error(err));
                        return callback(err);
                }
            )
        }
    )
}

function get_category(catid, callback) {
    model.getOne(
        'shop_categories', 
        'id=?', [ catid ],
        (err, category) => {
            if (err) return callback(new Error(err));
            callback(err, category);
        }
    );
}

function update_category(record, callback) {
    model.update(
        'shop_categories', 
        'id=?', [ record.id ], 
        record,
        (err) => {
            if (err) return callback(new Error(err));
            callback(err);
        }
    );
}

function create_category(record, callback) {
    model.insert(
        'shop_categories', 
        record,
        (err) => {
            if (err) return callback(new Error(err));
            callback(err);
        }
    );
}

function get_category_info(catid, callback) {
    // thats fucking inefficient, but should work for now... 
    var cnt=0, error=null;
    categories = {}
    
    var root_category={id: 'emma-shop', name: 'Hauptkategorie', path: ''};
    traverse(root_category);

    function traverse(category) {
        categories[category.id] = category;
        ++cnt;
        model.getAll(
            'shop_categories', 
            'category=? order by sort_order', [ category.id ],
            (err, catlist) => {
                --cnt;
                if (err) {
                    throw err;
                    error=err;
                } else {
                    category.items=[];
                    for(var n in catlist) {
                        let p = (category.path=='') ? '' : category.path + ' / '
                        var cat={id: catlist[n].id, name: catlist[n].name, path: p + catlist[n].name};
                        category.items.push(cat);
                        traverse(cat);
                    }
                    if (cnt<=0) {
                        callback(error, categories[catid]);
                    } 
                }
            }
        );
    }
}

function update_category_image(record, callback) {
    model.update(
        'shop_categories', 
        'id=?', [ record.id ], 
        record,
        (err) => {
            if (err) return callback(new Error(err));
            callback(err);
        }
    );
}

function remove_category(id, callback) {
    model.remove(
        'shop_categories', 
        'id=?', id,
        (err, result) => {
            if (err)
                return callback(new Error(err));

            callback(null);
        }
    );
}

function get_category_tree(callback) {
    var cnt=0, error=null;
    var root_category={id: 'emma-shop', name: 'Hauptkategorie', path: ''};
    traverse(root_category);

    function traverse(category) {
        ++cnt;
        model.getAll(
            'shop_categories', 
            'category=? order by sort_order', [ category.id ],
            (err, catlist) => {
                --cnt;
                if (err) {
                    throw err;
                    error=err;
                } else {
                    category.items=[];
                    for(var n in catlist) {
                        let p = (category.path=='') ? '' : category.path + ' / '
                        var cat={id: catlist[n].id, name: catlist[n].name, path: p + catlist[n].name};
                        category.items.push(cat);
                        traverse(cat);
                    }
                    if (cnt<=0) {
                        callback(error, root_category);
                    } 
                }
            }
        );
    }
}

function get_categories_and_products(parent, callback) {

    model.getAll(
        'shop_categories', 
        'category=? order by sort_order', [ parent ],
        (err, categories) => {
            if (err) return callback(err);

            model.getAll(
                'shop_products',' category=?', [ parent ],
                (err, products) => {
                    if (err) return callback(err);
                    callback(err, categories, products);
                }
            );
        }
    );
}

function get_product(pid, callback) {
    model.getOne(
        'shop_products', 
        'id=?', [ pid ],
        (err, category) => {
            if (err) return callback(new Error(err));
            callback(err, category);
        }
    );

}

function find_products(name, callback) {
    console.log('find54', name)
    let xtra_search = '';
    if (name.substr(0,1)=='*') {
        name = name.substr(1);
    } else {
        xtra_search = 'and category in ( select id from shop_categories where searchable = 1)'
    }
    model.getAll(
        'shop_products',
        'name like?' + xtra_search,[ name ],
        (err, products) => {
            if (err) return callback(new Error(err));
            callback(err, products);
        }
    )
}

function create_product(record, callback) {
    model.insert(
        'shop_products', 
        record,
        (err) => {
            if (err) return callback(new Error(err));
            callback(err);
        }
    );
}

function update_product(record, callback) {
    model.update(
        'shop_products', 
        'id=?', [ record.id ], 
        record,
        (err) => {
            if (err) return callback(new Error(err));
            callback(err);
        }
    );
}

function update_product_image(record, callback) {
    model.update(
        'shop_products', 
        'id=?', [ record.id ], 
        record,
        (err) => {
            if (err) return callback(new Error(err));
            callback(err);
        }
    );
}

function cart_update(cartid, callback) {
    var count, total, vat;

    model.aggregate(
        'shop_cartitems',
        { total: 'sum(price*amount)' },
        'cart=?', [ cartid ],   // "select sum(price*amount) as total from shop_cartitems where cart=?", [ cartid ],
        (err, result) => {
            if (err) return callback(new Error(err));
            total = result.total;
            model.aggregate(
                'shop_cartitems',
                { vat: 'sum(price*amount - price*amount / (1+vat/100) )' },
                'cart=?', [ cartid ], // "select sum(price*amount - price*amount / (1+vat/100) ) as vat from shop_cartitems where cart=?", [ cartid ],
                (err, result) => {
                    if (err) return callback(new Error(err));
                    vat = result.vat;
                    model.aggregate(
                        'shop_cartitems',
                        { count: 'count(*)' },
                        'cart=?', [ cartid ],   // "select count(*) as count from shop_cartitems where cart=?", [ cartid ],
                        (err, result) => {
                            if (err) return callback(new Error(err));
                            count = result.count;
                            model.update(
                                'shop_carts', 
                                'id=?', [ cartid ],
                                {   item_count: count || 0,
                                    total: total || 0,
                                    vat_total: vat || 0
                                },
                                (err, result) => {
                                    if (err) return callback(new Error(err));
                                    callback(err);                                        
                                }
                            )
                        }
                    );
                
                }
            );
        }
    );
}

function create_cartitem(record, callback) {
    model.insert(
        'shop_cartitems', 
        record, 
        (err) => {
            if (err) return callback(new Error(err));
            cart_update(record.cart, callback);
        }
    );
}

function get_cartitem(itemid, callback) {
    model.getOne(
        'shop_cartitems', 
        'id=?', [ itemid ],
        (err, item) => {
            if (err) return callback(new Error(err));
            callback(err, item);
        }
    );
}

function update_cartitem(record, callback) {
    model.update(
        'shop_cartitems', 
        'id=?', [ record.id ],
        record,
        (err) => {
            if (err) return callback(new Error(err));
            cart_update(record.cart, callback);
        }
    );
}

function remove_cartitem(itemid, callback) {
    model.getOne(
        'shop_cartitems', 
        'id=?', [ itemid ],
        (err, cartitem) => {
            if (err) 
                return callback(new Error(err));
            if (cartitem==undefined) 
                return callback(new Error('no such Cartitem: ' + itemid));
            model.remove(
                'shop_cartitems', 
                'id=?', [ itemid ],
                (err) => {
                    if (err) return callback(new Error(err));
                    cart_update(cartitem.cart, callback);
                }
            );
        }
    );
}

function add_to_journal(entry, callback) {
    entry,id = uuid();
    model.insert(
        'shop_journal', 
        entry,
        (err) => {
            if (err) 
                return callback(new Error(err));
            callback(err);
        }
    )
}

function add_to_cashdesk(entry, callback) {
    model.insert(
        'shop_cashdesk', 
        entry,
        (err) => {
            if (err) 
                return callback(new Error(err));
            callback(err);
        }
    )
}

function cash_amount(callback) {
    model.aggregate(
        'shop_journal',
        { total: 'sum(amount)' },
        '', [  ],   // "select sum(price*amount) as total from shop_cartitems where cart=?", [ cartid ],
        (err, result) => {
            if (err) 
                return callback(new Error(err));
            callback(null, result.total);
        }
    );
}

function stock_value(callback) {
    model.aggregate(
        "shop_products",
        { total: 'sum(stock*price)' },
        '', [],
        (err, result) => {
            if (err) 
                return callback(new Error(err));
            callback(null, result.total);
        }
    );
}


/**
 * perform the checkout of the cart items
 * 
 * - update product stock; 
 * - calculate & payout commissions
 * - TODO: mark cartitem as 'paid'
 * 
 * @param {*} items 
 * @param {*} callback 
 */
 function perform_item_checkout(cart_id, items, callback) {
    
    function process_items() {
        if (items.length>0) {

            var item=items.shift();

            get_product(item.product, (err, product) => {
                if (err) 
                    return callback(new Error(err));
                if (product == undefined)
                    return callback(new Error('No such Product: '+item.product));

                product.stock = (product.stock || 0) - item.amount;
                
                // don't allow negative stock
                if (product.stock<0) product.stock=0;

                update_product(product, (err) => {
                    if (err) 
                        return callback(new Error(err));
                        
                    if (product.commission && product.commissioner) {
                        let price = Number(item.price) * Number(item.amount);
                        let commission = price*(Number(product.commission)/100);

                        model.coop.add_to_journal( {
                            type: 'commission',
                            description: 'Kommission fuer ' + product.name,
                            reference: cart_id,
                            customer: product.commissioner,
                            amount: commission,
                            vat:0 
                        }, (err) => {
                            if (err) 
                                return callback(new Error(err));
                            
                            model.coop.add_to_journal( {
                                type: 'commision payout',
                                description: 'Kommission fuer ' + product.name,
                                reference: cart_id,
                                customer: 'emma-shop',
                                amount: -commission,
                                vat: 0
                            }, (err) => {
                                if (err) 
                                    return callback(new Error(err));

                                // continue with next item
                                process_items();
                            });
                        });

                    } else {
                        // continue with next item
                        process_items();
                    }
                });
            });

        } else {
            callback(null);
        }
    }

    process_items();
}


module.exports = {
    get_carts: get_carts,

    get_cart: get_cart,
    create_cart: create_cart,
    remove_cart: remove_cart,
    update_cart: update_cart,
    annotate_cart: annotate_cart,

    get_category: get_category,
    create_category: create_category,
    update_category: update_category,
    update_category_image: update_category_image,
    remove_category: remove_category,
    get_category_info: get_category_info,

    get_product: get_product,
    create_product: create_product,
    update_product: update_product,
    update_product_image: update_product_image,
    
    find_products: find_products,

    get_categories_and_products: get_categories_and_products,
    get_category_tree: get_category_tree,

    get_cartitem: get_cartitem,
    create_cartitem:create_cartitem,
    update_cartitem: update_cartitem,
    remove_cartitem: remove_cartitem,
    
    add_to_journal: add_to_journal,
    add_to_cashdesk: add_to_cashdesk,

    cash_amount: cash_amount,
    stock_value: stock_value

};