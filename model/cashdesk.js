const uuid = require('uuid');

const model=require('./index');

module.exports = {
    overview: overview,
    add_to_journal: add_to_journal,
    deposit: deposit,
    withdraw: withdraw,
    cashin: cashin,
    cashout: cashout

}

function add_to_journal(entry, callback) {
    entry,id = uuid();
    model.insert(
        'cashdesk_journal', 
        entry,
        (err) => {
            if (err) 
                return callback(new Error(err));
            callback(err);
        }
    )
}

function overview(callback) {
    model.aggregate(
        'cashdesk_journal',
        { balance: 'sum(amount)'},
        'true', [ ],
        (err, balance) => {
            if (err)
                return callback(err);

            model.getAll(
                'cashdesk_journal',
                ' 1=1 order by date DESC', [],
                (err, result) => {
                    if (err)
                        return callback(err);
                    callback(err, {
                        balance: balance.balance,
                        values: result
                    });
                }
            )
        }
    );
}

function deposit(amount, remark, callback) {
    model.shop.add_to_journal({
        amount: -amount,
        vat: 0,
        type: 'cashdesk deposit',
        remark: remark
    }, (err) => {
        if (err) return callback(err);

        model.coop.add_to_journal({
            customer: 'emma-shop',
            amount: amount,
            vat: 0,
            type: 'cashdesk deposit',
            remark: remark 
        }, (err) => {
            if (err) return callback(err);

            model.coop.add_to_journal({
                customer: 'cashdesk',
                amount: -amount,
                vat: 0,
                type: 'cashdesk deposit',
                remark: remark     
            }, (err) => {
                if (err) return callback(err);

                add_to_journal({
                    amount: amount, 
                    type: 'cashdesk deposit', 
                    remark: remark
                }, (err) => {
                    if (err) return callback(err);
                    callback(null);
                })
            });
    
        });
    });
}

function withdraw(amount, remark, callback) {
        model.coop.add_to_journal({
            customer: 'food-coop',
            amount: -amount,
            vat: 0,
            type: 'cashdesk withdraw',
            remark: remark 
        }, (err) => {
            if (err) return callback(err);

            model.coop.add_to_journal({
                customer: 'cashdesk',
                amount: amount,
                vat: 0,
                type: 'cashdesk withdraw',
                remark: remark     
            }, (err) => {

                if (err) return callback(err);
                add_to_journal({
                    amount: -amount, 
                    type: 'cashdesk withdraw', 
                    remark: remark
                }, (err) => {
                    if (err) return callback(err);
                    callback(null);
                })
            });
    
        });
}

function cashin(amount, customer, remark, callback) {
    model.coop.add_to_journal({
        customer: customer,
        amount: amount,
        vat: 0,
        type: 'cashdesk cashin',
        remark: remark 
    }, (err) => {
        if (err) return callback(err);

        model.coop.add_to_journal({
            customer: 'cashdesk',
            amount: -amount,
            vat: 0,
            type: 'cashdesk cashin',
            remark: remark     
        }, (err) => {

            if (err) return callback(err);
            add_to_journal({
                amount: amount, 
                type: 'cashdesk cashin', 
                customer: customer,
                remark: remark
            }, (err) => {
                if (err) return callback(err);
                callback(null);
            })
        });

    });
}

function cashout(amount, customer, remark, callback) {
    model.coop.add_to_journal({
        customer: customer,
        amount: -amount,
        vat: 0,
        type: 'cashdesk cashout',
        remark: remark 
    }, (err) => {
        if (err) return callback(err);

        model.coop.add_to_journal({
            customer: 'cashdesk',
            amount: amount,
            vat: 0,
            type: 'cashdesk cashout',
            remark: remark     
        }, (err) => {

            if (err) return callback(err);
            add_to_journal({
                amount: -amount, 
                type: 'cashdesk cashout', 
                customer: customer,
                remark: remark
            }, (err) => {
                if (err) return callback(err);
                callback(null);
            })
        });

    });
}