const dblite = require('dblite');

var db=null;
var schema={};

function connect_dblite(config, callback) {
    var file = (config.model || {}).database || 'db/catalog.sqlite';
    db = new dblite(file, '-header');

    db.query(
        "select * from sqlite_master where type='table'",
        (err, tables) => {
            get_meta_info();

            function get_meta_info() {
                if (tables.length==0) {
                    console.info('database connection established via dblite.');
                    return callback(null, module.exports);
                }
                
                var table=tables.shift();
                db.query(
                    "pragma table_info(?)", [ table.name ],
                    (err, info) => {
                        var columns={};
                        var tp;
                        for (var n in info) {
                            switch(info[n].type) {
                                case 'VARCHAR':
                                    tp=String; break;
                                case 'BOOLEAN':
                                    tp=Boolean; break;
                                case 'DATETIME':
                                    tp=Date; break;
                                case 'DATE':
                                    tp=Date; break;
                                case 'DOUBLE':
                                    tp=Number; break;
                                case 'INTEGER':
                                    tp=Number; break;
                                default:
                                    console.log('unknown type: '+info[n].type);
                            }
                            columns[info[n].name] = tp;
                        }
                        schema[table.name] = columns;

                        get_meta_info();
                    }
                )
            }
        }
    )
}

function query(sql, args, schema, callback) {
 //   console.log(sql,args,schema)

    db.query.apply(db, arguments);
}


function getOne(table, condition, args, callback) {
    var sql='select rowid,* from ' + table;
    if (condition) {
        sql += ' where ' + condition;
    } 

    query(sql, args, schema[table], (err, result) => {
        if (err) return callback(err);

        callback(null, result[0])
    });
}

function getAll(table, condition, args, callback) {
    var sql='select rowid,* from ' + table;
    if (condition) {
        sql += ' where ' + condition;
    } 
    query(sql, args, schema[table], callback);
}

function insert( table, fields, callback) {
    var keys=[], values=[], qmarks=[];
    for (var field in fields) {
        keys.push(field);
        qmarks.push('?');
        var v=fields[field];
        if (v.constructor==Date)
            v=now.getTime()/86400000 + 2440587.5;
        values.push(v);
    }
    var sql='insert into ' + table + '(' + keys.join(',') + ') values (' + qmarks.join(',') + ')';
    query(sql, values, callback);
}

function update(table, condition, condition_values, fields, callback) {
    var keys=[], values=[];
    for (var field in fields) {
        if (field!='id') {
            keys.push(field+'=?');
            var v=fields[field];
            if (v.constructor==Date)
                v=now.getTime()/86400000 + 2440587.5;
            values.push(v);
        }
    }
    var sql='update ' + table + ' set ' + keys.join(',');
    if (condition) {
        sql += ' where ' + condition;
        while (condition_values.length>0)
            values.push(condition_values.shift());
    }
    query(sql, values, callback);
}

function aggregate(table, operations, condition, args, callback) {
    var ops=[];
    var schema={};
    for (var op in operations) {
        ops.push(operations[op] + ' as ' + op);
        schema[op] = Number;
    }
    var sql = 'select ' + ops.join(',') + ' from ' + table;
    if (condition) {
        sql += ' where ' + condition;
    }

    query(sql, args, schema, (err, result) => {
        if(err) return callback(err);
        callback(err, result[0]); 
    });
}

function remove(table, condition, args, callback) {
    var sql = 'delete from ' + table + ' where ' + condition;
    query(sql, args, callback);
}

function sql(statement, args, callback) {
    query(statement, args, callback);
}

module.exports = {
    connect: connect_dblite,

    query: query,

    getOne: getOne,
    getAll: getAll,
    aggregate: aggregate,
    insert: insert, 
    update: update,
    remove: remove,
    sql: sql
};