module.exports = {
    'de': {
        'Coop: Overview': 'Food-Coop: Übersicht',

        'Balance Overview': 'Kassen-Uebersicht',
        'Shop Balance:': 'Shop-Konto:',
        'Stock value:': 'Warenwert:',
        'Members Balance:': 'Kundenkonten:',
        'Desk:': 'Kasse:',
        'Cashdesk:': 'Handkasse:',

        'Coop: Member List': 'Food-Coop: Mitglieder',


        'Members': 'Mitglieder',
        'New Member': 'Neues Mitglied',
        
        'Coop: Member Cashin': 'Coop: Bareinzahlung',
        'Cashin': 'Einzahlen',
        'Back to Member Page': 'Zurueck zur Mitgliedsansicht',

        'Edit Member': 'Bearbeiten',
        'Deposit Cash': 'Einzahlung',
        'Withdraw Cash': 'Auszahlung',
        'Coop: Member Account': 'Mitgliedskonto',
        'Type': 'Art',
        'no transactions': 'keine Vorgänge',

        'Administrator': 'Administrator',
        'Clerk': 'Verkäufer',
        'Manager': 'Manager',
        'Coop: Edit Member': 'Coop: Mitglied bearbeiten',
        'This id is already in use.': 'Diese Id wird bereits benutzt.',
        'This pin is already in use.': 'Diese Pin ist bereits benutzt.',
        'Special': 'Spezial',
        'Roles': 'Rollen',

        'Special Accounts: ': 'Spezialkonten:',
        'Coop Balance:': 'Coop Konto',

        'of': 'von',
        'Id': 'Id',
        'Balance': 'Kontostand',
        'Coop: Member Cashout': 'Coop: Auszahlung',
        'Cashout': 'Auszahlung',

        'Journal': 'Kassenbuch',

        'Delivery': 'Lieferung',
        'Coop: Deliveries': 'Coop: Lieferungen',
        'Coop: Delivery': 'Coop: Lieferung',
        'Back to Coop Page': 'Zurueck zur Coop Uebersicht',
        'New': 'Neu',
        'New Delivery': 'Neue Lieferung',
        'State': 'Status',

        'amount:': 'Menge:',
        'old stock:': 'alter Bestand:',
        'old price: ': 'alter Preis: ',
        'price: ': 'Preis: ',

        'Delete this pending delivery': 'Diese Lieferung loeschen',
        'Finish': 'Abschliessen',

        'Duplicate Delivery': 'Diese Lieferung duplizieren',
    }
};