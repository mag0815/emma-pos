module.exports = {
    'de': {
        "To Coop": 'Zur Coop',
        "From Desk": "Aus der Kasse",
        "Cashdesk": "Handgeldkasse",
        "Date": "Datum",
        "Date": "Datum",
        "Amount": "Betrag",
        "Customer": "Mitglied",
        "Cashdesk Balance:": "Handgeld Kassenstand:",
        "&euro;": "&euro;",

        'Cashdesk: Cash-In from Coop Member': 'Handgeldkasse: Einzahlung von Mitglied',
        'Cashdesk: Cash-Out to Coop Member':  'Handgeldkasse: Auszahlung an Mitglied',
        'Cashdesk: Withdraw to Coop': 'Handgeldkasse: Auszahlung Coop',
        'Cashdesk: Deposit from Desk': 'Handgeldkasse: Bargeldentnahme',
        'Withdraw': 'Auszahlen',
        'Deposit': 'Entnehmen'

    }
};