module.exports = {
    'de': {

        ';': ';',
        'vars=': 'vars=',
        
        'Welcome to emmaPOS': 'Willkommen bei emmaPOS',
        'Back': 'Zurück',
        'Delete': 'Löschen',
        'Message': 'Nachricht',
        'Coop': 'FoodCoop',
        'Products': 'Produkte',
        '&nbsp;': '&nbsp;',
        'Go Back': 'Zurückgehen',
        '\n': '\n',
        'VAT': 'MwSt.',
        'incl.': 'inkl.',
        'Total': 'Gesamt',
        'Save': 'Speichern',

        'Login': 'Anmelden',
        'Food Coop': 'Food Coop',
        'Leave a Message': 'Eine Nachricht hinterlassen',
        'Login to the System': 'Als Benutzer anmelden',
        'Use dark Theme': 'Dunkles Farbschema',
        'Use light Theme': 'Helles Farbschema',
        'Logout': 'Abmelden',
        'Login as Admin': 'Als Administrator anmelden',

        'Name:': 'Name:',
        'Save': 'Speichern',
        'Image:': 'Bild',
        'Sort Order:': 'Sortierung:',
        'Edit': 'Bearbeiten',

        'This field is required': 'Das ist ein Pflichtfeld.',
        'INVALID FORM FIELD TYPE ':'INVALID FORM FIELD TYPE ',
        '%':'%',
        '€':'€',
        'Unknown Pin.': 'Falsche Pin.',

        'Whats new..': 'Neuigkeiten..',
        'view complete release notes..': 'komplette Historie..',
        'Latest Messages:': 'Letzte Nachrichten:',
        'There are no Messages yet.': 'Bisher keine Nachrichten',
        'characters left': 'Zeichen übrig',
        'Please enter your message.': 'Gib deine Nachricht ein.',
        'Send': 'Senden',
        'Leave Message': 'Nachricht hinterlasen'

        
    }
};
