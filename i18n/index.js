
const walk = require('pug-walk');

const extend = require('../lib/util').extend;

var translations={};

function translate_pug(req, res, next) {
    var lang=req.cookies.lang || 'en';
    req.lang = req.cookies.lang = lang;

    req.t = (str) => t(lang, str); 
    req.t.format_date = (str) => format_date(lang, str);
    req.t.format_currency = (str) => format_currency(lang, str);
    
    res.locals.plugins = [{
        postParse(ast) {
            return walk(ast, (node, replace) => {
                if (node.type === 'Text') {
                    node.type='Code';
                    let txt = node.val
                        .replace('\n', '\\n')
                        .replace('\r', '\\r')
                        .replace('\"', '\\\"')
                        .replace('\'', '\\\'')
                        .replace('\t', '\\t');
                    node.isInline=true;
                    node.buffer=true;
                    node.mustEscape=false;
                    node.val='t("' + txt + '")'; 
                }
            });
        }
    }];
    next();
};

extend( true, translations,
    require('./emmapos'),
    require('./cart'),
    require('./cashdesk'),
    require('./coop'),
    require('./admin')
);


function t(lang, str) {
    if (str.endsWith('();'))
        return str;
        
    if (translations[lang]) {
        if (translations[lang][str]) {
            return translations[lang][str];
        } else {
            if (str.trim()=='') return '';
            console.warn('no translation for \'' + str + "'");
            return str;
        }
    }
    return str;
}

function format_date(lang,dt) {
    if (lang == 'de')   
        return (new Date(dt)).toLocaleString('de-DE');
    return (new Date(dt)).toLocaleString();
}   

function format_currency(lang,n) {
    var res=Number(n).toFixed(2);
    if (lang == 'de')   
        return res.replace('.',',');
    return res;
}   

module.exports = {
    translate_pug: translate_pug,
    t: t,
    format_date: format_date
};



