module.exports = {
    'de': {
        'Admin Login': 'Anmeldung als Administrator',
        'Login into the System': 'Als Administrator anmelden',
        'Invalid PIN.': 'Falsche PIN.',
        'Enter your PIN-Code:': 'PIN-Code eingeben:',

        'Administration': 'Administration',
        'Invoices': 'Rechnungen',
        'Manage Invoices': 'Rechnungen verwalten',
        'Deliveries': 'Lieferungen',
        'Manage Deliveries': 'Lieferungen verwalten',
        'Orders': 'Bestellungen',
        'Manage Orders': 'Bestellungen verwalten',

        'Coop Member': 'Mitglieder',
        'Manage Coop Member': 'Mitglieder verwalten',

        'Manage the POS': 'POS verwalten',
        'Manage the Coop': 'Coop verwalten',
        'Manage the Cashdesk': 'Handgeldkasse verwalten',
        'view all messages..': 'alle Nachrichten..',
        'Release Notes': 'Release Notes',

        'Uncle Emma': 'Onkel Emma',
        'Emma Shop': 'Emma Shop',
        'Go to the Shop': 'Zum Shop',
        'Back to Home': 'Zurueck zur Startseite'
    }
};