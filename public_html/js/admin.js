
function adminlogin_init() {

    jQuery( function($) {

        $('.error').hide();
        $($('#pos-buttons button')[1]).attr('disabled', 'disabled');

        $('[name="pin"]').on('change', function() {
            var pin=$(this).value();
            var form=$('form');
            
            $('.error').hide();
            $($('#pos-buttons button')[1]).attr('disabled', 'disabled');

            if (pin.length == 6) {
                $.post('/admin/login-check', form.formdata(), (result) => {
                    if (result.status=='failure') {
                        $('.error').show();
                    } else {
                        $($('#pos-buttons button')[1]).removeAttr('disabled');
                    }
                });
                
            }
        });


        $('form')
            .on('success', (ev,result) => {
                load_page(result.redirect);
            })
            .on('error', (ev,result) => {
                alert(result.stack)
            })
            .on('fatal-error', (ev, xhr, cause) => {
            });

    });
}
