

function init_keypad() {}


jQuery( ($) =>{


    function type_in($el, char) {
        var $cursor=$('.cursor', $el);
        var type=$el.attr('type');

        // erase selection if present
        $('span:last-child',$el).removeClass('selected');

        while ($cursor.prev().hasClass('selected'))
            $cursor=$cursor.prev();
        while ($cursor.hasClass('selected')) {
            var sel=$cursor;
            $cursor=$cursor.next();
            sel.remove();
        }
        var display=char;
        if (type=='password')
            display='*';

        if (char=='\n') {
            $cursor.addClass('cursor').before($('<span ch="\u000d" class="nl"><br></span>'));
        } else {
            $cursor.addClass('cursor').before($('<span ch="'+char+'">'+char+'</span>'));
        }

        adjust_scroll($el);
    }

    function backspace($el) {
        var $cursor=$('.cursor', $el);
        var $selection=$('.selected',$el);

        if ($selection.length>0) {
            while($cursor.hasClass('selected'))
                $cursor=$cursor.next();
            $selection.remove();
            $cursor.addClass('cursor');
        } else {
            $cursor.prev().remove();
        }
        adjust_scroll($el)
    }

    function erase($el) {
        var $cursor=$('.cursor', $el);
        var $selection=$('.selected',$el);

        if ($selection.length>0) {
            while($cursor.hasClass('selected'))
                $cursor=$cursor.next();
            $selection.remove();
            $cursor.addClass('cursor');
        } else {
            var $next=$cursor.next();
            if ($next.length>0) {
                $cursor.remove();
                $next.addClass('cursor');
            }
        }
        adjust_scroll($el)
    }

    function go_left($el, shift, ctrl) {
        var $el = get_focus_element();
        var $cursor=$('.cursor', $el);

        if (shift) {
            $cursor=$cursor.prev();
            if ($cursor.length==0) return;

            $cursor.toggleClass('selected');
            if (ctrl) {
                while ($cursor.prev().text()==' ') {
                    $cursor=$cursor.prev();
                    $cursor.toggleClass('selected');        
                }
                while ($cursor.prev().length>0 && $cursor.prev().text()!=' ' && !$cursor.prev().hasClass('nl')) {
                    $cursor=$cursor.prev();
                    $cursor.toggleClass('selected');        
                }
            }

            $('span',$el).removeClass('cursor');
            $cursor.addClass('cursor');

        } else {
            while ($cursor.prev().hasClass('selected')) {
                $cursor = $cursor.prev();
            }
            if (!$cursor.hasClass('selected')) {
                $cursor = $cursor.prev();
            }

            if (ctrl) {
                while($cursor.prev().length>0 && $cursor.prev().text()==' ' && !$cursor.prev().hasClass('nl')) {
                    $cursor = $cursor.prev()
                }
                while($cursor.prev().length>0 && $cursor.prev().text()!=' ' && !$cursor.prev().hasClass('nl')) {
                    $cursor = $cursor.prev()
                }
            }

            $('span',$el).removeClass('selected');

            if ($cursor.length>0) {
                $('span',$el).removeClass('cursor');
                $cursor.addClass('cursor');
            }
        }
        adjust_scroll($el)
    }

    function go_right($el, shift, ctrl) {
        var $cursor=$('.cursor', $el);

        if (shift) {
            if ($cursor.next().length==0)
                return;

            if (ctrl) {
                while ($cursor.next().length>0 && $cursor.text()!=' ' && !$cursor.hasClass('nl')) {
                    $cursor.toggleClass('selected');
                    $cursor = $cursor.next();
                }
                while ($cursor.next().length>0 && $cursor.text()==' ' && !$cursor.hasClass('nl')) {
                    $cursor.toggleClass('selected');
                    $cursor = $cursor.next();
                }

            } else {
                $cursor.toggleClass('selected');
                $cursor=$cursor.next();
            }

            $('span',$el).removeClass('cursor');
            $cursor.addClass('cursor');

        } else {
            while ($cursor.hasClass('selected'))
                $cursor=$cursor.next();
            if (!$cursor.prev().hasClass('selected')) {
                if ($cursor.next().length>0)
                    $cursor = $cursor.next();
            }

            if (ctrl) {
                if ($cursor.prev().text()!=' ' && !$cursor.hasClass('nl')) {
                    while ($cursor.next().length>0 && $cursor.text()!=' ' && !$cursor.hasClass('nl'))
                        $cursor = $cursor.next();
                }
                while ($cursor.next().length>0 && ($cursor.text()==' ' || $cursor.hasClass('nl')))
                    $cursor = $cursor.next();
            }
                
            $('span',$el).removeClass('selected');
            $('span',$el).removeClass('cursor');
            $cursor.addClass('cursor');
        }
        adjust_scroll($el)
    }

    function go_homepos($el, shift) {
        var $cursor=$('.cursor', $el);

        if (shift) {
            while ($cursor.prev().hasClass('selected')) {
                $cursor=$cursor.prev();
                $cursor.toggleClass('selected');
            }
        } else
            $('span', $el).removeClass('selected cursor');

        do {
            $cursor=$cursor.prev();
            if (shift) {
                $cursor.toggleClass('selected');
            }
        } while($cursor.prev().length>0);

        $cursor.addClass('cursor');
        adjust_scroll($el);
    }

    function go_endpos($el, shift) {
        var $cursor=$('.cursor', $el);

        adjust_scroll($el)
    }

    function adjust_scroll($el) {
        var type=$el.attr('type');
        if (type=='text')
            adjust_vscroll($el);
        else
            adjust_hscroll($el)
    }

    function adjust_hscroll($el) {
        $el=$('.input-line', $el)
        var $cursor=$('.cursor', $el);
        var h=$cursor.offset().left - $el.offset().left;

        if (h>$el.width()) {
            $el.scrollLeft($el.scrollLeft() + h-$el.width() + 4);
        } else if (h<0) {
            $el.scrollLeft($el.scrollLeft() + h);
        }

    }

    function adjust_vscroll($el) {
        var $cursor=$('.cursor', $el);
        var c_top = $cursor.offset().top - $el.offset().top;
        if (c_top<0) {
            $el.scrollTop( $el.scrollTop() - $cursor.height() );
        } else if (c_top+$cursor.height() > $el.height()) {
            $el.scrollTop( $el.scrollTop() + $cursor.height() );
        }
    }

    function go_up($el, shift) {
        var $cursor=$('.cursor', $el);

        var $c_offset=$cursor.offset();
        var $el_offset=$el.offset();

        var c_top=$c_offset.top - $el_offset.top;
        var top_margin = ($el.outerHeight()-$el.height())/2;

        if (c_top - top_margin - $cursor.height() < 0) {
            $el.scrollTop( $el.scrollTop() - $cursor.height());
            c_offset=$cursor.offset();
        }

        var $n=$(document.elementFromPoint($c_offset.left, $c_offset.top-4));
        if ($n.parent().is($el)) {
            $n=$cursor.prev();
            while($n.length>0 && !$n.hasClass('nl')) {
                $n=$n.prev();
            }
            if ($n.length==0) 
                return false;
        } else if (!$n.parent().parent().is($el) ) {
            return false;
        } 

        if (shift) {
            if ($cursor.hasClass('selected'))
                $('span',$el).removeClass('selected');
            do {
                $cursor.addClass('selected');
                $cursor = $cursor.prev();
            } while (!$cursor.is($n));
            $cursor.addClass('selected');
        } else {
            $('span',$el).removeClass('selected');
        }
        $('span',$el).removeClass('cursor');
        $n.addClass('cursor');

        adjust_scroll($el);
    }

    function go_down($el, shift) {
        var $cursor=$('.cursor', $el);

        var $c_offset=$cursor.offset();
        var $el_offset=$el.offset();

        var c_top=$c_offset.top - $el_offset.top;
        var top_margin = ($el.outerHeight()-$el.height())/2;

        if (c_top - top_margin - $cursor.height() > $el.height()) {
            $el.scrollTop( $el.scrollTop() + $cursor.height());
            c_offset=$cursor.offset();
        }

        var $n=$(document.elementFromPoint($c_offset.left, $c_offset.top + $cursor.height()+2));
        console.log($el,$n)
        if ($n.parent().is($el)) {

        } else if (!$n.parent().parent().is($el) ) {
            return false;
        } 

        
        $('span',$el).removeClass('cursor');
        $n.addClass('cursor');

        adjust_scroll($el);

    }

    function select_all($el) {
        var spans=$('span',$el);
        if (spans.length==1) {
            spans.removeClass('selected').addClass('cursor');
        } else {
            spans.addClass('selected').removeClass('cursor');
            spans=$('span:last-child',$el).removeClass('selected').addClass('cursor');
        }
    }

    function update_text_value($el) {
        $span_list=$('span', $el);
        for (var n=0; n<$span_list.length; ++n) {
            var $span=$span_list[n];
            if ($span.hasClass('cr')) {

            } else if (!$span.hasClass('nl')) {

            }
        }
    }

    function set_value($el, value) {
    }

    function toggle_select($el) {
        $('.checkbox-hook', $el).toggleClass('checked');
    }



    function validate($el) {
        return true;
    }

    function get_focus_element() {  
        var $focus = $('.input.focus');
        return $focus;
    }


    function loose_focus($el) {
        if (validate($el)) {
            $el.removeClass('focus');
            return true;
        }
        return false;
    }

    function set_focus($el) {
        var $focus = $('.input.focus');
        if (!$focus.is($el)) {
            if (loose_focus($focus)) {
                $el.addClass('focus');
                switch($el.attr('type')) {
                    case 'string':
                    case 'password':
                    case 'text':
                        select_all($el);    
                        break;
                    case 'checkbox':
                        toggle_select($el);
                        break;
                }
            }
        } else {
            if ($el.attr('type') == 'checkbox') {
                toggle_select($el);
            }
        }
    }

    $('div.input'). on('mousedown', function(ev) {
        var $focus = $('.input.focus');
        if (!$focus.is($(this))) {
        } else {
            console.log(ev);
        }
        
    });

    $('div.input'). on('click', function(ev) {
        set_focus($(this));
        ev.stopPropagation();
    });

    $(window). on('click', function(ev) {
        var $focus = $('.input.focus');
        loose_focus($focus);
    });



    function key_down(key, shift, ctrl, alt, meta) {

        var $el = get_focus_element();
        var type = $el.attr('type');

        if (type =='string' || type =='password' || type=='text' || type=='autocomplete') {
            if (key.length==1 && ctrl==false && alt==false && meta==false) {
                return type_in($el,key);
            }
            switch(key) {
                case 'ArrowLeft':
                    return go_left($el,shift, ctrl);
                case 'ArrowRight':
                    return go_right($el,shift, ctrl);

                case 'ArrowUp':
                    if (type=='text')
                        return go_up($el, shift);
                    break;
                case'ArrowDown':
                    if (type=='text')
                        return go_down($el, shift);
                    break;                

                case 'Enter':
                    if (type=='text')
                        return type_in($el,'\n');
                    break;

                case 'Delete':
                    if (shift) return cut($el);
                    return erase($el);
                case 'Backspace':
                    return backspace($el);

                case 'Home':
                    return go_homepos($el, shift);
                case 'End':
                    return go_endpos($el, shift);

                default:
                    console.log(key);
            }

            if (ctrl) {
                switch(key) {
                    case 'a':
                    case 'A':
                        return select_all($el);
                    case 'x': 
                    case 'X':
                        return cut($el);
                    case 'c': 
                    case 'C':
                        return copy($el);
                    case 'v': 
                    case 'V':
                        return paste($el);
                }
            }
        }

        if (type=='checkbox') {
            if (key==' ') {
                return toggle_select($el);
            }
        }

    }


    $(window).on('keydown', (ev) => {
        key_down(ev.key, ev.shiftKey, ev.ctrlKey, ev.altKey, ev.metaKey);
        ev.preventDefault()
    });

});