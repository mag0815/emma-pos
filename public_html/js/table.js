


class RemoteModel {

    constructor(datasource, columns) {

        this.datasource=datasource;
        this.columns=columns;

        this.data = {length: 0};
        this.searchstr = "";
        this.sortcol = null;
        this.sortdir = 1;
        this.h_request = null;
        this.req = null; // ajax request
    
        // events
        this.onDataLoading = new Slick.Event();
        this.onDataLoaded = new Slick.Event();
    }


    isDataLoaded(from, to) {
        for (var i = from; i <= to; i++) {
            if (this.data[i] == undefined || this.data[i] == null) {
                return false;
            }   
        }
        return true;
    }

    clear() {
        for (var key in this.data) {
            delete this.data[key];
        }
        this.data.length = 0;
    }


    getPayload(from, to) {
        var payload = {
            from: from,
            count: to - from,
            sort: 'rowid',
            sort_dir: 'asc'
        };
        if (this.sortcol != null) {
            payload.sort = this.sortcol;
            payload.sort_dir = this.sortdir;
        }

        return payload; 
    }

    ensureData(from, to) {
        if (this.req) {
            this.req.abort();
            for (var i = this.req.loadFrom; i <= this.req.loadTo; i++) {
                this.data[i] = undefined;
            }
            this.req=null;
        }
  
        if (from < 0) {
            from = 0;
        }
  
        if (to - from<20)
            to=from+20;

        if (this.data.length > 0) {
            to = Math.min(to, this.data.length - 1);
        }

        var loadFrom=from, loadTo=to;
  
        /*
        while (this.data[loadFrom] !== undefined && loadFrom<=loadTo)
            loadFrom++;
  
        while (this.data[loadTo] !== undefined && loadFrom <= loadTo)
            loadTo--;

        if (loadFrom > loadTo || ((loadFrom == loadTo) && this.data[loadFrom] !== undefined)) {
            this.onDataLoaded.notify({from: from, to: to});
            return;
        }
        */
  
        var payload = this.getPayload(loadFrom, loadTo);
  
        if (this.h_request != null) {
          clearTimeout(this.h_request);
        }   
  
        this.h_request = setTimeout( () => {
            this.h_request=null;
            for (var i = loadFrom; i <= loadTo; i++)
                this.data[i] = null; // null indicates a 'requested but not available yet'
  
            this.onDataLoading.notify({from: from, to: to});
  
            this.req = $.post(
                this.datasource+'get', payload, 
                (result) => {
                    this.onSuccess(result.result);
                }
            ).fail( (ev) => {
                if (ev.statusText!='abort') 
                    this.onError(loadFrom, loadTo);
            });
                
            this.req.loadFrom = loadFrom;
            this.req.loadTo = loadTo;
        }, 50);
    }

    onError(fromPage, toPage) {
        alert("error loading items " + fromPage + " to " + toPage);
    }

    onSuccess(resp) {
        var from=Number(resp.from), count = Number(resp.count);
        this.data.length = resp.row_count;

        for (var i = 0; i < count; i++) {
            var item = resp.rows[i];
            this.data[from + i] = item;
            this.data[from + i].index = from + i;
        }

        this.req = null;

        //TODO: shoud notify _requested_ instead of _loaded_ items?
        this.onDataLoaded.notify({from: from, to: from+count});
    }


    reloadData(from, to) {
        for (var i = from; i <= to; i++)
          delete this.data[i];
  
          this.ensureData(from, to);
    }
  

    setSort(column, dir) {
        this.sortcol = column;
        this.sortdir = dir;
        this.clear();
    }
  
    setSearch(str) {
        this.searchstr = str;
        this.clear();
    }
}

class GridView {
    constructor(container, datamodel, options) {

        this.container = container;
        this.datamodel = datamodel;
        this.options = options;

        options.formatterFactory = GridView;
        options.editorFactory = GridView;

        options.fullWidthRows = true;
        options.editable = true;
        options.autoEdit = true;

        this.create_grid();
    }

    create_grid() {
        this.grid = new Slick.Grid(this.container, this.datamodel.data, this.datamodel.columns, this.options);

        this.grid.onViewportChanged.subscribe((e, args) => {
            var vp = this.grid.getViewport();
            this.datamodel.ensureData(vp.top, vp.bottom);
        });
    
        this.grid.onSort.subscribe( (e, args) => {
            this.datamodel.setSort(args.sortCol.field, args.sortAsc ? 1 : -1);
            var vp = this.grid.getViewport();
            this.datamodel.ensureData(vp.top, vp.bottom);
        });
    
        this.datamodel.onDataLoading.subscribe(() => { this.loading_indicator(true); });
    
        this.datamodel.onDataLoaded.subscribe((e, args) => {
            for (var i = args.from; i <= args.to; i++) {
                this.grid.invalidateRow(i);
            }
    
            this.grid.updateRowCount();
            this.grid.render();
    
            this.loading_indicator(false);
        });
    
        this.grid.setSortColumn("pubDate", false);
    
        // load the first page
        this.grid.onViewportChanged.notify();

        new ResizeSensor($(this.container), () => {
            console.log('resize')
            this.grid.resizeCanvas()
        });
        //$(this.container).on('click', () => {
         //   console.log(this.grid.getColumns())
        //})
    }

    loading_indicator(show) {
        if (this.loading_indicator_span == undefined) {
            this.loading_indicator_span = $('<span class="loading-indicator"></span>').appendTo($(this.container));
        }
        var $g=$(this.container);
        this.loading_indicator_span
            .css("position", "absolute")
            .css("top", $g.position().top + $g.height() / 2 - this.loading_indicator_span.height() / 2)
            .css("left", $g.position().left + $g.width() / 2 - this.loading_indicator_span.width() / 2);

        if (show) {
            this.loading_indicator_span.show();
        } else {
            this.loading_indicator_span.fadeOut();
        }
    }

    static getFormatter(column) {
        switch(column.format) {
            case 'String':
                return simpleFormat;
            case 'Number':
                return numberFormat;
            case 'Currency':
                return currencyFormat;
            case 'Percent':
                return percentFormat;
            case 'Image':
                return imageFormat;
            case 'Id':
                return idFormat;
            default:
                if(column.format.endsWith('Id'))
                    return idFormat;
                return simpleFormat;
        }

        function simpleFormat(row, cell, value, columnDef, dataContext) {
            return value;
        }

        function numberFormat(row, cell, value, columnDef, dataContext) {
            if (value) {
                var cls='number';
                if (Number(value)<0) cls+=' negative';
                value=Number(value).toFixed(2);
                if (lang=='de') value=value.replace('.',',');
                return '<span class="' + cls + '">' + value + '</span>';
            } else
                return '';
        }

        function currencyFormat(row, cell, value, columnDef, dataContext) {
            if (value) {
                var cls='currency';
                if (Number(value)<0) cls+=' negative';
                value=Number(value).toFixed(2);
                if (lang=='de') value=value.replace('.',',');
                return '<span class="' + cls + '">' + value + '<sup>€</sup></span>';
            } else
            return '';
        }

        function percentFormat(row, cell, value, columnDef, dataContext) {
            if (value) {
                var cls='percent';
                if (Number(value)<0) cls+=' negative';
                value=Number(value).toFixed(1);
                if (lang=='de') value=value.replace('.',',');
                return '<span class="' + cls + '">' + value + '<sup>%</sup></span>';
            } else
            return '';
        }

        function imageFormat(row, cell, value, columnDef, dataContext) {
            if (value) {
                return '<span class="image"><img src="' + value + '"></span>';
            } else
            return '';
        }

        function idFormat(row, cell, value, columnDef, dataContext) {
            if (value) {
                return '<span class="id">' + value.substr(0,19) + '</span>' +
                   '<span class="id">' + value.substr(18) + '</span>'
                 ;
            } else
                return '';
        }
    }


    static getEditor(column) {

        class TextEditor {

            constructor(args) {
console.log(args)
                var navOnLR = args.grid.getOptions().editorCellNavOnLRKeys;

                this.grid = args.grid;
                this.column = args.column;

                this.$input = $("<INPUT type=text class='text-input' />")
                    .appendTo(args.container)
                    //.on("keydown.nav", navOnLR ? handleKeydownLRNav : handleKeydownLRNoNav)
                    .focus()
                    .select();
            }
        
            destroy() {
                this.$input.remove();
            }
        
            focus() {
                this.$input.focus();
            }
        
            getValue() {
                return this.$input.val();
            }
        
            setValue(val) {
                this.$input.val(val);
            }
        
            loadValue(item) {
                this.defaultValue = item[this.column.field] || "";
                this.$input.val(this.defaultValue);
                this.$input[0].defaultValue = this.defaultValue;
                this.$input.select();
            }
        
            serializeValue() {
                return this.$input.val();
            }
        
            applyValue(item, state) {
                console.log('SAVE')
                item[this.column.field] = state;
            }
        
            isValueChanged() {
                return (!(this.$input.val() == "" && this.defaultValue == null)) && (this.$input.val() != this.defaultValue);
            }
        
            validate() {
                if (this.column.validator) {
                    var validationResults = this.column.validator(this.$input.val());
                    if (!validationResults.valid) {
                    return validationResults;
                    }
                }
        
                return {
                    valid: true,
                    msg: null
                };
            }
        }

        switch(column.format) {
            case 'String':
                return TextEditor;
            default:
                return null;
        }

    }

}

class RemoteTableModel extends RemoteModel {

    constructor(table_name) {
        super();
        this.datasource = '/sql/'+table_name+'/';
    }

    init(callback) {
        $.post(this.datasource + 'schema', ( result ) => {
            this.columns=result.result;
            callback(null);

        }).fail( (ev) => {

        });
    }
}

class SqlTable extends GridView {

    constructor(container, table_name) {
        super(container, new RemoteTableModel(table_name), {

        });
    }

    create_grid() {
        
        this.loading_indicator(true);
        this.datamodel.init( (err) => {
            super.create_grid();
        });
    }

}

class SqlView extends GridView {

    constructor(container, table_name) {
        var data=new RemoteTableModel(table_name);
        super(container, data, {
            
        });
    }
}