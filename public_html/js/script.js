

function format_date(dt) {
    return dt;
}

function format_price(p) {
    return p+'&eur;'
}

function format_currency(p) {
    var val=Number(p).toFixed(2);
    if (lang=='de') val=val.replace('.',',');
    return val;
}

function load_page(url) {
    document.location = (url);
}

function reload_page() {
    location.reload(true);
}

function show_search() {
    $('#pos-buttons .buttons').hide();
    $('#pos-buttons .searchfield').show();
    $('.formfield-search').mousedown();
    $('#keyboard-wrapper').show();
}

function hide_search() {
    $('#pos-buttons .buttons').show();
    $('#pos-buttons .searchfield').hide();
    $('#keyboard-wrapper').hide();
}

jQuery(($) => {
    $('#pos-buttons button').each( function() {
        $(this).on('click', function() {
            var action = $(this).attr('x-action') || '';
            if (action.startsWith('/')) {
                if (action.endsWith('?') || action.endsWith('&')) {
                    var payload=btoa(JSON.stringify($(document).formdata()))
                    load_page(action + 'payload=' + payload);
                } else
                    load_page(action);

            } else if (action == 'search') {
                show_search();
            } else if (action == 'post-search') {
                var search=$('#pos-buttons .formfield-search').attr('value');
                if (search=='')
                    hide_search();
                else {
                    let re = /(.*)products/;
                    let baseurl = re.exec(document.location.href)[0];
                    load_page(baseurl+'/find_products?name=' + search);
                }
            } else if (action == 'post-form') {
                var form=$('form');
                $.post(form.attr('action'), form.formdata(), (result) => {
                    if (result.status == 'success') {
                        form.trigger('success', result);
                    } else {
                        form.trigger('error', result);
                    }
                }).fail( (ev) => {
                    form.trigger('fatal-error', ev);
                });
            } else {
                if (button_action)
                    button_action(action);
            }
        });
    });

    $('span.flag.flag-de').on('click', () => {
        Cookies.set('lang', 'en');
        reload_page();
    })
    $('span.flag.flag-gb').on('click', () => {
        Cookies.set('lang', 'de');
        reload_page();
    })

    $('#menu-wrapper .menu-button').on('click', () => {
        if ($('#menu-container').is(':visible')) {
            $('#menu-container').slideUp(200);
        } else {
            $('#menu-container').slideDown(200);
        }
    }); 

    $('body').on('mousedown', () => {
        $('#menu-container').slideUp(200);
    });

    $('#menu-container .menu-item').on('mousedown', function (ev)  {
        ev.stopPropagation();
    });
    $('#menu-container .menu-item').on('click', function (ev)  {
        if ($(this).attr('disabled')) {
            return;
        }

        $('#menu-container').slideUp(200);

        var action = $(this).attr('x-action') || '';
        if (action.startsWith('/')) {
            load_page(action);

        } else {
            if (action=='use-light-theme') {
                Cookies.set('theme', 'light');
                reload_page();
        
            } else if (action=='use-dark-theme') {
                Cookies.set('theme', 'dark');
                reload_page();
        
            } else if (button_action)
                button_action(action);
        }
    });
});