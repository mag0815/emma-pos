
JSON.old_parse=JSON.parse;
JSON.parse = function(str) {
    return JSON.old_parse(str, function(key, val) {
        if (val!==null && typeof val == 'object') {
            if (val['$date']) {
                var julian=val['$date'];
                return new Date( (julian-2440587.5)*86400000 );
            }
        }
        return val;
    });
}

Date.prototype.toJSON = function() {
    var julian = this.getTime()/86400000 + 2440587.5;
    return {"$date":  julian };
};