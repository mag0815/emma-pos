

function button_action(action) {
    var cmd=action.split('/',2);
    switch(cmd[0]) {

        default:
            console.log('dunno how to handle '+cmd[0] + ' with ' +cmd[1]);
    }

}

function cartlist_init() {
    jQuery(function($) {
        $('.cart-list-entry').on('click', function() {
            load_page($(this).attr('x-action'));
        });
    });
}

function cartview_init() {
    jQuery(function($) {
        $('.cart-view-item').on( 'click', function() {
            load_page($(this).attr('x-action'));
        });
    });
}

function cartcategories_init() {
    jQuery(function($) {
        $('#cart-product-list .cat-list-item').on('click', function() {
            load_page($(this).attr('x-action'));
        });
    });
}

function cartcatedit_init() {
    jQuery(function($) {
        
        $('form')
            .on('success', (ev,result) => {
                load_page(result.redirect);
            })
            .on('error', (result) => {

            })
            .on('fatal-error', (ev, xhr, cause) => {
            });

    });
}

function cartcatimage_init() {
    jQuery(function($) {
        
        $('form')
            .on('success', (ev,result) => {
                load_page(result.redirect);
            })
            .on('error', (result) => {

            })
            .on('fatal-error', (ev) => {

            });

    });
}


function cartannotate_init() {
    jQuery(function($) {
        console.log('init annotate')
        console.log($('form'))
        $('form')
            .on('success', (ev,result) => {
                console.log('redirect',result.redirect )
                load_page(result.redirect);
            })
            .on('error', (result) => {

            })
            .on('fatal-error', (ev, xhr, cause) => {
            });

    });
}

function cartprodedit_init() {
    jQuery(function($) {
        
        $('form')
            .on('success', (ev,result) => {
                load_page(result.redirect);
            })
            .on('error', (result) => {

            })
            .on('fatal-error', (ev, xhr, cause) => {
            });

    });
}

function cartproduct_init() {
    jQuery(function($) {
        
        $('form')
            .on('success', (ev,result) => {
                load_page(result.redirect);
            })
            .on('error', (ev,result) => {
                alert(result.stack)
            })
            .on('fatal-error', (ev, xhr, cause) => {
            });

        $('.formfield').on('change', () => {
            var total=$('.formfield[name="price"]').value() * $('.formfield[name="amount"]').value();
            var vat=total - (total / ( 1 + $('.formfield[name="vat"]').value()/100));
            $('.price-total').text(total.toFixed(2));
            $('.vat-total').text(vat.toFixed(2));
        });
    });
}

function cartcheckout_init() {
    jQuery(function($) {
        $($('#pos-buttons button')[2]).attr('disabled', 'disabled');

        var amount=Number($('.header span.cashback').text().replace(',','.'));
        $('.formfield').on('blur', function () {
            var cash=$(this).value();
            if (cash-amount >= 0) {
                $($('#pos-buttons button')[2]).removeAttr('disabled');
                $('.footer span.cashback').text(Number(cash-amount).toFixed(2).replace('.',','));
            } else {
                $($('#pos-buttons button')[2]).attr('disabled', 'disabled');
                $('.footer span.cashback').text('\u00a0');
            }
        });
    });
}

function cartcheckout2_init() {
    jQuery(function($) {
        
        $('.check-customer').hide();
        $('.check-customer-error').hide();
        $($('#pos-buttons button')[2]).attr('disabled', 'disabled');

        $('[name="pin"]').on('change', function() {
            var pin=$(this).value();
            var form=$('form');
            
            $('.check-customer').hide();
            $('.check-customer-error').hide();
            $($('#pos-buttons button')[2]).attr('disabled', 'disabled');

            if (pin.length == 6) {
                $.post('checkout2_check', form.formdata(), (result) => {
                    if (result.status=='failure') {
                        $('.check-customer-error').show();
                    } else {
                        $($('#pos-buttons button')[2]).removeAttr('disabled');
                        $('.check-customer').show();
                        $('.value.customer').text(result.customer);
                        $('.value.balance').text(result.balance);
                    }
                });
                
            }
        });


        $('form')
            .on('success', (ev,result) => {
                load_page(result.redirect);
            })
            .on('error', (ev,result) => {
                alert(result.stack)
            })
            .on('fatal-error', (ev, xhr, cause) => {
            });

    });
}

function cartcashout_init() {

    jQuery(function($) {

        $('.footer').hide();

        $('[name="pin"]').on('change', function() {
            var pin=$('[name="pin"]').value();
            var form=$('form');
            
            $($('#pos-buttons button')[2]).attr('disabled', 'disabled');

            if (pin.length == 6) {
                $.post('/admin/login-check', form.formdata(), (result) => {
                    if (result.status=='failure') {
                        $('.check-customer-error').show();
                    } else {
                        var balance=result.customer.balance;
                        
                        $('.text.customer').text(result.customer.name);

                        $('.current-saldo').text(format_currency(balance));
                        if (balance<0) $('.current-saldo').addClass('negative-saldo');
                        else $('.current-saldo').removeClass('negative-saldo');

                        $('.new-saldo').text(format_currency(balance));
                        if (balance<0) $('.new-saldo').addClass('negative-saldo');
                        else $('.new-saldo').removeClass('negative-saldo');

                        $('h3.cashout-pin').hide();
                        $('#cashout-pin-field').hide();
                        $('#cashout-remark-field').show();
                        $('#cashout-amount-field').show();
                        $('h3.cashout-header').show();

                        $('#cashout-amount-field .formfield').mousedown();

                        $('.footer').show();
                    }
                });
                
            }
        });

        $('[name="amount"]').on('change', function() {
            var amount=Number($('[name="amount"]').value());
            if (amount.toString() == 'NaN' || amount==0) {
                $($('#pos-buttons button')[2]).attr('disabled', 'disabled');
            } else {
                $($('#pos-buttons button')[2]).removeAttr('disabled');

                let balance = $('.current-saldo').text()
                balance = balance.replace(",",".");
                balance = Number(balance)
                $('.new-saldo').text(format_currency(balance-amount));
                if (balance-amount<0) $('.new-saldo').addClass('negative-saldo');
                else $('.new-saldo').removeClass('negative-saldo');
            }
        })

        $('#cashout-remark-field').hide();
        $('#cashout-amount-field').hide();
        $('h3.cashout-header').hide();

        $('form')
            .on('success', (ev,result) => {
                load_page(result.redirect);
            })
            .on('error', (ev,result) => {
                alert(result.stack)
            })
            .on('fatal-error', (ev, xhr, cause) => {
            });

    });
}

function cartcashin_init() {

    jQuery(function($) {

        $('.footer').hide();

        $('[name="pin"]').on('change', function() {
            var pin=$('[name="pin"]').value();
            var amount=$('[name="amount"]').value();
            var form=$('form');
            
            if (pin.length == 6) {
                $.post('/admin/login-check', form.formdata(), (result) => {
                    if (result.status=='failure') {
                        $('.check-customer-error').show();
                    } else {
                        var balance=result.customer.balance;
                        
                        if (amount>0)
                            $($('#pos-buttons button')[2]).removeAttr('disabled');
                        
                        $('.text.customer').text(result.customer.name);

                        $('.current-saldo').text(format_currency(balance));
                        if (balance<0) $('.current-saldo').addClass('negative-saldo');
                        else $('.current-saldo').removeClass('negative-saldo');

                        $('.new-saldo').text(format_currency(balance+amount));
                        if (balance+amount<0) $('.new-saldo').addClass('negative-saldo');
                        else $('.new-saldo').removeClass('negative-saldo');

                        $('h3.cashin-pin').hide();
                        $('#cashin-pin-field').hide();
                        $('#cashin-remark-field').show();
                        $('#cashin-amount-field').show();
                        $('h3.cashin-header').show();

                        $('#cashin-amount-field .formfield').mousedown();
                
                        $('.footer').show();
                    }
                });
            }
        });
        
        $('[name="amount"]').on('change', function() {
            var amount=Number($('[name="amount"]').value());
            if (amount.toString() == 'NaN' || amount==0) {
                $($('#pos-buttons button')[2]).attr('disabled', 'disabled');
            } else {
                $($('#pos-buttons button')[2]).removeAttr('disabled');

                let balance = Number($('.current-saldo').text().replace(',','.'))
                $('.new-saldo').text(format_currency(balance+amount));
                if (balance+amount<0) $('.new-saldo').addClass('negative-saldo');
                else $('.new-saldo').removeClass('negative-saldo');
            }
        })

        $('#cashin-remark-field').hide();
        $('#cashin-amount-field').hide();
        $('h3.cashin-header').hide();

        $('form')
            .on('success', (ev,result) => {
                load_page(result.redirect);
            })
            .on('error', (ev,result) => {
                alert(result.stack)
            })
            .on('fatal-error', (ev, xhr, cause) => {
            });

    });
}


function cashdeskcashout_init() {

    jQuery(function($) {

        $('.footer').hide();

        $('[name="pin"]').on('change', function() {
            var pin=$('[name="pin"]').value();
            var form=$('form');
            
            $($('#pos-buttons button')[2]).attr('disabled', 'disabled');

            if (pin.length == 6) {
                $.post('/admin/login-check', form.formdata(), (result) => {
                    if (result.status=='failure') {
                        $('.check-customer-error').show();
                    } else {
                        var balance=result.customer.balance;
                        
                        $('.text.customer').text(result.customer.name);

                        $('.current-saldo').text(format_currency(balance));
                        if (balance<0) $('.current-saldo').addClass('negative-saldo');
                        else $('.current-saldo').removeClass('negative-saldo');

                        $('.new-saldo').text(format_currency(balance));
                        if (balance<0) $('.new-saldo').addClass('negative-saldo');
                        else $('.new-saldo').removeClass('negative-saldo');

                        $('h3.cashout-pin').hide();
                        $('#cashout-pin-field').hide();
                        $('#cashout-remark-field').show();
                        $('#cashout-amount-field').show();
                        $('h3.cashout-header').show();

                        $('#cashout-amount-field .formfield').mousedown();

                        $('.footer').show();
                    }
                });
                
            }
        });

        $('[name="amount"]').on('change', function() {
            var amount=Number($('[name="amount"]').value());
            if (amount.toString() == 'NaN' || amount==0) {
                $($('#pos-buttons button')[1]).attr('disabled', 'disabled');
            } else {
                $($('#pos-buttons button')[1]).removeAttr('disabled');

                let balance = Number($('.current-saldo').val())
                $('.new-saldo').text(format_currency(balance-amount));
                if (balance-amount<0) $('.new-saldo').addClass('negative-saldo');
                else $('.new-saldo').removeClass('negative-saldo');
            }
        })

        $('#cashout-remark-field').hide();
        $('#cashout-amount-field').hide();
        $('h3.cashin-header').hide();

        $('form')
            .on('success', (ev,result) => {
                load_page(result.redirect);
            })
            .on('error', (ev,result) => {
                alert(result.stack)
            })
            .on('fatal-error', (ev, xhr, cause) => {
            });

    });
}

function cashdeskcashin_init() {

    jQuery(function($) {

        $('.footer').hide();

        $('[name="pin"]').on('change', function() {
            var pin=$('[name="pin"]').value();
            var amount=$('[name="amount"]').value();
            var form=$('form');
            
            if (pin.length == 6) {
                $.post('/admin/login-check', form.formdata(), (result) => {
                    if (result.status=='failure') {
                        $('.check-customer-error').show();
                    } else {
                        var balance=result.customer.balance;
                        
                        if (amount>0)
                            $($('#pos-buttons button')[2]).removeAttr('disabled');
                        
                        $('.text.customer').text(result.customer.name);

                        $('.current-saldo').text(format_currency(balance));
                        if (balance<0) $('.current-saldo').addClass('negative-saldo');
                        else $('.current-saldo').removeClass('negative-saldo');

                        $('.new-saldo').text(format_currency(balance+amount));
                        if (balance+amount<0) $('.new-saldo').addClass('negative-saldo');
                        else $('.new-saldo').removeClass('negative-saldo');

                        $('h3.cashin-pin').hide();
                        $('#cashin-pin-field').hide();
                        $('#cashin-remark-field').show();
                        $('#cashin-amount-field').show();
                        $('h3.cashin-header').show();

                        $('#cashin-amount-field .formfield').mousedown();
                
                        $('.footer').show();
                    }
                });
            }
        });
        
        $('[name="amount"]').on('change', function() {
            var amount=Number($('[name="amount"]').value());
            if (amount.toString() == 'NaN' || amount==0) {
                $($('#pos-buttons button')[1]).attr('disabled', 'disabled');
            } else {
                $($('#pos-buttons button')[1]).removeAttr('disabled');

                let balance = Number($('.current-saldo').val())
                $('.new-saldo').text(format_currency(balance+amount));
                if (balance+amount<0) $('.new-saldo').addClass('negative-saldo');
                else $('.new-saldo').removeClass('negative-saldo');
            }
        })

        $('#cashin-remark-field').hide();
        $('#cashin-amount-field').hide();
        $('h3.cashin-header').hide();

        $('form')
            .on('success', (ev,result) => {
                load_page(result.redirect);
            })
            .on('error', (ev,result) => {
                alert(result.stack)
            })
            .on('fatal-error', (ev, xhr, cause) => {
            });

    });
}

function cashdeskwithdraw_init() {




    jQuery(function($) {        

        $('[name="amount"]').on('change', function() {
            var amount=Number($('[name="amount"]').value());
            if (amount.toString() == 'NaN' || amount==0) {
                $($('#pos-buttons button')[1]).attr('disabled', 'disabled');
            } else {
                $($('#pos-buttons button')[1]).removeAttr('disabled');
            }
        })

        $('form')
            .on('success', (ev,result) => {
                load_page(result.redirect);
            })
            .on('error', (result) => {
            })
            .on('fatal-error', (ev, xhr, cause) => {
            });
    });

}

function cashdeskdeposit_init() {

    jQuery(function($) {        

        $('[name="amount"]').on('change', function() {
            var amount=Number($('[name="amount"]').value());
            if (amount.toString() == 'NaN' || amount==0) {
                $($('#pos-buttons button')[1]).attr('disabled', 'disabled');
            } else {
                $($('#pos-buttons button')[1]).removeAttr('disabled');
            }
        })

        $('form')
            .on('success', (ev,result) => {
                load_page(result.redirect);
            })
            .on('error', (result) =>    {
            })
            .on('fatal-error', (ev, xhr, cause) => {
            });
    });

}


function coop_member_edit_init() {

    function enable_button() {
        var id=String($('[name="id"]').value());
        var name=String($('[name="name"]').value());

        if (id.length<3) return disable_button();
        if (name.length<2) return disable_button();

        $($('#pos-buttons button')[1]).removeAttr('disabled');
    }

    function disable_button() {
        $($('#pos-buttons button')[1]).attr('disabled', 'disabled');
    }

    $('form')
        .on('success', (ev,result) => {
            load_page(result.redirect);
        })
        .on('error', (result) => {

        })
        .on('fatal-error', (ev, xhr, cause) => {
        });

    $('[name="new_pin"]').on('change', function() {
        var pin=String($('[name="new_pin"]').value());
        $('#member-edit-pin_error-field').hide();
        disable_button();
        if (pin=='') {
            enable_button();
        } else {
            if (pin.length == 6) {
                $.post('check-pin', {pin: pin}, (result) => {
                    console.log(result)
                    if (result.result=='found') {
                        $('#member-edit-pin_error-field').show();
                    } else {
                        enable_button()
                    }
                });
                
            }
        }
    });

    $('[name="id"]').on('change', function() {
        var id=String($('[name="id"]').value());
        $('#member-edit-id_error-field').hide();
        disable_button();
        if (id.length>=3) {
            $.post('check-id', {id: id}, (result) => {
                console.log(result)
                if (result.result=='found') {
                    $('#member-edit-id_error-field').show();
                } else {
                    enable_button()                }
            }); 
        }
    });

    enable_button();

}


function coop_member_cashin_init() {

    $('form')
        .on('success', (ev,result) => {
            load_page(result.redirect);
        })
        .on('error', (result) => {

        })
        .on('fatal-error', (ev, xhr, cause) => {
        });

    $('[name="amount"]').on('change', function() {
        var amount=$('[name="amount"]').value();
        if (amount>0) {
            $($('#pos-buttons button')[1]).removeAttr('disabled');
        } else {
            $($('#pos-buttons button')[1]).attr('disabled', 'disabled');
        }
    });
}


function coop_invoice_init() {

    function enable_button() {
        var amount=$('[name="amount"]').value();
        if (amount<=0) return disable_button();
        var remark=$('[name="remark"]').value();
        if (remark.length<=0) return disable_button();

        $($('#pos-buttons button')[1]).removeAttr('disabled');
    }
    function disable_button() {
        $($('#pos-buttons button')[1]).attr('disabled', 'disabled');
    }

    $('form')
        .on('success', (ev,result) => {
            load_page(result.redirect);
        })
        .on('error', (result) => {

        })
        .on('fatal-error', (ev, xhr, cause) => {
        });

    $('[name="amount"]').on('change', enable_button);
    $('[name="remark"]').on('change', enable_button);

}




function delivery_list_init() {
    $(".data-table tr").on("click", function() {
        load_page('delivery/'+this.id+'/view')
    })
}

function delivery_view_init() {
}

function delivery_edit_init() {

    var timer_id = false;
    var changeset = {}

    function run_update() {
        let id=$('[name="id"]').value();
        changeset.id = id;
        let param = changeset;
        changeset = {}
        timer_id = false;
        $.post("update", param, 
            (result) => {
                console.log(result);
                enable_buttons();                
            }
        )
    }

    function disable_buttons() {
        $($('#pos-buttons button')[1]).attr('disabled', 'disabled');
        $($('#pos-buttons button')[2]).attr('disabled', 'disabled');
        $('[name="search"] button').attr('disabled', 'disabled');
    }
    function enable_buttons() {
        $($('#pos-buttons button')[1]).removeAttr('disabled');        
        $($('#pos-buttons button')[2]).removeAttr('disabled');        
        $('[name="search"] button').removeAttr('disabled');
    }

    $('[name="remark"]').on('change', function() {
        var remark=$('[name="remark"]').value();
        changeset.remark = remark;
        
        disable_buttons();
        if (timer_id) clearTimeout(timer_id);
        timer_id = setTimeout(run_update, 500)
    });

    $('[name="search"] button').on('click', function() {
        var search=$('[name="search"]').value();
        if (search!='') {
            let url = 'search?q=' + encodeURI(search)
            load_page(url);    
        }
        return false;
    });

    $('[name="price"]').on('change', function() {
        let item_div = this.parentElement.parentElement.parentElement; 
        let id = item_div.id;
        if (!changeset[id]) {
            changeset[id] = {}
        }
        changeset[id].new_price = $(this).value();
        disable_buttons();
        if (timer_id) clearTimeout(timer_id);
        timer_id = setTimeout(run_update, 500)
    })
    $('[name="amount"]').on('change', function() {
        let item_div = this.parentElement.parentElement.parentElement; 
        let id = item_div.id;
        if (!changeset[id]) {
            changeset[id] = {}
        }
        changeset[id].amount = $(this).value();
        disable_buttons();
        if (timer_id) clearTimeout(timer_id);
        timer_id = setTimeout(run_update, 500)
    })

}


function delivery_search_init() {
    $('[name="search"] button').on('click', function() {
        var search=$('[name="search"]').value();
        if (search!='') {
            let url = 'search?q=' + encodeURI(search)
            load_page(url);    
        }
        return false;
    });

    $(".data-table tr").on("click", function() {
        load_page('add?product='+this.id)
    })

}
