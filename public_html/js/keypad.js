


function init_keypad() {

    jQuery( function($) {

        function keydown(key) {
            if (key == '\u00a0')
                return;

            var el=$('.formfield.focused')[0];
            if (el === undefined)
                return;

            var chars;
            switch($(el).attr('type')) {
                case 'pin': 
                case 'keypad-pin': 
                    chars= /[0-9]/; break; 
                case 'percent':
                case 'currency':
                case 'number': 
                case 'keypad-percent':
                case 'keypad-currency':
                case 'keypad-number': 
                    chars= /[0-9-,.]/; break; 
                default: 
                    chars= /./; break; 
            }
            var e=chars.exec(key);
            if (key.length!=1 || e == null) {
                return;
            }
            if (key==' ')
                key='\u00a0';

            if ($(el).hasClass('selected')) {
                txt='';
            } else {
                txt=$('.text', el).text();
            }

            var len=Number($(el).attr('length'));
            if (txt.length>=len)
                return;

            txt=txt+key;
            $('.text', el).text(txt);
            $(el).removeClass('selected empty').addClass('typing').attr('value',txt);
            
            $(el).trigger('change');
        }

        function erase() {
            var el=$('div.formfield.focused')[0];
            if (el === undefined)
                return;
            
            if ($(el).hasClass('selected')) {
                $('.text', el).text('');
                $(el).attr('value','');
            } else {
                var txt=$('.text', el).text();
                if (txt.length>0)
                    txt=txt.substr(0,txt.length-1);
                $('.text', el).text(txt);
                $(el).attr('value', txt);
                
            }
            $(el).removeClass('selected').addClass('typing');

            $(el).trigger('change');
        }

        function enter() {
            var el=$('div.formfield.focused')[0];
            if (el === undefined)
                return;

            // HACK: submit search-field
            if ($(el).hasClass('formfield-search')) {
                $("button[x-action='post-search']").click();
                return;
            }
            var fields=$('div.formfield');
            for (var n=0; n<fields.length; ++n) {
                if (fields[n] === el) {
                    if (n == fields.length-1) {
                        blur();
                        focus(el);
                        //TODO: trigger default button action 
                        console.log($(document).formdata());
                    } else {
                        focus(fields[n+1]);
                    }
                    break;
                }
            }
        }

        function tab() {
            var el=$('div.formfield.focused')[0];
            if (el === undefined)
                return;
            var fields=$('div.formfield');
            for (var n=0; n<fields.length; ++n) {
                if (fields[n] === el) {
                    if (n == fields.length-1) {
                        blur();
                    } else {
                        focus(fields[n+1]);
                    }
                    break;
                }
            }
        }

        let first_field = $('.formfield-input').parent()[0];

        // focus first field (and show keypad meanwhile), but not the search-field
        if ($(first_field).attr('name') != 'search')
            focus(first_field);

        function focus(el) {
            if (typeof el== 'undefined' || el.length==0)
                return;
            if ($(el).hasClass('focused'))
                return;

            last_field = $('.formfield.focused').removeClass('focused selected typing');

            var element_top = $(el).position().top;
            var element_height = $(el).height();

            var type=$(el).attr('type').toLowerCase();
            switch(type) {
                case 'keypad-pin':
                case 'keypad-number':
                case 'keypad-int':   
                case 'keypad-currency':
                case 'keypad-percent':                 
                    if (type == 'pin' || type=='int') {
                        $('.keypad-button-7, .keypad-button-12').attr('disabled','disabled');
                    } else {
                        $('.keypad-button-7, .keypad-button-12').removeAttr('disabled');
                    }
                    if ($('#keyboard-wrapper').is(':visible'))
                        $('#keyboard-wrapper').hide();
                    if (!$('#keypad-wrapper').is(':visible'))
                        $('#keypad-wrapper').show();

                    $(el).addClass('focused selected')
                    if ($(el).value() == '')
                        $(el).removeClass('selected').addClass('typing');
                    $(el).trigger('focus');

                    break; 

                case 'pin':
                case 'number':
                case 'int':   
                case 'currency':
                case 'percent':  
                    if ($('#keypad-wrapper').is(':visible'))
                        $('#keypad-wrapper').hide();
                    if (!$('#keyboard-wrapper').is(':visible'))
                        $('#keyboard-wrapper').show();

                    $('#keyboard-wrapper').removeClass('shift').addClass('symbol');

                    $(el).addClass('focused selected');
                    if (type == 'datalist') {
                        filter_datalist(el);
                    }
                    $(el).addClass('focused selected')
                    if ($(el).value() == '')
                        $(el).removeClass('selected').addClass('typing');
                    $(el).trigger('focus');

                    break; 

                case 'text':
                case 'password':
                case 'email':
                case 'datalist':
                    if ($('#keypad-wrapper').is(':visible'))
                        $('#keypad-wrapper').hide();
                    if (!$('#keyboard-wrapper').is(':visible'))
                        $('#keyboard-wrapper').show();

                    $('#keyboard-wrapper').addClass('shift').removeClass('symbol');

                    $(el).addClass('focused selected');
                    if (type == 'datalist') {
                        filter_datalist(el);
                    }
                    $(el).addClass('focused selected')
                    if ($(el).value() == '')
                        $(el).removeClass('selected').addClass('typing');
                    $(el).trigger('focus');

                    break; 

                case 'select':
                    if ($('#keypad-wrapper').is(':visible'))
                        $('#keypad-wrapper').hide();
                    if (!$('#keyboard-wrapper').is(':visible'))
                        $('#keyboard-wrapper').show();

                    $('#keyboard-wrapper').addClass('shift').removeClass('symbol');

                    $(el).addClass('focused selected').trigger('focus');
                    var first = $('.formfield-select-value-list-item[selected]', el)[0];
                    if (first)
                        first.scrollIntoView();

                    element_height += $('.formfield-select-value-list').height();
                    break;
            }
            //TODO: find a more generic approach to find scrolling box
            var c=$('.pos-content-inner');
            var scroll = c.scrollTop();
            //TODO: find a better way to handle select lists
            //console.log(scroll, element_top)
            if (scroll>element_top) scroll=element_top;
            

            //c.scrollTop(scroll);

            unfocus(last_field);
        }

        function filter_datalist(el) {


        }

        function unfocus($el) {
            if ($el.length<1) return;

            //TODO: format last_field

            $el.removeClass('focused selected typing');
            $el.trigger('blur');
        }

        function blur() {
            unfocus($('.formfield.focused'));
            
            if ($('#keypad-wrapper').is(':visible'))
                $('#keypad-wrapper').hide();
            if ($('#keyboard-wrapper').is(':visible'))
                $('#keyboard-wrapper').hide();
        }
         
        $('div.formfield').on('validation-error', function() {
            if (!$(this).hasClass('focused')) {
                focus(this);
            }
        });

        $('div.formfield').on('blur', function() {
            if ($(this).hasClass('focused')) {
                $('.keypad-field.focused').removeClass('focused selected typing');
            } else {
                var type=$(this).attr('type');
                if (type == 'currency' || type == 'keypad-currency') {
                    var val = $(this).value();
                    if (val==0) val='';
                    else val=val.toFixed(2);
                    $(this).value(val);
                }
                if (!$(this).validate()) {
                    $(this).trigger('validation-error');
                }
            }         
        });

        $('.formfield-select-value-list-item').on('click', function() {
            var input=$(this).parent().parent().parent();
            $('.formfield-select-value-list-item', input).removeAttr('selected');
            $(this).attr('selected', 'selected');

            input.attr('value', $(this).attr('value'));
            $('.text', input).text($(this).text().trim());
            enter();
        });

        $('#keypad-wrapper .keypad-button').on('click', function() {
            var key=$(this).attr('x-key');

            if (key=='⇐') 
                erase();
            else if (key=='↲') 
                enter();
            else 
                keydown(key);
        });

        $('#keyboard-wrapper .char-button').on('click', function () {
            if ($(this).hasClass('shift-key')) {
                $('#keyboard-wrapper').removeClass('symbol');
                if ($('#keyboard-wrapper').hasClass('shift')) {
                    $('#keyboard-wrapper').removeClass('shift');
                } else {
                    $('#keyboard-wrapper').addClass('shift');
                }
            }
            else if ($(this).hasClass('symbol-key')) {
                $('#keyboard-wrapper').removeClass('shift');
                if ($('#keyboard-wrapper').hasClass('symbol')) {
                    $('#keyboard-wrapper').removeClass('symbol');
                } else {
                    $('#keyboard-wrapper').addClass('symbol');
                }
            }
            else if ($(this).hasClass('enter-key')) {
                enter();
            }
            else if ($(this).hasClass('backspace-key')) {
                erase();
            }
            else if ($(this).hasClass('space-key')) {
                keydown(' ');

            } else {
                var key;
                if ($('#keyboard-wrapper').hasClass('shift')) {
                    key = $('.shift',this).text();
                    $('#keyboard-wrapper').removeClass('shift')
                } else if ($('#keyboard-wrapper').hasClass('symbol')) {
                    key = $('.symbol',this).text();
                } else {
                    key = $('.normal',this).text();
                }
                if (key != ' ')
                    keydown(key);
            }

        });

        $(document).on('keydown', (event) => {
            switch(event.key) {
                case 'Backspace':
                    erase();
                    break;
                case 'Enter':
                    enter();
                    break;
                default:
                    keydown(event.key);
                    break;
            }
        });

        $('div.formfield').on('mousedown', function(event) {
            focus(this);
            event.stopPropagation();
            event.preventDefault();
        });

    });
}



(function($) {
    const cropper_canvas = {
        width: 128,
        height: 96
    };

    var old_val = $.fn.val;

    $.fn.value = function(val) {
        if (typeof val != 'undefined') {
            return this.each(
                function() {
                    if (this.nodeName.toUpperCase() == 'DIV') {
                        $('.text', this).text(val);
                    } else if (this.nodeName.toUpperCase() == 'IMG') {
                        var cropper=$(this).data('cropper');
                        cropper.replace(val);
                    } else 
                        old_val.apply($(this), [val]);
                }
            );        
        } else {
            if ($(this).prop('tagName').toUpperCase() == 'DIV') {
                var val = $(this).attr('value') || '';
                val = val.replace(/\u00a0/g,' ');
                val=val.trim();
                var type=this.attr('type');
                if (type == 'number' || type== 'percent' || type == 'currency' || type == 'keypad-number' || type== 'keypad-percent' || type == 'keypad-currency') {
                    val=Number(val.replace(/,/g,'.'));
                }
                return val;
            } else if (this.prop('tagName').toUpperCase() == 'IMG') {
                    var cropper=this.data('cropper');
                    var url = cropper.getCroppedCanvas(cropper_canvas).toDataURL();
                    return url;
            } else 
                return old_val.apply(this);
        }
    }

    $.fn.validate = function() {
        var result=true;
        this.each( function() {
            if ($(this).attr('required') && $(this).value()=='') {
                $(this).addClass('validation-error');
                if (result)
                    $(this).trigger('validation-error');
                result=false;
                } else {
                $(this).removeClass('validation-error');
            }
        })
        return result;
    }

    $.fn.formdata = function() {
        var values={};
        this.each( function() {
            $('.formfield', this).each(
                function() {
                    values[$(this).attr('name')] = $(this).value();
                }
            )
        });
        return values;
    }

})($);