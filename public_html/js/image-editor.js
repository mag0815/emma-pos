


(function($) {
    var defaults;
    $.event.fix = (function(originalFix) {
      return function(event) {
        event = originalFix.apply(this, arguments);
        if (event.type.indexOf('copy') === 0 || event.type.indexOf('paste') === 0) {
          event.clipboardData = event.originalEvent.clipboardData;
        }
        return event;
      };
    })($.event.fix);
    defaults = {
      callback: $.noop,
      matchType: /image.*/
    };
    return $.fn.pasteImageReader = function(options) {
      if (typeof options === "function") {
        options = {
          callback: options
        };
      }
      options = $.extend({}, defaults, options);
      return this.each(function() {
        var $this, element;
        element = this;
        $this = $(this);
        return $this.bind('paste', function(event) {
          var clipboardData, found;
          found = false;
          clipboardData = event.clipboardData;
          return Array.prototype.forEach.call(clipboardData.types, function(type, i) {
            var file, reader;
            if (found) {
              return;
            }
            if (type.match(options.matchType) || clipboardData.items[i].type.match(options.matchType)) {
              file = clipboardData.items[i].getAsFile();
              reader = new FileReader();
              reader.onload = function(evt) {
                return options.callback.call(element, {
                  dataURL: evt.target.result,
                  event: evt,
                  file: file,
                  name: file.name
                });
              };
              reader.readAsDataURL(file);
              //snapshoot();
              return found = true;
            }
          });
        });
      });
    };
  })(jQuery);

  


function image_edit_init() {

    jQuery(function($) {

        var cropper;
        var options={
            aspectRatio: 192/144,
            dragMode: 'move',
            ready: () => {
              cropper.zoom(-0.25);
            }
        };

        $('#image').cropper(options);
        cropper = $('#image').data('cropper');

        $('#image-editor button').on('click', function(ev) {
            switch ($(this).attr('x-action')) {
                case 'zoom-in':
                    cropper.zoom(0.1);
                    break;
                case 'zoom-out':
                    cropper.zoom(-0.1);
                    break;
                case 'rotate-left':
                    cropper.rotate(-11.25);
                    break;
                case 'rotate-right':
                    cropper.rotate(11.25);
                    break;
                case 'flip-horizontal':
                    cropper.scaleX(- cropper.getData().scaleX);
                    break;
                case 'flip-vertical':
                    cropper.scaleY(- cropper.getData().scaleY);
                    break;
                case 'import':
                    $('#file-input').click();
                    break;
                case 'camera':
                    show_camera();
                    break;
            }
            ev.preventDefault();
            ev.stopPropagation();
        });

        $('#photo-camera button').on('click', () => {
          take_photo();
        });

        // paste from clipboard
        $(document).pasteImageReader(function (result) {
            cropper.replace(result.dataURL);
        });

        // file upload
        var uploadedImageURL;
        $('#file-input').on('change', function() {
            var files = this.files;
            var file;
      
            if (cropper && files && files.length) {
              file = files[0];
      
              if (/^image\/\w+/.test(file.type)) {
                uploadedImageName = file.name;
      
                if (uploadedImageURL) {
                  URL.revokeObjectURL(uploadedImageURL);
                }
                uploadedImageURL = URL.createObjectURL(file);
                cropper.replace(uploadedImageURL);
              }
            } 
        });

        var localMediaStream = null;

        function show_camera() {
          $('#photo-camera').removeClass('hidden');
          $('video').height($('#image-editor .image').height());
          $('#image-editor').addClass('hidden');

          if (localMediaStream == null) {
            navigator.getUserMedia({video: { facingMode: 'environment', } }, function(stream) {
              $('#video').attr('src',  window.URL.createObjectURL(stream));
              localMediaStream = stream;
              
          }, function () {
              $('#image-editor').addClass('hidden');
              $('#photo-camera').removeClass('hidden');  
            }
            );
          }
        }

        function take_photo() {
          var video=$('video')[0];

          var canvas=document.createElement('canvas');
          canvas.width=video.videoWidth;
          canvas.height=video.videoHeight;

          var ctx=canvas.getContext('2d');
          ctx.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
          var img=canvas.toDataURL('image/png');

          $('#photo-camera').addClass('hidden');  
          $('#image-editor').removeClass('hidden');
          
          cropper.replace(img);
        }

    });
}


