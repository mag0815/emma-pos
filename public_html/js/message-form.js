function init_keypad() {
}

function messageform_init() {

    $(function() {

        $('form')
        .on('success', (ev,result) => {
            load_page(result.redirect);
        })
        .on('error', (ev,result) => {
            alert(result.stack)
        })
        .on('fatal-error', (ev, xhr, cause) => {
        });

        $('#keyboard-wrapper').show();
        $('#keyboard-wrapper').addClass('shift');

        $(window).on('keydown', (ev) => {
            switch(ev.key) {
                case 'Shift':
                case 'Control':
                    break;
                case 'Enter':
                    enter();
                    break;
                case 'ArrowLeft': 
                    left(); 
                    break;
                case 'ArrowRight': 
                    right(); 
                    break;
                case 'ArrowUp': 
                    up(); 
                    break;
                case 'ArrowDown': 
                    down(); 
                    break;
                case 'Backspace':
                    backspace();
                    break;
                default: 
                var key=ev.key;
                if (key.length==1)
                    keydown(key);
            }
        });


        $('#keyboard-wrapper .char-button').on('click', function () {
            if ($(this).hasClass('shift-key')) {
                $('#keyboard-wrapper').removeClass('symbol');
                if ($('#keyboard-wrapper').hasClass('shift')) {
                    $('#keyboard-wrapper').removeClass('shift');
                } else {
                    $('#keyboard-wrapper').addClass('shift');
                }
            }
            else if ($(this).hasClass('symbol-key')) {
                $('#keyboard-wrapper').removeClass('shift');
                if ($('#keyboard-wrapper').hasClass('symbol')) {
                    $('#keyboard-wrapper').removeClass('symbol');
                } else {
                    $('#keyboard-wrapper').addClass('symbol');
                }
            }
            else if ($(this).hasClass('enter-key')) {
                enter();
            }
            else if ($(this).hasClass('backspace-key')) {
                backspace();
            }
            else if ($(this).hasClass('up-key')) {
                up();
            }
            else if ($(this).hasClass('down-key')) {
                down();
            }
            else if ($(this).hasClass('left-key')) {
                left();
            }
            else if ($(this).hasClass('right-key')) {
                right(' ');
            }
            
            else if ($(this).hasClass('space-key')) {
                keydown(' ');
                                        
            } else {
                var key;
                if ($('#keyboard-wrapper').hasClass('shift')) {
                    key = $('.shift',this).text();
                    $('#keyboard-wrapper').removeClass('shift')
                } else if ($('#keyboard-wrapper').hasClass('symbol')) {
                    key = $('.symbol',this).text();
                } else {
                    key = $('.normal',this).text();
                }
                if (key != ' ')
                    keydown(key);
            }

        });

        var el=$('.formfield-input');
        var txt='';
        for (var n=0; n<txt.length; ++n) {
            ch=txt.substr(n,1);
            if (ch=='\n') {
                el.append($('<span class="nl"/><span class="br"></span>'))
            } else
                el.append($('<span>' + ch + '</span>'))
        }
        el.append('<span class="nl"/>')

        var c=$('span',el); $(c[c.length-1]).addClass('cursor');

        function keydown(key) {
            if ((el.attr('value')||'').length >= 1024)
                return;
            var c=$('.cursor', el);
            c.before($('<span>' + key + '</span>'));
            adjust_scroll_up();
            set_value();
            if (key=='.')
                $('#keyboard-wrapper').addClass('shift');

        }

        function enter() {
            var c=$('.cursor', el);
            c.before($('<span class="nl"/><span class="br"></span>'));
            adjust_scroll_up();
            set_value();
            $('#keyboard-wrapper').addClass('shift');

        } 

        function left() {
            var c=$('.cursor', el);
            var n=c.prev();
            if (n.hasClass('br'))
                n=n.prev();
            if (n.length>0) {
                c.removeClass('cursor');
                n.addClass('cursor');
            }
            adjust_scroll_down();
        }

        function right() {
            var c=$('.cursor', el);
            var n=c.next();
            if (n.hasClass('br'))
                n=n.next();
            if (n.length>0) {
                c.removeClass('cursor');
                n.addClass('cursor');
            }
            adjust_scroll_up();
        }

        function adjust_scroll_down() {
            var c=$('.cursor', el);
            var offset = c.offset();
            var el_offset = el.offset();
            var height = c.outerHeight();


            var top_margin = (el.outerHeight()-el.height())/2;
            if ( (offset.top-el_offset.top-top_margin - height) <= 0) {
                el.scrollTop(el.scrollTop()-height);
                offset = c.offset();
            }
        }

        function adjust_scroll_up() {
            var c=$('.cursor', el);
            var offset = c.offset();
            var el_offset = el.offset();

            var height = c.outerHeight();
            var el_height = el.innerHeight();

            var margin = (el.outerHeight()-el.height());
            if ( el_offset.top + el_height - margin < offset.top + 2*height) {
                el.scrollTop(el.scrollTop()+height)
                offset = c.offset();
            }
        }

        function up() {
            var c=$('.cursor', el);
            var offset = c.offset();
            var el_offset = el.offset();
            var height = c.outerHeight();


            var top_margin = (el.outerHeight()-el.height())/2;
            if ( (offset.top-el_offset.top-top_margin - height) <= 0) {
                el.scrollTop(el.scrollTop()-height);
                offset = c.offset();
            }

            var n=$(document.elementFromPoint(offset.left, offset.top-4));
            if (n.parent().is(el) ) {
                c.removeClass('cursor');
                n.addClass('cursor');
            } else if (n.is(el)) {
                n=c;
                while (n.length>0) {
                    if (n.hasClass('nl')) {
                        c.removeClass('cursor');
                        n.addClass('cursor');
                        break;
                    }
                    n=n.prev();
                } 
            }
        }

        function down() {
            var c=$('.cursor', el);
            var offset = c.offset();
            var el_offset = el.offset();

            var height = c.outerHeight();
            var el_height = el.innerHeight();

            var margin = (el.outerHeight()-el.height());
            if ( el_offset.top + el_height - margin < offset.top + 2*height) {
                el.scrollTop(el.scrollTop()+height)
                offset = c.offset();
            }

            var n=$(document.elementFromPoint(offset.left, offset.top+height+4));
            if (n.parent().is(el) ) {
                c.removeClass('cursor');
                n.addClass('cursor');
            } else if (n.is(el)) {
                var el_offset=el.offset();                    
                var n=$(document.elementFromPoint(el_offset.left+6, offset.top+height+4));
                if (!el.is(n)) {
                    while (n.length>0) {
                        if (n.hasClass('nl')) {
                            c.removeClass('cursor');
                            n.addClass('cursor');
                            break;
                        }
                        n=n.next();
                    } 
                }
            }
        }

        function backspace() {
            var c=$('.cursor', el);
            var n=c.prev();
            if (n.length>0) {
                if (n.hasClass('br')) {
                    n.remove();
                    backspace();
                } else {
                    n.remove();
                }
            }                 
            adjust_scroll_down();   
            set_value();      
        }

        function set_value() {
            var chars = $('span',el);
            var value='';
            for (var n=0; n<chars.length; ++n) {
                var char=$(chars[n]);
                if (char.hasClass('br')) {
                    value += '\n';
                }
                else if (!char.hasClass('nl')) {
                    value += char.text();
                }
            }
            el.parent().attr('value', value);
            $('.charsleft').text(1024-value.length);
        }
    });
}




(function($) {
    const cropper_canvas = {
        width: 128,
        height: 96
    };

    var old_val = $.fn.val;

    $.fn.value = function(val) {
        if (typeof val != 'undefined') {
            return this.each(
                function() {
                    if (this.nodeName.toUpperCase() == 'DIV') {
                        $('.text', this).text(val);
                        
                    } else if (this.nodeName.toUpperCase() == 'IMG') {
                        var cropper=$(this).data('cropper');
                        cropper.replace(val);
                    } else 
                        old_val.apply($(this), [val]);
                }
            );        
        } else {
            if ($(this).prop('tagName').toUpperCase() == 'DIV') {
                var val = $(this).attr('value') || '';
                val = val.replace(/\u00a0/g,' ');
                val=val.trim();
                var type=this.attr('type');
                if (type == 'number' || type== 'percent' || type == 'currency' || type == 'keypad-number' || type== 'keypad-percent' || type == 'keypad-currency') {
                    val=Number(val.replace(/,/g,'.'));
                }
                return val;
            } else if (this.prop('tagName').toUpperCase() == 'IMG') {
                    var cropper=this.data('cropper');
                    var url = cropper.getCroppedCanvas(cropper_canvas).toDataURL();
                    return url;
            } else 
                return old_val.apply(this);
        }
    }

    $.fn.validate = function() {
        var result=true;
        this.each( function() {
            if ($(this).attr('required') && $(this).value()=='') {
                $(this).addClass('validation-error');
                if (result)
                    $(this).trigger('validation-error');
                result=false;
                } else {
                $(this).removeClass('validation-error');
            }
        })
        return result;
    }

    $.fn.formdata = function() {
        var values={};
        this.each( function() {
            $('.formfield', this).each(
                function() {
                    values[$(this).attr('name')] = $(this).value();
                }
            )
        });
        return values;
    }

})($);