 const express = require('express');
var model = require('../model');

const schemata = {
    tables: {
        permissions: {
            token: 'String',
            roles: 'String'
        },
        customer: {
            id: 'Id',
            pin: 'Integer',
            name: 'String',
            balance: 'Currency',
            roles: 'String'
        },
        messages: {
            id: 'Id',
            date: 'Date',
            message: 'String',
            sticky: 'Boolean',
            urgent: 'Boolean'
        },
        coop_invoices: {
            id: 'Id',
            date: 'Date',
            distributer: 'String',
            description: 'String',
            remark: 'String'
        },
        coop_deliveries: {
            id: 'Id',
            date: 'Date',
            distributer: 'String',
            invoice: 'coop_invoices-Id'
        },
        coop_journal: {
            date: 'Date',
            type: 'String',
            customer: 'customer-Id',
            amount: 'Currency',
            vat: 'Currency',
            description: 'String',
            remark: 'String'
        },
        shop_cashdesk: {
            date: 'Date',
            cart: 'shop_carts-Id',
            amount: 'Currency',
            vat: 'Currency',
            type: 'String'
        },
        shop_categories: {
            id: 'Id',
            sort_order: 'Integer',
            category: 'shop_categories-Id',
            name: 'String',
            image: 'Image'
        },
        shop_journal: {
            date: 'Date',
            cart: 'shop_carts-Id',
            amount: 'Currency',
            vat: 'Currency',
            type: 'String',
            desription: 'String',
            remark: 'String'
        },
        shop_products: {
            id: 'Id',
            category: 'shop_categories-Id',
            name: 'String',
            description: 'String',
            image: 'Image',
            vendor: 'String',
            commissioner: 'customer-Id',
            commission: 'Currency',
            distributer: 'String',
            distributer_sku: 'String',
            distributer_price: 'Currency',
            barcode: 'String',
            price: 'Currency',
            vat: 'Percent',
            stock: 'Number',
            unit: 'String',
            stock_warning_level: 'Number',
            packing_unit: 'Number'
        },
        shop_cartitems: {
            id: 'Id',
            date: 'Date',
            cart: 'shop_carts-Id',
            state: 'String',
            product: 'shop_products_Id',
            name: 'String',
            amount: 'Number',
            price: 'Currency',
            vat: 'Percent'
        },
        coop_orders: {
            id: 'Id',
            date: 'Date',
            state: 'String',
            customer: 'customer-Id',
            delivery: 'coop_deliveries-Id',
            distributer: 'String',
            distributer_sku: 'String',
            name: 'String',
            price: 'Currency',
            amount: 'Number',
            total: 'Currency',
            remark: 'String',
            shop_product: 'shop_products-Id'
        },
        shop_carts: {
            id: 'Id',
            date: 'Date',
            state: 'String',
            item_count: 'Number',
            total: 'Currency',
            vat_total: 'Currency',
            payment: 'String',
            customer: 'customer-Id'
        }
    }
}

var routes = [
    [ '*', list_tables ],
    [ 'product_csv', product_form ],
    [ 'products.csv', get_products, post_products ],
    [ ':table', [
        [ '*', show_table ],
        [ 'schema', false, get_table_schema ],
        [ 'get', false, get_table_rows ],
//        [ 'update', false, update_tablerow ],
//        [ 'remove', null, remove_tablerow ],
//        [ 'insert', null, insert_tablerow ],    
    ]],
];

var router=express.Router();
router.install_routes(routes);

/**
 * show list if tables
 * 
 * @http-method             all
 * @path                    <prefix>/
 * @path                    <prefix>/list
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function list_tables(request, response, mext) {
    var t = request.t;

    model.check_access(request.customer, 
        [ 'SQL_ACCCES' ], 
        (err, permission) => {

            var vars = {
                title: t('SQL Tables and Views'),
                buttons: [
                    {   title: t('Back'),
                        action: '/',
                        icon: 'back',
                        description: t('Back to Home Screen')
                    },
                    {},
                    {},
                    {},
                    {}
                ],
                tables: [],
                views: [ 'product_csv']
            };

            for (var i in schemata.tables) {
                vars.tables.push(i);
            }
            vars.tables.sort();
            for (var i in schemata.views) {
                vars.views.push(i);
            }
            vars.views.sort();


            response.render('sql_tables',vars);
        }
    );
}

/**
 * show a sql table
 * 
 * @http-method             all
 * @path                    <prefix>/
 * @path                    <prefix>/list
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function show_table(request, response, mext) {
    var t = request.t;
    var table=request.params.table;

    model.check_access(request.customer, 
        [ 'SQL_ACCCES' ], 
        (err, permission) => {

            var vars = {
                buttons: [
                    {   title: t('Back'),
                        action: '/sql',
                        icon: 'back',
                        description: t('Back to Home Screen')
                    },
                    {},
                    {},
                    {},
                    {}
                ],
                table: table
            };

            response.render('sql_table_view',vars);
        }
    );
}


/**
 * 
 * 
 * @http-method             POST
 * @path                    <prefix>/
 * @path                    <prefix>/list
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function get_table_schema(request, response, mext) {
    var t = request.t;
    var table=request.params.table;

    model.check_access(request.customer, 
        [ 'SQL_ACCCES' ], 
        (err, permission) => {
            var schema = schemata.tables[table] || schemata.views[table]; 

            var columns=[];
            for (var field in schema) {
                var col= { id: field, name: field, field: field, format: schema[field] };
                switch(schema[field]) {
                    case 'Id':  
                        col.width=100; 
                        break;
                    case 'Date': 
                        col.width = 220;
                        col.sortable=true;
                        break;
                    case 'Currency':
                        col.width = 90;
                        col.sortable=true;
                        break;
                    case 'Percent':
                        col.width = 70;
                        col.sortable=true;
                        break;
                    case 'String':
                        col.width = 200;
                        col.sortable=true;
                        break;
                    case 'Number':
                        col.width = 60;
                        col.sortable=true;
                        break;
                    default:
                        if (schema[field].endsWith('Id'))
                            col.width=100; 
                        else
                            col.width = 200;
                        break;
                }
                columns.push(col);
            }

            response.send({
                status: 'success',
                result: columns
            });
        }
    );
}

/**
 * show a sql table
 * 
 * @http-method             POST
 * @path                    <prefix>/
 * @path                    <prefix>/list
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function get_table_rows(request, response, mext) {
    var t = request.t;
    var table=request.params.table;
//console.log(request.body)
    var sort = request.body.sort || 'rowid';
    sort += ' ';
    sort += (request.body.sort_dir=='desc' ? 'desc':'asc');

    var from = request.body.from || 0;
    var count = request.body.count || 20 

    var rows = {
        table: table,
        from: from        
    };

    model.check_access(request.customer, 
        [ 'SQL_ACCCES' ], 
        (err, permission) => {
            model.aggregate(
                table,
                { count: 'count(*)' },
                '', [],
                (err, result) => {

                    if (err) {
                        return response.send({
                            status: 'error',
                            message: err.message,
                            stack: err.stack
                        });
                    }

                    rows.row_count = result.count;

                    model.getAll(
                        table, 
                        '1=1 order by ' + sort + ' limit ?,?', [ from, count+1 ],
                        (err, result) => {
                            if (err) {
                                return response.send({
                                    status: 'error',
                                    message: err.message,
                                    stack: err.stack
                                });
                            }
        
                            rows.rows = result;
                            rows.count = result.length;
                            response.send({
                                status: 'success',
                                result: rows
                            });
                        }
                    )
                }
            );
        }
    );
}

/**
 * update a table row
 * 
 * @http-method             POST
 * @path                    <prefix>/
 * @path                    <prefix>/list
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function update_tablerow(request, response, mext) {
    var t = request.t;
    var table=request.params.table;

    model.check_access(request.customer, 
        [ 'SQL_ACCCES' ], 
        (err, permission) => {
            if (err) {
                return response.send({
                    status: 'error',
                    message: err.message,
                    stack: err.stack
                });
            }

            model.update(
                table, 
                "where rowid=?", [ request.body.rowid ], 
                request.body
            )

            response.send({
                status: 'failure',
                message: 'not implemented'
            });
        }
    );
}

function product_form(request, response) {
    var t = request.t;
    var vars = {
        buttons: [
            {   title: t('Back'),
                action: '/slq',
                icon: 'back',
                description: t('Back to Home Screen')
            },
            {},
            {},
            {},
            {}
        ]
    };
    response.render('sql_product_form',vars);
}

function get_products(req, resp) {

    resp.setHeader('Content-type', 'text/csv')
    resp.setHeader('Content-Disposition', 'attachment; filename="' + "products.csv" + '"')

    let content='id, catid, category, name, price, vat, stock\n';
    model.getAll(
        'shop_categories',
        '1=1', [],
        (err, result) => {
            let categories = {};
            for (let n=0; n<result.length; ++n) {
                categories[result[n].id] = result[n].name;
            }
            model.getAll(
                'shop_products',
                '1=1 order by name', [],
                (err, result) => {
                    for (let n=0; n<result.length; ++n) {
                        let rec=result[n];
                        rec.vat = String(rec.vat).replace( /[,]/g, '.');
                        rec.price = String(rec.price).replace( /[,]/g, '.');
                        content +=  (rec.id + ', "' + 
                                    rec.category + '", "' + 
                                    categories[rec.category] + '", "' + 
                                    rec.name + '", "' + 
                                    rec.price + '", "'+ 
                                    rec.vat + '", "' + 
                                    rec.stock + '"\n');
                    }
                    resp.send(content);
                }
            );
        }
    );
}

const streamBuffers = require('stream-buffers');

function post_products(request, response) {
    var file = request.files && request.files.products;
    var t = request.t;
    var vars = {
        buttons: [
            {   title: t('Back'),
                action: '/slq',
                icon: 'back',
                description: t('Back to Home Screen')
            },
            {},
            {},
            {},
            {}
        ]
    };
    if (file) {
        var stream=new streamBuffers.ReadableStreamBuffer();
        stream.push(file.data);
        stream.push(null);

        var processing=0, ended=false;
        var csv = require('csv-parser')();
        stream.pipe(csv)
            .on('data', (data) => {
                if (data.id) {
                    processing++;
                    model.getOne(
                        'shop_products',
                        'id=?', [data.id],
                        (err, record) => {
                            for (var f in data) {
                                data[f.trim()] = data[f];
                            }
                            // console.log(record);
                            // console.log(data);

                            var fields = {
                                name: String,
                                price: Number,
                                vat: Number,
                                unit: String,
                                stock: Number,
                                stock_warning_level: Number,
                                distributer: String,
                                distributer_sku: String
                            };
                            for (var f in fields) {
                                if (typeof data[f] != 'undefined') {
                                    let v = data[f];
                                    if (fields[f] === Number) {
                                        v = Number(String(v)
                                                .replace(',', '.')
                                                );
                                    }
                                    record[f] = v;
                                    console.log('set', f, v);
                                }
                            }
                            // console.log('==');
                            model.shop.update_product(record, (err) => {
                                processing--;    
                                check_for_end();                                    
                            });
                        }
                    );
                } else {
                    check_for_end();
                }
            })
            .on('end', () => {
                ended=true;
                vars.message = t('Success: File was processed.');
                check_for_end();
            })
            .on('error', (err) => {
                console.log(err)
            });

            function check_for_end() {
                console.log('check_end', processing, ended);
                if (processing==0 && ended) {
                    response.render('sql_product_form',vars);            
                }
            }

    } else {
        vars.message = t('Error: no file was uploaded.');
        response.render('sql_product_form', {});
    }
}

module.exports = router;