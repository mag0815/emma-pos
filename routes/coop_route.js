const { text } = require('body-parser');
const express = require('express');
const { t } = require('tar');
const uuid=require('uuid');

const model = require('../model');
const coop = require('../model/coop');

var router=express.Router();

var routes =
    [   [ '*', coop_overview ],
        [ "login", login, login_post ],
        [ "logout", logout ],
        [ "delivery", [
            [ '*', coop_deliveries ],
            [ 'create', coop_create_delivery ],
            [ ':did', [
                [ 'edit', coop_view_delivery ],
                [ 'view', coop_view_delivery ],
                [ 'search', coop_delivery_search ],
                [ 'add', coop_delivery_add_product ],
                [ 'update', coop_delivery_update, coop_delivery_update ],
                [ 'finish', coop_delivery_finish ],
                [ 'new', coop_delivery_new_product, coop_delivery_new_product_post ]
            ]],
        ]],
        [ "invoice", coop_invoice, coop_invoice_post ],
        [ "journal", coop_journal ],
        [ 'member', [
            [ "*", coop_member_list ],
            [ "create", coop_member_create, coop_member_create_post ],
            [ "check-pin", coop_pin_check_post ],
            [ "check-id", coop_id_check_post ],
            [ ":mid", [
                [ "check-pin", coop_pin_check_post ],
                [ "check-id", coop_id_check_post ],
                [ "*", coop_member_view ],
                [ "edit", coop_member_edit, coop_member_edit_post ],
                [ "cashin", coop_member_cashin, coop_member_cashin_post ],
                [ "cashout", coop_member_cashout, coop_member_cashout_post ],
            ]]
        ]],
    ];


router.install_routes(routes);

module.exports=router;

/**
 * build a link, prefixed by module prefix
 * 
 * @param                   arguments       the path components
 */
function l() {
    var links=Array.from(arguments).join('/');
    return '/coop/' + links;
}

function login(request, response, next) {
    var t = request.t;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE' ], 
        (err, permission) => {
            if (err) return next(new Error(err));

            var vars = {
                title: t('Login required'),
                form: {
                    action: l('login'),
                    values: {
                    },
                    fields: [
                        {
                            name: 'backlink',
                            type: 'const'
                        },
                        {
                            name: 'pin',
                            type: 'keypad-pin',
                            caption: 'Pin'
                        }
                    ]
                },
                buttons: [
                    {   title: t('Back'),
                        icon: 'back',
                        description: t('Go Back'),
                        action: '/index'
                    },
                    {},
                    {},
                    {},
                    {   title: t('Login'),
                        icon: 'cart',
                        description: t('Login into the System'),
                        action: 'post-form'
                    }
                ]
        
            };

            response.render('cart_login', vars);    

        });
}

function login_post(request, response, next) {
    var pin=request.body.pin;

    model.coop.get_customer_by_pin(pin, (err, customer) => {
        if (err) {
            response.send(
                {   status: 'error', 
                    error: err.message,
                    stack: err.stack
                }
            );

        } else if (customer == undefined) {
            response.send(
                {   status: 'failure', 
                    error: 'Bad Pin.'
                }
            );

        } else {
            response.cookie('adminpin', pin);
            response.send(
                {   status: 'success', 
                    redirect: l('')
                }
            );
        }
    });
}

function logout(request, response, next) {
    var t = request.t;

    // remove login cookie
    response.cookie('adminpin', '');

    response.redirect('/');    
}

/**
 * Cashdesk Overview Page
 * 
 * @param {*} request 
 * @param {*} response 
 * @param {*} next 
 */
function coop_overview(request, response, next) {
    var t = request.t;

    model.check_access(request.customer, 
        [ 'COOP_VIEW', 'COOP_MANAGE' ], 
        (err, permission) => {
            if (err)
                return next(err);

            if (!permission.COOP_MANAGE) {
                if (!permission.COOP_VIEW)
                    return response.redirect(l('login'));
                else 
                    return response.redirect(l('member', request.customer.id));
            }

            let buttons = [
                {   title: t('Back'),
                    icon: 'back',
                    description: t('Back to Home'),
                    action: '/'
                },
                { },
                { },
                { },
                { }
            ];

            if (permission.COOP_MANAGE) {
                buttons[1] = {   title: t('Members'),
                    icon: 'customer',
                    description: t(''),
                    action: l('member')
                };
                buttons[2] = {   title: t('Delivery'),
                    icon: 'delivery',
                    description: t(''),
                    action: l('delivery')
                };
                buttons[3] = {   title: t('Invoices'),
                    icon: 'invoice',
                    description: t(''),
                    action: l('invoice')
                };
                buttons[4] = {   title: t('Journal'),
                    icon: 'moneypig',
                    description: t(''),
                    action: l('journal')
                };
            }

            model.coop.overview(
                (err, result) => {
                    if (err)
                        return next(err);

                    var vars = {
                        title: t('Coop: Overview'),
                        buttons: buttons,
                        balance: result
                    }

                    response.render('coop_overview', vars);    
                }
            );
        }
    );
}

/**
 *Member Overview Page
 * 
 * @param {*} request 
 * @param {*} response 
 * @param {*} next 
 */
function coop_member_list(request, response, next) {
    var t = request.t;

    let pager = {
        limit: 10,
        skip: request.query.skip || 0,
        link: l('member')
    };

    model.check_access(request.customer, 
        [ 'COOP_VIEW', 'COOP_MANAGE' ], 
        (err, permission) => {

            if (err)
                return next(err);
            if (!permission.COOP_VIEW) {
                return response.redirect('/');
            }

            let buttons = [
                {   title: t('Back'),
                    icon: 'back',
                    description: t('Back to Home'),
                    action: l()
                },
                { },
                { },
                { },
                { }
            ];

            if (permission.COOP_MANAGE) {
                buttons[4] = {   
                    title: t('New Member'),
                    icon: 'editcustomer',
                    description: t(''),
                    action: l('member/create')
                };
            }

            coop.get_customers(pager, (err, list) => {

                if (err) return next(err);

                model.cashdesk.overview(
                    (err, result) => {
                        if (err)
                            return next(err);
    
                        var vars = {
                            title: t('Coop: Member List'),
                            buttons: buttons,
                            balance: result.balance,
                            table: {
                                columns: [
                                    {   name: 'id', caption: t('Id'), width: '20%' },
                                    {   name: 'name', caption: t('Name'), width: '60%' },
                                    {   name: 'balance', caption: t('Balance'), width: '20%' },
                                ],
                                pager: pager,
                                values: list,
                                link: l('member')
                            },
                        }
    
                        response.render('coop_member_list', vars);    
                    }
                );
    
            });
        }
    );
}

function coop_member_view(request, response, next) {
    var t = request.t;
    var mid = request.params.mid;

    let pager = {
        limit: 10,
        skip: request.query.skip || 0,
        link: l('member')
    };

    model.check_access(request.customer, 
        [ 'COOP_VIEW', 'COOP_MANAGE' ], 
        (err, permission) => {

            if (err)
                return next(err);
            if (!permission.COOP_VIEW) {
                return response.redirect('/');
            }

            let buttons = [
                { },
                { },
                { },
                { },
                { }
            ];

            if (permission.COOP_MANAGE) {
                buttons[0] = {   
                   title: t('Back'),
                    icon: 'back',
                    description: t('Back to Home'),
                    action: l()
                },
                buttons[2] = {   
                    title: t('Edit Member'),
                    icon: 'newcustomer',
                    description: t(''),
                    action: l('member',mid,'edit')
                };
                buttons[4] = {   
                    title: t('Deposit Cash'),
                    icon: 'cashout',
                    description: t(''),
                    action: l('member',mid,'cashin')
                };
                buttons[3] = {   
                    title: t('Withdraw Cash'),
                    icon: 'cashin',
                    description: t(''),
                    action: l('member',mid,'cashout')
                };
            } else {
                buttons[0] = {   
                    title: t('logout'),
                     icon: 'customer',
                     description: t('Back to Home'),
                     action: l('logout')
                 } 
            }

            coop.get_customer_saldo( mid, pager, (err, result) => {

                if (err) return next(err);
                var vars = {
                    title: t('Coop: Member Account'),
                    buttons: buttons,
                    member: result.customer,
                    table: {
                        columns: [
                            {   name: 'date', caption: t('Date'), width: '20%' },
                            {   name: 'type', caption: t('Type'), width: '10%' },
                            {   name: 'remark', caption: t('Remark'), width: '50%'},
                            {   name: 'amount', caption: t('Amount'), width: '20%' },
                        ],
                        pager: pager,
                        noresult: t('no transactions'),
                        values: result.journal
                    },
                    saldo: result.balance
                }
                response.render('coop_member_view', vars);    
    
            });
        }
    );
}

function get_role_selects(t) {
    return [
        { value:'', caption: '\xa0'},
        { value:'admin', caption: t('Administrator')},
        { value:'clerk', caption: t('Clerk')},
        { value:'member', caption: t('Member')},
        { value:'manager', caption: t('Manager')},
    ];
}

function coop_member_edit(request, response, next) {
    var t = request.t;
    var mid = request.params.mid;

    model.check_access(request.customer, 
        [ 'COOP_VIEW', 'COOP_MANAGE' ], 
        (err, permission) => {

            if (err)
                return next(err);
            if (!permission.COOP_VIEW) {
                return response.redirect('/');
            }


            coop.get_customer(mid, (err, customer) => {
                if (err) return next(err);

                let roles = eval(customer.roles);
                customer.roles1 = roles[0] || '';
                customer.roles2 = roles[1] || '';
                customer.roles3 = roles[2] || '';
                customer.roles4 = roles[3] || '';

                let role_selects = get_role_selects(t);
                customer.new_pin = customer.pin;
                
                var vars = {
                    title: t('Coop: Edit Member'),
                    buttons: [
                        {   title: t('Back'),
                            icon: 'back',
                            description: t('Back to Home'),
                            action: l('member', mid)
                        },
                        { },
                        { },
                        { },
                        {   title: t('Save'),
                            icon: 'save',
                            descriiption: t(''),
                            action: 'post-form'        
                        }
                    ],
                    form: {
                        id: 'member-edit',
                        action: l('member', mid, 'edit'),
                        fields: [
                            { name: 'id',
                              type: 'const' },
                            { name: 'id_error',
                              caption: '',
                              type: 'error-message',
                              text: t('This id is already in use.')
                            },
                            { name: 'name',
                              caption: t('Name'),
                              type: 'text',
                              required: true },
                            { name: 'new_pin',
                              caption: t('Pin'),
                              type: 'number',
                              required: false },
                            { name: 'pin_error',
                              caption: '',
                              type: 'error-message',
                              text: t('This pin is already in use.')
                            },
                            { name: 'is_special',
                              caption: t('Special'),
                              type: 'select',
                              values: [
                                  { value:0, caption: 'Nein'},
                                  { value:1, caption: 'Ja'},
                              ]
                            },
                            { name: 'roles1',
                              caption: t('Roles'),
                              type: 'select',
                              values: role_selects
                            },
                            { name: 'roles2',
                              caption: '\xa0',
                              type: 'select',
                              values: role_selects
                            },
                            { name: 'roles3',
                              caption: '\xa0',
                              type: 'select',
                              values: role_selects
                            },
                            { name: 'roles4',
                              caption: '\xa0',
                              type: 'select',
                              values: role_selects
                            },
                        ],
                        values: customer
                    }
                }

                response.render('coop_member_edit', vars);    
    
            });
        }
    );
}

function coop_pin_check_post(request, response, next) {
    let post = request.body;
    model.coop.get_customer_by_pin(post.pin, (err, customer) => {
        if (err) {
            response.send(
                {   status: 'error', 
                    error: err.message,
                    stack: err.stack
                }
            );

        } else if (customer == undefined) {
            response.send(
                {   status: 'success', 
                    result: 'not-found'
                }
            );

        } else {
            response.send(
                {   status: 'success', 
                    result: 'found'
                }
            );
        }
    })
}

function coop_id_check_post(request, response, next) {
    let post = request.body;
    model.coop.get_customer(post.id, (err, customer) => {
        if (err) {
            response.send(
                {   status: 'error', 
                    error: err.message,
                    stack: err.stack
                }
            );

        } else if (customer == undefined) {
            response.send(
                {   status: 'success', 
                    result: 'not-found'
                }
            );

        } else {
            response.send(
                {   status: 'success', 
                    result: 'found'
                }
            );
        }
    })
}

function coop_member_edit_post(request, response, next) {
    var t = request.t;
    var mid = request.params.mid;
    let post = request.body;

    model.coop.get_customer(mid, (err, customer) => {
        if (err) return next(err);

        customer.name = post.name;
        customer.is_special = Number(post.is_special);
        if (post.new_pin) {
            customer.pin = post.new_pin;
        }
        let roles = [];
        if (post.roles1) roles.push(post.roles1);
        if (post.roles2) roles.push(post.roles2);
        if (post.roles3) roles.push(post.roles3);
        if (post.roles4) roles.push(post.roles4);
        customer.roles = JSON.stringify(roles);

        model.coop.update_customer(mid, customer, (err, result) => {

            if (err) {
                response.send(
                    {   status: 'error', 
                        error: err.message,
                        stack: err.stack
                    }
                );    
            } else {
                response.send({
                    status: 'success',
                    redirect: l('member', mid)
                });
            }
        });
    });
}

function coop_member_create(request, response, next) {
    var t = request.t;
    var mid = request.params.mid;

    model.check_access(request.customer, 
        [ 'COOP_VIEW', 'COOP_MANAGE' ], 
        (err, permission) => {

            if (err)
                return next(err);

            if (!permission.COOP_MANAGE) {
                return response.redirect('/');
            }

            let role_selects = get_role_selects(t);

            var vars = {
                title: t('Coop: Create Member'),
                buttons: [
                    {   title: t('Back'),
                        icon: 'back',
                        description: t('Back to Home'),
                        action: l('member', mid)
                    },
                    { },
                    { },
                    { },
                    {   title: t('Save'),
                        icon: 'save',
                        description: t(''),
                        action: 'post-form',
                        disabled: true
    
                    }
                ],
                form: {
                    id: 'member-edit',
                    action: l('member', 'create'),
                    fields: [
                        {   name: 'id',
                            caption: 'Id',
                            type: 'text',
                            required: true },
                        {   name: 'id_error',
                            caption: '',
                            type: 'error-message',
                            text: t('This id is already in use.')
                        },
                        {   name: 'name',
                            caption: t('Name'),
                            type: 'text',
                            required: true },
                        {   name: 'new_pin',
                            caption: t('Pin'),
                            type: 'number',
                            required: true },
                        {   name: 'pin_error',
                            caption: '',
                            type: 'error-message',
                            text: t('This pin is already in use.')
                        },
                        {   name: 'is_special',
                            caption: t('Special'),
                            type: 'select',
                            values: [
                                { value:0, caption: 'Nein'},
                                { value:1, caption: 'Ja'},
                            ]
                        },
                        {   name: 'roles1',
                            caption: t('Rollen'),
                            type: 'select',
                            values: role_selects
                        },
                        {   name: 'roles2',
                            caption: '\xa0',
                            type: 'select',
                            values: role_selects
                        },
                        {   name: 'roles3',
                            caption: '\xa0',
                            type: 'select',
                            values: role_selects
                        },
                        {   name: 'roles4',
                            caption: '\xa0',
                            type: 'select',
                            values: role_selects
                        },
                    ],
                    values: {
                        roles1: "member"
                    }
                }
            }

            response.render('coop_member_edit', vars);    
    
        }
    );
}

function coop_member_create_post(request, response, next) {
    var t = request.t;
    let post = request.body;
    var mid = post.id;

    let customer = {};

    customer.id = mid;
    customer.name = post.name;
    customer.is_special = Number(post.is_special);
    if (post.new_pin) {
        customer.pin = post.new_pin;
    }
    let roles = [];
    if (post.roles1) roles.push(post.roles1);
    if (post.roles2) roles.push(post.roles2);
    if (post.roles3) roles.push(post.roles3);
    if (post.roles4) roles.push(post.roles4);
    customer.roles = JSON.stringify(roles);

    model.coop.create_customer(mid, customer, (err) => {
        if (err) {
            response.send(
                {   status: 'error', 
                    error: err.message,
                    stack: err.stack
                }
            );    
        } else {
            response.send({
                status: 'success',
                redirect: l('member', mid)
            });
        }
    });
}

function coop_member_cashin(request, response, next) {
    var t = request.t;
    var mid = request.params.mid;

    model.check_access(request.customer, 
        [ 'COOP_VIEW', 'COOP_MANAGE' ], 
        (err, permission) => {

            if (err)
                return next(err);
            if (!permission.COOP_VIEW) {
                return response.redirect('/');
            }

            values = { 
                id: mid,
            }
            var vars = {
                title: t('Coop: Member Cashin'),
                buttons: [
                    {   title: t('Back'),
                        icon: 'back',
                        description: t('Back to Member Page'),
                        action: l('member', mid)
                    },
                    { },
                    { },
                    { },
                    {   title: t('Cashin'),
                        icon: 'cashout',
                        descriiption: t(''),
                        action: 'post-form',
                        disabled: true
    
                    }
                ],
                form: {
                    id: 'member-cashin',
                    action: l('member', mid, 'cashin'),
                    fields: [
                        { name: 'id',
                            type: 'const' },
                        { name: 'amount',
                            caption: t('Amount'),
                            type: 'currency',
                            required: true },
                        { name: 'remark',
                            caption: t('Remark'),
                            type: 'text',
                            required: false }
                    ],
                    values: values
                }
            };

            response.render('coop_member_cashin', vars);    
    
        }
    );
}

function coop_member_cashin_post(request, response, next) {
    var t = request.t;
    var mid = request.params.mid;
    var post = request.body;

    model.coop.add_to_journal({
        type: 'deposit',
        description: 'Einzahlung in coop',
        customer: post.id,
        amount: Number(post.amount),
        remark: post.remark,
        vat: 0
    }, (err) => {
        model.coop.add_to_journal({
            type: 'deposit',
            description: 'Einzahlung in coop von '+post.id,
            customer: 'emma-shop',
            amount: -Number(post.amount),
            remark: post.remark,
            vat: 0    
        }, (err) => {
            return response.send({
                status: 'success',
                redirect: l('member', post.id)
            });
        })
    });
}

function coop_member_cashout(request, response, next) {
    var t = request.t;
    var mid = request.params.mid;

    model.check_access(request.customer, 
        [ 'COOP_VIEW', 'COOP_MANAGE' ], 
        (err, permission) => {

            if (err)
                return next(err);
            if (!permission.COOP_VIEW) {
                return response.redirect('/');
            }

            values = { 
                id: mid,
            }
            var vars = {
                title: t('Coop: Member Cashout'),
                buttons: [
                    {   title: t('Back'),
                        icon: 'back',
                        description: t('Back to Member Page'),
                        action: l('member', mid)
                    },
                    { },
                    { },
                    { },
                    {   title: t('Cashout'),
                        icon: 'cashin',
                        description: t(''),
                        action: 'post-form',
                        disabled: true
    
                    }
                ],
                form: {
                    id: 'member-cashout',
                    action: l('member', mid, 'cashout'),
                    fields: [
                        { name: 'id',
                            type: 'const' },
                        { name: 'amount',
                            caption: t('Amount'),
                            type: 'currency',
                            required: true },
                        { name: 'remark',
                            caption: t('Remark'),
                            type: 'text',
                            required: false }
                    ],
                    values: values
                }
            };

            response.render('coop_member_cashin', vars);    
    
        }
    );
}

function coop_member_cashout_post(request, response, next) {
    var t = request.t;
    var mid = request.params.mid;
    var post = request.body;

    model.coop.add_to_journal({
        type: 'deposit',
        description: 'Auszahlung von coop',
        customer: post.id,
        amount: -Number(post.amount),
        remark: post.remark,
        vat: 0
    }, (err) => {
        model.coop.add_to_journal({
            type: 'deposit',
            description: 'Auszahlung von coop von '+post.id,
            customer: 'emma-shop',
            amount: Number(post.amount),
            remark: post.remark,
            vat: 0    
        }, (err) => {
            return response.send({
                status: 'success',
                redirect: l('member', post.id)
            });
        })
    });
}


function build_customer_select(list) {
    let values = [ ];
    for(let n=0; n<list.length; ++n) {
        let customer = list[n];
        values.push({
            value: customer.id,
            caption: customer.name
        });
    }
    return values;
}

function coop_invoice(request, response, next) {
    var t = request.t;
    var mid = request.params.mid;

    model.check_access(request.customer, 
        [ 'COOP_VIEW', 'COOP_MANAGE' ], 
        (err, permission) => {

            if (err)
                return next(err);
            if (!permission.COOP_VIEW) {
                return response.redirect('/');
            }

            model.admin.get_customers(
                (err, customers) => {
                    if (err) 
                        return next(new Error(err));

                    var vars = {
                        title: t('Coop: Member Cashout'),
                        buttons: [
                            {   title: t('Back'),
                                icon: 'back',
                                description: t('Back to Coop Page'),
                                action: l('')
                            },
                            { },
                            { },
                            { },
                            {   title: t('Pay'),
                                icon: 'cashin',
                                description: t(''),
                                action: 'post-form',
                                disabled: true
            
                            }
                        ],
                        form: {
                            id: 'member-invoice',
                            action: l('invoice'),
                            fields: [
                                {   name: 'amount',
                                    caption: t('Amount'),
                                    type: 'currency',
                                    required: true },
                                {   name: 'customer',
                                    caption: t('Customer'),
                                    type: 'select',
                                    required: true,
                                    values: build_customer_select(customers)
                                },
                                {   name: 'remark',
                                    caption: t('Remark'),
                                    type: 'text',
                                    required: true }
                            ],
                            values: {
                                customer: 'food-coop'
                            }
                        }
                    };
        
                    response.render('coop_invoice', vars);    
                }
            );
        }
    );
}

function coop_invoice_post(request, response, next) {
    let ost = request.body;

    model.coop.add_to_journal(
        {   amount: post.amount,
            customer: post.customer,
            type: 'invoice',
            description: 'invoice payment',
            remark: post.remark

        }, (err) => {
            if (err) {
                response.send(
                    {   status: 'error', 
                        error: err.message,
                        stack: err.stack
                    }
                );
            } else {
                response.send({
                    status: 'success',
                    redirect: l('')
                });
            }
        }
    )
}

function coop_journal(request, response, next) {
    var t = request.t;

    let pager = {
        limit: 10,
        skip: request.query.skip || 0,
        link: l('journal')
    };

    model.check_access(request.customer, 
        [ 'COOP_VIEW', 'COOP_MANAGE' ], 
        (err, permission) => {

            if (err)
                return next(err);
            if (!permission.COOP_VIEW) {
                return response.redirect('/');
            }

            let buttons = [
                {   title: t('Back'),
                    icon: 'back',
                    description: t('Back to Home'),
                    action: l()
                },
                { },
                { },
                { },
                { }
            ];

            coop.get_journal(pager, (err, list) => {

                if (err) return next(err);
    
                var vars = {
                    title: t('Coop: Journal'),
                    buttons: buttons,
                    table: {
                        columns: [
                            {   name: 'date', caption: t('Date'), width: '20%' },
                            {   name: 'amount', caption: t('Amount'), width: '60%' },
                            {   name: 'type', caption: t('Type'), width: '20%' },
                            {   name: 'description', caption: t('Description'), width: '20%' },
                            {   name: 'remark', caption: t('Remark'), width: '20%' },
                        ],
                        pager: pager,
                        values: list,
                        link: l('journal')
                    },
                }
                response.render('coop_journal', vars);    
    
            });


        }
    );
}

function coop_deliveries(request, response, next) {
    var t = request.t;
    model.check_access(request.customer, 
        [ 'COOP_VIEW', 'COOP_MANAGE' ], 
        (err, permission) => {

            if (err) return next(err);
            if (!permission.COOP_VIEW) {
                return response.redirect('/');
            }

            let pager = {
                limit: 10,
                skip: request.query.skip || 0,
                link: l('journal')
            };
        

            coop.get_deliveries(pager, (err, list) => {

                if (err) return next(err);
                
                var vars = {
                    title: t('Coop: Deliveries'),
                    buttons: [
                        {   title: t('Back'),
                            icon: 'back',
                            description: t('Back to Coop Page'),
                            action: l()
                        },
                        { },
                        { },
                        { },
                        {   title: t('New'),
                            icon: 'invoice',
                            description: t('New Delivery'),
                            action: l('delivery/create')
                        }
                    ],
                    table: {
                        columns: [
                            {   name: 'date', caption: t('Date'), width: '20%' },
                            {   name: 'state', caption: t('State'), width: '10%' },
                            {   name: 'total', caption: t('Total'), width: '10%' },
                            {   name: 'remark', caption: t('Remark'), width: '60%' },
                        ],
                        pager: pager,
                        values: list,
                        link: l('journal')
                    },
                }
                response.render('coop_deliveries', vars);    

            });
        }
    );
}

function coop_create_delivery(request, response, next) {
    model.check_access(request.customer, 
        [ 'COOP_VIEW', 'COOP_MANAGE' ], 
        (err, permission) => {

            if (err) return next(err);
            if (!permission.COOP_VIEW) {
                return response.redirect('/');
            }

            model.coop.create_delivery( (err, delivery) => {
                if (err) return next(err);
                response.redirect(l('delivery', delivery.id, 'view'));
            });
        }
    );
}

function coop_view_delivery(request, response, next) {
    var t = request.t;
    var did = request.params.did;

    model.check_access(request.customer, 
        [ 'COOP_VIEW', 'COOP_MANAGE' ], 
        (err, permission) => {

            if (err) return next(err);
            if (!permission.COOP_VIEW) {
                return response.redirect('/');
            }

            model.coop.get_delivery(did, (err, delivery, items) => {
                if (err) return next(err);

                if (delivery.state == 'pending')
                    return coop_edit_delivery(delivery, items, request, response, next);

                vars = {
                    title: t('Coop: Delivery'),
                    buttons: [
                        {   title: t('Back'),
                            icon: 'back',
                            description: t('Back to Coop Page'),
                            action: l()
                        },
                        { },
                        { },
                        { },
                        {   /*
                            title: t('Duplicate'),
                            icon: 'invoice',
                            description: t('Duplicate Delivery'),
                            action: l('delivery', did, 'duplicate')
                            */
                        }
                    ],
                    delivery: delivery,
                    items: items
                }
                response.render('coop_delivery_view', vars);    

            })
        }
    );
}

function coop_edit_delivery(delivery, items, request, response) {
    var t = request.t;
    vars = {
        title: t('Coop: Delivery'),
        buttons: [
            {   title: t('Back'),
                icon: 'back',
                description: t('Back to Coop Page'),
                action: l('delivery')
            },
            {   title: t('Delete'),
                icon: 'trash',
                description: t('Delete this pending delivery'),
                action: l('delivery', delivery.id, 'delete')
            },
            { },
            { },
            {   title: t('Finish'),
                icon: 'invoice',
                description: t('Duplicate Delivery'),
                action: l('delivery', delivery.id, 'finish')
            }
        ],
        delivery_form: {
            fields: [
                {   name: 'id',
                    type: 'const',
                },
                {   name: 'search',
                    type: 'text-with-button',
                    caption: t('Search')
                },
                {   name: 'remark',
                    type: 'text',
                    caption: t('Remark') 
                },
            ],
            values: delivery,
        },
        items: items
    }
    response.render('coop_delivery_edit', vars);    

}

function coop_delivery_search(request, response, next) {
    var t = request.t;
    var did = request.params.did;
    var search = request.query.q;

    model.check_access(request.customer, 
        [ 'COOP_VIEW', 'COOP_MANAGE' ], 
        (err, permission) => {

            if (err) return next(err);
            if (!permission.COOP_VIEW) {
                return response.redirect('/');
            }

            model.shop.find_products('%'+search+'%', (err, products) => {

                var vars = {
                    title: t('Coop: Delivery'),
                    buttons: [
                        {   title: t('Back'),
                            icon: 'back',
                            description: t('Back to Delivery'),
                            action: l('delivery', did, 'view')
                        },
                        { },
                        { },
                        { },
                        { }
                    ],
                    search_form: {
                        fields: [
                            {   name: 'search',
                                type: 'text-with-button',
                                caption: t('Search')
                            },
                        ],
                        values: {
                            search: search
                        },
                    },
                    table: {
                        columns: [
                            {   name: 'name', caption: t('Name'), width: '60%' },
                            {   name: 'stock', caption: t('Stock'), width: '20%' },
                            {   name: 'price', caption: t('Price'), width: '20%' },
                        ],
                        pager: null,
                        values: products
                    },
                }
                response.render('coop_delivery_search', vars);    
            });
        }
    );

}

function coop_delivery_add_product(request, response, next) {
    var t = request.t;

    var did = request.params.did;
    var pid = request.query.product;

    model.check_access(request.customer, 
        [ 'COOP_VIEW', 'COOP_MANAGE' ], 
        (err, permission) => {
            
        if (err) return next(err);
        //if (permission.COOP_MANAGE) return response.redirect(l(""));

        model.coop.delivery_add_product(did, pid, 
            (err, item) => {
                if (err) return next(err);
                response.redirect(l("delivery", did, 'view'));
            });
        }
    );
}

function coop_delivery_update(request, response, next) {
    model.check_access(request.customer, 
        [ 'COOP_VIEW', 'COOP_MANAGE' ], 
        (err, permission) => {

            if (err) {
                return response.send(
                    {   status: 'error', 
                        error: err.message,
                        stack: err.stack
                    }
                );            
            }

            if (!permission.COOP_VIEW) {
                return response.send(
                    {   status: 'failure', 
                        error: 'permission denied.'
                    }
                );            
            }
            let changeset = request.body;
            model.coop.delivery_update(changeset, (err) => {
                return response.send(
                    {   status: 'success'
                    });
            });
        }
    );   
}

function coop_delivery_new_product(request, response, next) {
}

function coop_delivery_new_product_post(request, response, next) {
}

function coop_delivery_finish(request, response, next) {
    var did = request.params.did;

    model.check_access(request.customer, 
        [ 'COOP_VIEW', 'COOP_MANAGE' ], 
        (err, permission) => {

            if (err) return next(err);

            if (!permission.COOP_VIEW) return response.redirect('/');

            model.coop.delivery_finish(did, (err) => {
                if (err) return next(err);
                return response.redirect(l('delivery'));
        });
    });
} 