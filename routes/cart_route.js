const express = require('express');
const { t } = require('tar');
const uuid=require('uuid');

const model = require('../model');
const coop = require('../model/coop');

var router=express.Router();

var routes =
    [   [ '*', list_carts ],
        [ "login", login, login_post ],
        [ "overview", login_overview ],
        [ "logout", logout ],
        [ 'list', list_carts ],
        [ 'create', new_cart ],
        [ 'cashin', cashin, cashin_post ],
        [ 'cashout', cashout, cashout_post ],
        [ ':cid', [
            [ '*', show_cart ],
            [ 'products', [
                [ '*', list_products ],
                ['find_products', find_products],
                [ ':catid', [
                    [ '*', list_products ],
                    [ 'new_category', new_category, new_category_post ],
                    [ 'edit', edit_category, edit_category_post ],
                    [ 'image', edit_category_image, edit_category_post ],
                    [ 'delete', remove_category ],
                    [ 'new_product', new_product, new_product_post ],
                ]],
            ]],
            [ 'product', [
                [  ':pid', [
                    [ '*', show_product, show_product_post ],
                    [ 'edit', edit_product, edit_product_post ], 
                    [ 'image', edit_product_image, edit_product_image_post ], 
                ]]
            ]],
            [ 'item', [
                [ ':itemid', [
                    [ '*', edit_cartitem, edit_cartitem_post ],
                    [ 'remove', remove_cartitem ],
                ]]
            ]],
            [ 'cart_annotate', annotate_cart, annotate_cart_post ],
            [ 'delete', remove_cart ],
            [ 'checkout', checkout ],
            [ 'checkout2', checkout2, checkout_prepaid ],
            [ 'checkout_cash', checkout_cash],
            [ 'checkout2_check', false, checkout2_check ],
        ]]
    ]
;
router.install_routes(routes);

module.exports = router;


/**
 * build a link, prefixed by module prefix
 * 
 * @param                   arguments       the path components
 */
function l() {
    var links=Array.from(arguments).join('/');
    return '/cart/' + links;
}

function login(request, response, next) {
    var t = request.t;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE' ], 
        (err, permission) => {
            if (err) return next(new Error(err));

            var vars = {
                title: t('Login required'),
                form: {
                    action: '/cart/login',
                    values: {

                    },
                    fields: [
                        {
                            name: 'backlink',
                            type: 'const'
                        },
                        {
                            name: 'pin',
                            type: 'keypad-pin',
                            caption: 'Pin'
                        }
                    ]
                },
                buttons: [
                    {   title: t('Back'),
                        icon: 'back',
                        description: t('Go Back'),
                        action: '/index'
                    },
                    {},
                    {},
                    {},
                    {   title: t('Login'),
                        icon: 'cart',
                        description: t('Login into the System'),
                        action: 'post-form'
                    }
                ]
        
            };

            response.render('cart_login', vars);    

        });
}

function login_post(request, response, next) {
    var pin=request.body.pin;

    model.coop.get_customer_by_pin(pin, (err, customer) => {
        if (err) {
            response.send(
                {   status: 'error', 
                    error: err.message,
                    stack: err.stack
                }
            );

        } else if (customer == undefined) {
            response.send(
                {   status: 'failure', 
                    error: 'Bad Pin.'
                }
            );

        } else {
            response.cookie('adminpin', pin);
            response.send(
                {   status: 'success', 
                    redirect: l('overview')
                }
            );
        }
    });
}

function login_overview(request, response, next) {
    var t = request.t;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK' ], 
        (err, permission) => {
            var cash_amount = 0;
            var message = 'Hello Customer!';

            if (err) return next(new Error(err));

            if (!permission.ESHOP_DESK) 
                return response.redirect(l('login'));
            
            model.shop.cash_amount((err, amount) => {
                if (err) return next(new Error(err));

                var vars = {
                    title: t('Overview'),
                    customer: request.customer,
                    cash_amount: amount,
                    message: request.customer.admin_message,
                    buttons: [
                        {},
                        {},
                        {},
                        {},
                        {   title: t('Continue'),
                            icon: 'continue',
                            description: t('Continue to cart list'),
                            action: l('/')
                        }
                    ]
                };
                response.render('cart_overview', vars);    
   
            });
        }
    );
}

function logout(request, response, next) {
    var t = request.t;

    // remove login cookie
    response.cookie('adminpin', '');

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE' ], 
        (err, permission) => {
            if (err) return next(new Error(err));

            model.shop.cash_amount((err, amount) => {
                if (err) return next(new Error(err));

                var vars = {
                    title: t('Overview'),
                    cash_amount: amount,
                    buttons: [
                        {},
                        {},
                        {},
                        {},
                        {   title: t('Continue'),
                            icon: 'continue',
                            description: t('Continue to Mainpage'),
                            action: '/'
                        }
                    ]
                };
    
                response.render('cart_logout', vars);    
    
            });    
        }
    );
}

/**
 * show cart list
 * 
 * @http-method             all
 * @path                    <prefix>/
 * @path                    <prefix>/list
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function list_carts(request, response, next) {
    var t = request.t;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE', 'EPOS_ADMIN' ], 
        (err, permission) => {
            if (err) return next(new Error(err));

            if (!permission.ESHOP_DESK) 
                return response.redirect(l('login'));

            model.shop.get_carts((err, carts) => { 
                if (err) return next(new Error(err));
        
                var vars = {
                    title: t('Active Carts'),
                    carts: carts,
                    buttons: [
                        {   title: t('Logout'),
                            icon: 'customer',
                            description: t('Logout from the cart'),
                            action: l('logout')
                        },
                        {},
                        {},
                        {},
                        {   title: t('New Cart'),
                            icon: 'cart',
                            description: t('Create new Cart'),
                            action: l('create')
                        }
                    ]
            
                };

                if (permission.ESHOP_MANAGE) {
                    vars.buttons[3] = {
                        title: t('Cash-In/Out'),
                        description: 'Payout cash to Coop-Member',
                        icon: 'cashout',
                        action: l('cashout')
                    };
                }

                if (permission.EPOS_ADMIN) {
                    vars.buttons[0] = {
                        title: t('Back'),
                        description: 'Back to Main',
                        icon: 'back',
                        action: '/'
                    };
                }

                response.render('cart_list', vars);    
            }
        );        
    });
}

/**
 * create a new cart and redirect to cart view
 * 
 * @http-method             all
 * @path                    <prefix>/create
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function new_cart(request, response, next) {
    var t = request.t;
    model.check_access(request.customer, 
        [ 'ESHOP_DESK' ], 
        (err, permission) => {

            model.shop.create_cart( request.customer.name, (err, cart) => {
                if (err) return next(new Error(err));
                response.redirect(l(cart.id));
            });            
        }
    );
}

/**
 * show a cart and its cart items
 * 
 * @http-method             all
 * @path                    <prefix>/:cid
 * @url-param               cid         the cart-id
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function show_cart(request, response, next) {
    var t = request.t;
    var cid=request.params.cid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK' ], 
        (err, permission) => {

            model.shop.get_cart(cid, (err, cart, items) => {
                if (err) return next(new Error(err));
        
                if (cart==undefined) return next(new Error('Unknow Cart: '+cid));
        
                response.render('cart_view', {
                    title: t('Cart created on') + ' ' + cart.date,
                    subtitle: cart.remark,
                    cart: cart,
                    items: items,
                    buttons: [
                        {   title: t('Back'),
                            icon: 'back',
                            description: t('Go back to Cart List'),
                            action: l('list')
                        },
                        {   title: t('Delete'),
                            icon: 'trash',
                            description: t('Delete this cart'),
                            action: l(cid, 'delete')
                        },
                        {   title: t('Remark'),
                            icon: 'message',
                            description: t('Add remark'),
                            action: l(cid, 'cart_annotate')

                        },
                        {   title: t('Buy Product'),
                            icon: 'basket',
                            description: t('Add a Product to Cart'),
                            action: l(cid + '/products')
                        },
                        {   title: t('Checkout'),
                            icon: 'cart',
                            description: t('Goto Checkout Screen'),
                            action: l(cid, 'checkout'),
                            disabled: items.length==0
                        }
                    ]
                });
            });
        }
    );
}


/**
 * add annotation to cart, show form
 * 
 * @http-method             GET
 * @path                    <prefix>/:cid/cart_anotate
 * @url-param               cid         the cart-id
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function annotate_cart(request, response, next) {
    var t = request.t;
    var cid=request.params.cid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK' ], 
        (err, permission) => {

            model.shop.get_cart(cid, (err, cart, items) => {
                if (err) return next(new Error(err));
        
                if (cart==undefined) {
                    return next(new Error('Unknow Cart: '+cid));
                }
        
                let vars = {
                    title: t('Cart created on') + ' ' + cart.date,
                    cart: cart,
                    items: items,
                    buttons: [
                        {   title: t('Back'),
                            icon: 'back',
                            description: t('Go back to Cart'),
                            action: l(cid)
                        },
                        {   
                        },
                        {   
                        },
                        {   
                        },
                        {   title: t('Save'),
                            icon: 'message',
                            description: t('Add Annotation'),
                            action: "post-form"
                        }
                    ],
                    form: {
                        id: 'cat-create',
                        action: l(cid, 'cart_annotate'),
                        values: {
                            category: cid,
                            remark: cart.remark||''
                        },
                        fields: [
                            {   name: 'remark',
                                caption: t('Remark'), 
                                type: 'text'
                            }
                        ]
                    }
                }
                response.render('cart_annotate', vars);
            });
        }
    );
}

/**
 * add annotation to cart, perform action
 * 
 * @http-method             POST
 * @path                    <prefix>/:cid/cart_anotate
 * @url-param               cid         the cart-id
 * @post-param              remark      the new annotation
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
 function annotate_cart_post(request, response, next) {
    var cid=request.params.cid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE' ], 
        (err, permission) => {

            if (!permission.ESHOP_DESK) {
                return response.send(
                    {   status: 'error', 
                        error: "permission denied" 
                    }
                );
            }

            model.shop.annotate_cart(cid, request.body.remark,
                (err) => {
                    if (err) {
                        return response.send(
                            {   status: 'error', 
                                error: err 
                            }
                        );
                    }
                    response.send(
                        {   status: 'success', 
                            redirect: l(cid) 
                        }
                    );
                }
            );
        }
    );
}

/**
 * remove a cart and its cart items and redirect to cart list
 * 
 * @http-method             all
 * @path                    <prefix>/:cid/delete
 * @url-param               cid                     the cart-id
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function remove_cart(request, response, next) {

    var cid=request.params.cid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE' ],
        (err, permission) => {

            if (!permission.ESHOP_DESK)
                return response.redirect(l('list'));

            model.shop.remove_cart(cid, (err) => {
                if (err) return next(new Error(err));
                response.redirect(l('list'));
            });
            
        }
    );    
}

/**
 * show category and product list for a specific category
 * 
 * @http-method             all
 * @path                    <prefix>/:cid/products/:parent
 * @path                    <prefix>/:cid/products/:parent
 * @url-param               cid                     the cart-id
 * @url-param               parent                  the category
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function list_products(request, response, next) {
    var t = request.t;

    var cid=request.params.cid;
    var parent=request.params.catid || 'emma-shop';

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE' ], 
        (err, permission) => {

            if (parent == 'emma-shop') {
                show_items(
                    { name: t('Main Categories')
                    }
                );
            } else {
                model.shop.get_category(parent, (err, category) => {
                    if (err) return next(err);
        
                    show_items(category);
                });
            }
            
            function show_items(category) {
                model.shop.get_categories_and_products(parent, (err, categories, products) => {
                    if (err) return next(new Error(err));
        
                    var backlink = l(cid);
                    if (parent != 'emma-shop') {
                        backlink += '/products/' + category.category
                    }
        
                    vars = {
                        title: category.name,
                        cid: cid,
                        category: category,
                        categories: categories,
                        products: products,
                        buttons: [
                            {   title: t('Back'),
                                icon: 'back',
                                description: t('Go back to Cart List'),
                                action: backlink
                            },
                            {},
                            {},
                            {},
                            {   title: t('Search'),
                                icon: 'search',
                                description: t('search for article'),
                                action: 'search'
                            }
                        ]
                    };
                    if (permission.ESHOP_MANAGE) {
                        vars.menu=[
                            {
                                title: t('Edit Category'),
                                description: t('Change Description Text'),
                                icon:'',
                                action: l(cid, 'products', parent, 'edit')  ,
                                disabled: (parent=='emma-shop')
                            },
                            {
                                title: t('Edit Category Image'),
                                description: t('Change Category Image'),
                                icon:'',
                                action: l(cid, 'products', parent, 'image')  ,
                                disabled: (parent=='emma-shop')
                            },
                            {
                                title: t('Remove Category'),
                                description: t('Delete this Category'),
                                icon:'',
                                action: l(cid, 'products', parent, 'delete') ,
                                disabled: (parent=='emma-shop') || (categories.length>0 && products.length>0)
                            },
                            {
                                title: t('Create Category'),
                                description: t('Create a new Sub-Category'),
                                icon:'',
                                action: l(cid, 'products', parent, 'new_category')
                            },
                            {
                                title: t('Add Product'),
                                description: t('Add a Product to this Category'),
                                icon:'',
                                action: l(cid, 'products', parent, 'new_product')
                            },
                        ];
                    }
                    response.render('cart_products', vars);
                });
            }
            
        }
    );
    
}

function find_products(request, response) {
    var t = request.t;
    var cid=request.params.cid;
    var name = '%'+ request.query.name + '%';
    var backlink = l(cid + '/products');
   
    model.shop.find_products(name, (err, products) => {
       if (err) return next(new Error(err));
    
    vars = {
        title: request.query.name,
        subtitle: products.length + t(' product(s) found.'),
        products: products,
        cid: cid,
        buttons: [
            {   title: t('Back'),
                icon: 'back',
                description: t('Go back to Product List'),
                action: backlink
            },
            {},
            {},
            {},
        
        ]
    };
    response.render('find_products', vars);
  })
}


/**
 * build category list, used for SELECT input widget
 * 
 * @param {*} list                      the category tree, as resulted 
 *                                      from model.shop.get_category_tree()
 * @param {*} exclude                   exclude this category-id from list
 */
function build_cat_select(list, exclude) {
                
    if (list && list.length>0) {
        var result=[];
        for (var v in list) {
            var vv=list[v];
            if (vv.id === exclude)
                continue;
            var vr =  {
                value: vv.id,
                caption: vv.name
            };
            if (vv.items) {
                vr.items=build_cat_select(vv.items, exclude);
            }
            result.push(vr);
        }    
        return result;
    } else
        return undefined;
}

/**
 * create new category
 * 
 * @http-method             POST
 * @path                    <prefix>:cid/products/:catid/new_category
 * @url-param               cid                     the cart-id
 * @url-param               catid                   the category
 * @post-body                                       the category record
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function new_category_post(request, response) {
    var cid=request.params.cid;
    var catid=request.params.catid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE' ], 
        (err, permission) => {
    
            if (!permission.ESHOP_MANAGE) 
                return response.send(
                    {   status: 'error',
                        error: 'permission denied',
                        stack: 'permission denied in new_category_post()',
                    }
                );

            model.shop.create_category(request.body, (err) => {
                if (err) {
                    response.send(
                        {   status: 'error', 
                            error: err.message,
                            stack: error.stack
                        }
                    );
                } else {
                    response.send(
                        {   status: 'success', 
                            redirect: l(cid, 'products', request.body.id) 
                        }
                    );
                }
            });

        }
    );
    
}

/**
 * form to create new category
 * 
 * @http-method             GET
 * @path                    <prefix>:cid/products/:catid/new_category
 * @url-param               cid                     the cart-id
 * @url-param               catid                   the category
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function new_category(request, response, next) {
    var t = request.t;

    var cid=request.params.cid;
    var catid=request.params.catid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE' ], 
        (err, permission) => {

            if (!permission.ESHOP_MANAGE)
                return response.redirect(l('list'));

            var new_id = uuid();

            vars = {
                title: t('Create Category'),
                catid: catid,
                buttons: [
                    {   title: t('Back'),
                        icon: 'back',
                        description: t('Go back to Cart List'),
                        action: l(cid, 'products')
                    },
                    {},
                    {},
                    {},
                    {   title: t('Save'),
                        icon: 'save',
                        description: 'Save Changes',
                        action: 'post-form'
                    }
                ]
            };

            var record = {};
            if (request.query.payload) {
                record=request.query.payload;
            } 

            model.shop.get_category(catid, (err, category) => {
                if (err) 
                    return next(new Error(err));
                if (category==undefined)
                    return next(new Error('No such Category: '+catid));

                model.shop.get_category_tree( (err, categories) => {
                    if (err) 
                        return next(err);

                    vars.form = {
                        id: 'cat-create',
                        action: l(cid, 'products', catid, 'new_category'),
                        values: {
                            id: new_id,
                            name: t('new category'),
                            sort_order: '',
                            category: catid
                        },
                        fields: [
                            {   name: 'id',
                                type: '￼const'
                            },
                            {   name: 'name',
                                caption: t('Name'), 
                                type: 'text',
                                required: true
                            },
                            {   name: 'category',
                                caption: t('Category'),
                                type: 'select',
                                required: true,
                                values: build_cat_select([categories], ''),
                                v: categories
                            },
                            {   name: 'sort_order',
                                caption: t('Sort Order'),
                                type: 'number'
                            },
                        ]
                    };
                
                    response.render('cart_category_edit', vars);
                });
            });

        }
    );

}

/**
 * update a category
 * 
 * @http-method             POST
 * @path                    <prefix>:cid/products/:catid/edit_category
 * @url-param               cid                     the cart-id
 * @url-param               catid                   the category
 * @post-body                                       the category record
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function edit_category_post(request, response) {
    var cid=request.params.cid;
    var catid=request.params.catid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE' ], 
        (err, permission) => {

            if (!permission.ESHOP_MANAGE) 
                return response.send(
                    {   status: 'error',
                        error: 'permission denied',
                        stack: 'permission denied in new_category_post()',
                    }
                );

            model.shop.update_category(request.body, (err) => {
                if (err) {
                    response.send(
                        {   status: 'error', 
                            error: err.message,
                            stack: err.stack
                        }
                    );
                } else {
                    response.send(
                        {   status: 'success', 
                            redirect: l(cid,'products', catid) 
                        }
                    );
                }
            });

        }
    );
}

/**
 * form to create edit a category
 * 
 * @http-method             GET
 * @path                    <prefix>:cid/products/:catid/image
 * @url-param               cid                     the cart-id
 * @url-param               catid                   the category
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function edit_category(request, response, next) {
    var t = request.t;

    var cid=request.params.cid;
    var catid=request.params.catid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE' ], 
        (err, permission) => {

            if (!permission.ESHOP_MANAGE)
                return response.redirect(l('list'));

            var record;
            if (request.query.payload) {
                record=request.query.payload;
            } else {
                record = {};
            }

            vars = {
                catid: catid,
                buttons: [
                    {   title: t('Back'),
                        icon: 'back',
                        description: t('Go back to Cart List'),
                        action: '/cart/' + cid + '/products/' + catid
                    },
                    {},
                    {},
                    {},
                    {   title: t('Save'),
                        icon: 'save',
                        description: 'Save Changes',
                        action: 'post-form',
                        default: true
                    }
                ]
            };

            model.shop.get_category(catid, (err, category) => {
                if (err) 
                    return next(new Error(err));
                if (category==undefined)
                    return next(new Error('No such Category: '+catid));

                for (var k in record) {
                    category[k]=record[k];
                }
                
                vars.category = category;
                vars.title = t('Edit') + ': ' + category.name;

                model.shop.get_category_tree( (err, categories) => {
                    if (err) 
                        return next(err);

                    vars.form = {
                        id: 'cat-edit',
                        action:  l(cid, 'products', catid, 'edit'),
                        values: category,
                        fields: [
                            {   name: 'id',
                                type: 'const'
                            },
                            {   name: 'name',
                                caption: t('Name'), 
                                type: 'text',
                                required: true
                            },
                            {   name: 'category',
                                caption: t('Category'),
                                type: 'select',
                                required: true,
                                values: build_cat_select([categories], category.id),
                                v: categories
                            },
                            {   name: 'searchable',
                                caption: t('Sortable'),
                                type: 'select',
                                values: [
                                    { value: '0', caption: 'Yes' },
                                    { value: '1', caption: t('No') }
                                ]
                            },
                            {   name: 'sort_order',
                                caption: t('Sort Order'),
                                type: 'number'
                            },
                        ]
                    };
                
                    response.render('cart_category_edit', vars);
                });
            });

        }
    );
}

/**
 * update a category image
 * 
 * @http-method             POST
 * @path                    <prefix>:cid/products/:catid/image
 * @url-param               cid                     the cart-id
 * @url-param               catid                   the category
 * @post-body                                       the category image
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function edit_category_image_post(request, response, next) {
    var cid=request.params.cid;
    var catid=request.params.catid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE' ], 
        (err, permission) => {

            if (!permission.ESHOP_MANAGE) 
                return response.send(
                    {   status: 'error',
                        error: 'permission denied',
                        stack: 'permission denied in new_category_post()',
                    }
                );

            model.shop.update_category_image(request.body, (err) => {
                if (err) {
                    response.send(
                        {   status: 'error', 
                            error: err.message,
                            stack: err.stack
                        }
                    );
                } else {
                    response.send(
                        {   status: 'success', 
                            redirect: l(cid, 'products', catid) 
                        }
                    );
                }
            });
        }
    );
}

/**
 * form to create edit a category image
 * 
 * @http-method             GET
 * @path                    <prefix>:cid/products/:catid/edit_category
 * @url-param               cid                     the cart-id
 * @url-param               catid                   the category
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function edit_category_image(request, response, next) {
    var t = request.t;

    var cid=request.params.cid;
    var catid=request.params.catid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE' ], 
        (err, permission) => {

            if (!permission.ESHOP_MANAGE)
                return response.redirect(l('list'));

            var record;
            if (request.query.payload) {
                record=request.query.payload;
            } else {
                record = {};
            }

            vars = {
                catid: catid,
                buttons: [
                    {   title: t('Back'),
                        icon: 'back',
                        description: t('Go back to Product List'),
                        action: l(cid, 'products', catid)
                    },
                    {},
                    {},
                    {},
                    {   title: t('Save'),
                        icon: 'save',
                        description: t('Save Changes'),
                        action: 'post-form'
                    }
                ]
            };

            model.shop.get_category(catid, (err, category) => {
                if (err) 
                    return next(new Error(err));
                if (category==undefined)
                    return next(new Error('No such Category: ' + catid));

                vars.title = t('Edit Image') + ': ' + category.name;        
                vars.action = l(cid, 'products', catid, 'image');

                vars.id=catid;
                vars.image=category.image || '/img/category-image.png';

                response.render('cart_category_image', vars);

            });
      
        }
    );
}

/**
 * remove a category
 * 
 * @http-method             GET
 * @path                    <prefix>:cid/products/:catid/remove_category
 * @url-param               cid                     the cart-id
 * @url-param               catid                   the category
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function remove_category(request, response, next) {
    var t = request.t;

    var cid=request.params.cid;
    var catid=request.params.catid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE' ], 
        (err, permission) => {

            if (!permission.ESHOP_MANAGE)
                return response.redirect(l('list'));

            model.shop.get_category(catid, (err, category) => {
                if (err) 
                    return next(new Error(err));
                if (category==undefined)
                    return next(new Error('No such Category: '+catid));

                model.shop.remove_category(catid, (err) => {
                    if (err)
                        return next(new Error(err));

                    response.redirect(l(catid, 'products',  category.category));

                });
            });

        }
    );
}

/**
 * create a new product and redirect to category list
 * 
 * @http-method             POST
 * @path                    <prefix>:cid/products/:catid/new_product
 * @url-param               cid                     the cart-id
 * @url-param               catid                   the category
 * @post-body                                       the product record
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function new_product_post(request, response, next) {
    var t = request.t;

    var cid=request.params.cid;
    var catid=request.params.catid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE' ], 
        (err, permission) => {

            if (!permission.ESHOP_MANAGE) 
                return response.send(
                    {   status: 'error',
                        error: 'permission denied',
                        stack: 'permission denied in new_category_post()',
                    }
                );

            model.shop.create_product(request.body, (err) => {
                if (err) {
                    response.send(
                        {   status: 'error', 
                            error: err.message,
                            stack: err.stack
                        }
                    );
                } else {
                    response.send(
                        {   status: 'success', 
                            redirect: l(cid, 'products', catid) 
                        }
                    );
                }
            });
        }
    );
}


function build_customer_select(list) {
    let values = [ {
        value: '',
        caption: '< >'
    }];
    for(let n=0; n<list.length; ++n) {
        let customer = list[n];
        values.push({
            value: customer.id,
            caption: customer.name
        });
    }
    return values;
}



/**
 * form to create a new product
 * 
 * @http-method             GET
 * @path                    <prefix>:cid/products/:catid/new_product
 * @url-param               cid                     the cart-id
 * @url-param               catid                   the category
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function new_product(request, response, next) {
    var t = request.t;

    var cid=request.params.cid;
    var catid=request.params.catid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE' ], 
        (err, permission) => {

            if (!permission.ESHOP_MANAGE)
                return response.redirect(l('list'));

            vars = {
                catid: catid,
                buttons: [
                    {   title: t('Back'),
                        icon: 'back',
                        description: t('Go back to Product List'),
                        action: l(cid, 'products', catid)
                    },
                    {},
                    {},
                    {},
                    {   title: t('Save'),
                        icon: 'save',
                        description: t('Save Changes'),
                        action: 'post-form',
                        default: true
                    }
                ]
            };

            var product = {
                id: uuid(),
                name: t('new product'),
                category: catid
            }; 

            model.shop.get_category_tree( (err, categories) => {
                if (err) 
                    return next(new Error(err));

                model.admin.get_customers(
                    (err, customers) => {
                        if (err) 
                            return next(new Error(err));
        
                        vars.form = {
                            id: 'prod-edit',
                            action: l(cid, 'products', catid, 'new_product'),
                            values: product,
                            fields: [
                                {   name: 'id',
                                    type: 'const'
                                },
                                {   name: 'name',
                                    caption: t('Name'), 
                                    type: 'text',
                                    required: true
                                },
                                {   name: 'description',
                                    caption: t('Description'), 
                                    type: 'text',
                                },
                                {   name: 'category',
                                    caption: t('Category'),
                                    type: 'select',
                                    required: true,
                                    values: build_cat_select([categories])
                                },
                                {   name: 'commissioner',
                                    caption: t('Commissioner'), 
                                    type: 'select',
                                    values: build_customer_select(customers)
                                },
                                {   name: 'commission',
                                    caption: t('Commission'), 
                                    type: 'percent',
                                },
                                {   name: 'price',
                                    caption: t('Price'), 
                                    type: 'currency',
                                    required: true
                                },
                                {   name: 'vat',
                                    caption: t('Vat'), 
                                    type: 'percent',
                                },
                                {   name: 'unit',
                                    caption: t('Unit'), 
                                    type: 'select',
                                    values: [
                                        { value: 'peace', caption: t('pc.') },
                                        { value: 'kg', caption: t('kg') }
                                    ]
                                },
                                {   name: 'stock',
                                    caption: t('Stock'), 
                                    type: 'number'
                                },
        
                            ]
                        };
                    
                        response.render('cart_product_edit', vars);                   
                    }
                );

            });
        }
    );

}

/**
 * update a product and redirect to category list
 * 
 * @http-method             POST
 * @path                    <prefix>:cid/product/:pid/edit
 * @url-param               cid                     the cart-id
 * @url-param               pid                     the product id
 * @post-body                                       the product record
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function edit_product_post(request, response, next) {
    var t = request.t;

    var cid=request.params.cid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE' ], 
        (err, permission) => {

            if (!permission.ESHOP_MANAGE) 
                return response.send(
                    {   status: 'error',
                        error: 'permission denied',
                        stack: 'permission denied in edit_product_post()',
                    }
                );

            model.shop.update_product(request.body, (err) => {
                if (err) {
                    response.send(
                        {   status: 'error', 
                            error: err.message,
                            stack: err.stack
                        }
                    );
                } else {
                    response.send(
                        {   status: 'success', 
                            redirect: l(cid , 'products', request.body.category) 
                        }
                    );
                }
            });

        }
    );
}

/**
 * form to edit a product
 * 
 * @http-method             GET
 * @path                    <prefix>:cid/product/:pid/edit
 * @url-param               cid                     the cart-id
 * @url-param               pid                     the product-id
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function edit_product(request, response, next) {
    var t = request.t;

    var cid=request.params.cid;
    var pid=request.params.pid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE' ], 
        (err, permission) => {

            if (!permission.ESHOP_MANAGE)
               return response.redirect(l('list'));

            model.shop.get_product(pid, (err, product) => {
                if (err)
                    return next(new Error(err));
                if (product==undefined)
                    return next(new Error('No such Product: '+pid));

                var catid = product.category;

                vars = {
                    buttons: [
                        {   title: t('Back'),
                            icon: 'back',
                            description: t('Go back to Product List'),
                            action: l(cid, 'products', catid)
                        },
                        {},
                        {},
                        {},
                        {   title: t('Save'),
                            icon: 'save',
                            description: t('Save Changes'),
                            action: 'post-form',
                            default: true
                        }
                    ]
                };
            
                model.shop.get_category_tree( (err, categories) => {
                    if (err) 
                        return next(new Error(err));
     
                    model.admin.get_customers(
                        (err, customers) => {
                            if (err) 
                                return next(new Error(err));
        
                            vars.form = {
                                id: 'prod-edit',
                                action: l(cid, 'product', pid, 'edit'),
                                values: product,
                                fields: [
                                    {   name: 'id',
                                        type: 'const'
                                    },
                                    {   name: 'name',
                                        caption: t('Name'), 
                                        type: 'text',
                                        required: true
                                    },
                                    {   name: 'description',
                                        caption: t('Description'), 
                                        type: 'text',
                                    },
                                    {   name: 'category',
                                        caption: t('Category'),
                                        type: 'select',
                                        required: true,
                                        values: build_cat_select([categories])
                                    },
                                    {   name: 'commissioner',
                                        caption: t('Commissioner'), 
                                        type: 'select',
                                        values: build_customer_select(customers)
                                    },
                                    {   name: 'commission',
                                        caption: t('Commission'), 
                                        type: 'percent',
                                    },
                                    {   name: 'price',
                                        caption: t('Price'), 
                                        type: 'currency',
                                        required: true
                                    },
                                    {   name: 'vat',
                                        caption: t('Vat'), 
                                        type: 'percent',
                                    },
                                    {   name: 'unit',
                                        caption: t('Unit'), 
                                        type: 'select',
                                        values: [
                                            { value: 'peace', caption: t('pc.') },
                                            { value: 'kg', caption: t('kg') }
                                        ]
                                    },
                                    {   name: 'stock',
                                        caption: t('Stock'), 
                                        type: 'number'
                                    },
                    
                                ]
                            };
                
                            response.render('cart_product_edit', vars);
                        }
                    );
                });
            });
        }
    );

}

/**
 * update a product image and redirect to product view
 * 
 * @http-method             POST
 * @path                    <prefix>:cid/product/:pid/image
 * @url-param               cid                     the cart-id
 * @url-param               pid                     the product id
 * @post-body                                       the image record
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function edit_product_image_post(request, response, next) {
    var cid=request.params.cid;
    var pid=request.params.pid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE' ], 
        (err, permission) => {

            if (!permission.ESHOP_MANAGE) 
                return response.send(
                    {   status: 'error',
                        error: 'permission denied',
                        stack: 'permission denied in edit_product_post()',
                    }
                );

            model.shop.get_product(pid, (err, product) => {
                
                if (err) return next(err);
                model.shop.update_product_image(request.body, (err) => {
                    if (err) {
                        response.send(
                            {   status: 'error', 
                                error: err.message,
                                stack: err.stack
                            }
                        );
                    } else {
                        response.send(
                            {   status: 'success', 
                                redirect: l(cid, 'product', pid) 
                            }
                        );
                    }
                });
            
            });
        }
    );
}

/**
 * form to edit a product
 * 
 * @http-method             GET
 * @path                    <prefix>:cid/product/:pid/image
 * @url-param               cid                     the cart-id
 * @url-param               pid                     the product-id
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function edit_product_image(request, response, next) {
    var t = request.t;

    var cid=request.params.cid;
    var pid=request.params.pid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE' ], 
        (err, permission) => {

            if (!permission.ESHOP_MANAGE)
                return response.redirect(l('list'));

            var record;
            if (request.query.payload) {
                record=request.query.payload;
            } else {
                record = {};
            }

            model.shop.get_product(pid, (err, product) => {
                if (err) 
                    return next(new Error(err));
                if (product==undefined)
                    return next(new Error('No such Product: '+pid));

                vars = {
                    buttons: [
                        {   title: t('Back'),
                            icon: 'back',
                            description: t('Go back to Product List'),
                            action: l(cid, 'product', pid)
                        },
                        {},
                        {},
                        {},
                        {   title: t('Save'),
                            icon: 'save',
                            description: t('Save Changes'),
                            action: 'post-form'
                        }
                    ]
                };
                
                vars.title = t('Edit Image') + ': ' + product.name;        
                vars.action = l(cid, 'product', pid, 'image');

                vars.id=pid;
                vars.image=product.image || '/img/product-image.png';

                response.render('cart_category_image', vars);

            });
        }
    );
}

/**
 * form to put a product into cart
 * 
 * @http-method             GET
 * @path                    <prefix>:cid/product/:pid
 * @url-param               cid                     the cart-id
 * @url-param               pid                     the product-id
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function show_product(request, response, next) {
    var t = request.t;

    var cid=request.params.cid;
    var pid=request.params.pid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE'], 
        (err, permission) => {

            model.shop.get_product(pid, (err, product) => {

                if (err) 
                    return next(new Error(err));
                if (product == undefined) 
                    return next(new Error('Non-existing Product:' + pid));

                model.shop.get_category_info(product.category, (err, cat_info) => {
                    vars = {
                        product: product,
                        cat_info: cat_info||{},
                        buttons: [
                            {   title: t('Back'),
                                icon: 'back',
                                description: t('Go back to Product List'),
                                action: l(cid, 'products', product.category)
                            },
                            {},
                            {},
                            {},
                            {   title: t('Buy Product'),
                                icon: 'cart',
                                description: t('Put into Cart'),
                                action: 'post-form',
                                default: true
                            }
                        ]
                    };
    
                    var cart_item = {
                        id: uuid(),
                        cart: cid,
                        product: pid,
                        name: product.name,
                        amount: 1,
                        vat: product.vat,
                        price: product.price
                    };
    
                    vars.cart_item = cart_item;
                    vars.form = {
                        id: 'cartitem-add',
                        action: l(cid, 'product', pid),
                        values: cart_item,
                        fields: [
                            {   name: 'id',
                                type: 'const'
                            },
                            {   name: 'cart',
                                type: 'const'
                            },
                            {   name: 'product',
                                type: 'const'
                            },
                            {   name: 'name',
                                type: 'const'
                            },
                            {   name: 'amount',
                                caption: t('Amount'),
                                type: 'keypad-number',
                                required: true
                            },
                            {   name: 'price',
                                caption: t('Price'),
                                type : 'keypad-currency',
                                required: true
                            },
                            {   name: 'vat',
                                caption: t('VAT'),
                                type: 'keypad-percent'
                            }
                        ]
                    };
    
                    if (permission.ESHOP_MANAGE) {
                        vars.menu=[
                            {
                                title: t('Edit Product'),
                                description: t('Edt Product Data'),
                                icon:'',
                                action: l(cid, 'product', pid, 'edit')  
                            },
                            {
                                title: t('Edit Product Image'),
                                description: t('Change Product Image'),
                                icon:'',
                                action: l(cid, 'product', pid, 'image') 
                            },
                            {
                                title: t('Scan Barcode'),
                                description: t('Scan Barcode from Product Sample'),
                                icon:'',
                                action: l(cid, 'products', pid, 'barcode')
                            },
                        ];
                    }
    
                    response.render('cart_product_view', vars);
                });
            });
        }
    );
}

/**
 * put a product into cart and redirect to cart view
 * 
 * @http-method             POST
 * @path                    <prefix>:cid/product/:pid
 * @url-param               cid                     the cart-id
 * @url-param               pid                     the product-id
 * @post-body                                       the cartitem record
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function show_product_post(request, response, next) {
    var t = request.t;

    var cid=request.params.cid;
    var pid=request.params.pid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK'], 
        (err, permission) => {

            model.shop.create_cartitem(request.body, (err) => {
                if (err) {
                    response.send(
                        {   status: 'error', 
                            error: err.message,
                            stack: err.stack
                        }
                    );
                } else {
                    response.send(
                        {   status: 'success', 
                            redirect: l(cid) 
                        }
                    );
                }
            });
        }
    );
}

/**
 * form to edit a cart item
 * 
 * @http-method             GET
 * @path                    <prefix>:cid/item/:itemid
 * @url-param               cid                     the cart-id
 * @url-param               itemid                  the cartitem-id
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function edit_cartitem(request, response, next) {
    var t = request.t;

    var cid = request.params.cid;
    var itemid = request.params.itemid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE' ], 
        (err, permission) => {

            model.shop.get_cartitem(itemid, (err, item) => {
                if (err)
                    return next(new Error(err));
                if (item==undefined)
                    return next(new Error('No such Item: '+itemid));

                    var pid=item.product;

                    model.shop.get_product(pid, (err, product) => {

                        if (err) 
                            return next(new Error(err));
                        if (product == undefined) 
                            return next(new Error('No such Product:' + pid));
                
                        model.shop.get_category_info(product.category, (err, cat_info) => {

                            vars = {
                                product: product,
                                cat_info: cat_info||{},
                                buttons: [
                                    {   title: t('Back'),
                                        icon: 'back',
                                        description: t('Go back to Cart'),
                                        action: l(cid)
                                    },
                                    {   title: t('delete'),
                                        icon: 'trash',
                                        description: t('Remove this Item'),
                                        action: l(cid, 'item', itemid, 'remove')
                                    },
                                    {},
                                    {},
                                    {   title: t('Buy Product'),
                                        icon: 'cart',
                                        description: t('Put into Cart'),
                                        action: 'post-form',
                                        default: true
                                    }
                                ]
                            };
                    
                            vars.cart_item = item;
                            vars.form = {
                                id: 'cartitem-add',
                                action: l(cid, 'item', itemid),
                                values: item,
                                fields: [
                                    {   name: 'id',
                                        type: 'const'
                                    },
                                    {   name: 'cart',
                                        type: 'const'
                                    },
                                    {   name: 'product',
                                        type: 'const'
                                    },
                                    {   name: 'name',
                                        type: 'const'
                                    },
                                    {   name: 'amount',
                                        caption: t('Amount'),
                                        type: 'keypad-number',
                                        required: true
                                    },
                                    {   name: 'price',
                                        caption: t('Price'),
                                        type : 'keypad-currency',
                                        required: true
                                    },
                                    {   name: 'vat',
                                        caption: t('VAT'),
                                        type: 'keypad-percent'
                                    }
                                ]
                            };
                    
                            if (permission.ESHOP_MANAGE) {
                                vars.menu=[
                                    {
                                        title: t('Edit Product'),
                                        description: t('Edt Product Data'),
                                        icon:'',
                                        action: l(cid, 'product', pid, 'edit')  
                                    },
                                    {
                                        title: t('Edit Product Image'),
                                        description: t('Change Product Image'),
                                        icon:'',
                                        action: l(cid, 'product', pid, 'image') 
                                    },
                                    {
                                        title: t('Scan Barcode'),
                                        description: t('Scan Barcode from Product Sample'),
                                        icon:'',
                                        action: l(cid, 'products', pid, 'barcode')
                                    },
                                ];
                            }
                    
                            response.render('cart_product_view', vars);
                    
                        });
                    });
            });
        }
    );
}

/**
 * update a cartitem and redirect to cart view
 * 
 * @http-method             POST
 * @path                    <prefix>:cid/item/:itemid
 * @url-param               cid                     the cart-id
 * @url-param               itemid                  the cartitem-id
 * @post-body                                       the cartitem record
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function edit_cartitem_post(request, response, next) {
    var t = request.t;

    var cid=request.params.cid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK'], 
        (err, permission) => {

            model.shop.update_cartitem(request.body, (err) => {
                if (err) {
                    response.send(
                        {   status: 'error', 
                            error: err.message,
                            stack: err.stack
                        }
                    );
                } else {
                    response.send(
                        {   status: 'success', 
                            redirect: l(cid) 
                        }
                    );
                }
            });
        }
    );
}

/**
 * remove a cart item and redirect to cart view
 * 
 * @http-method             GET
 * @path                    <prefix>:cid/item/:itemid/remove
 * @url-param               cid                     the cart-id
 * @url-param               itemid                  the cartitem-id
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function remove_cartitem(request, response, next) {
    var t = request.t;

    var cid = request.params.cid;
    var itemid = request.params.itemid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK'], 
        (err, permission) => {

            model.shop.remove_cartitem(itemid, (err) => {
                if (err)
                    return next(new Error(err));

                response.redirect(l(cid));
            });
        }
    );
}

/**
 * checkout by cash form
 * 
 * @http-method             GET
 * @path                    <prefix>:cid/checkout
 * @url-param               cid                     the cart-id
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function checkout(request, response, next) {
    var t = request.t;

    var cid = request.params.cid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK'], 
        (err, permission) => {

            model.shop.get_cart(cid, (err, cart) => {

                vars = {
                    title: 'Checkout',

                    buttons: [
                        {   title: t('Back'),
                            icon: 'back',
                            description: t('Go back to Cart'),
                            action: l(cid)
                        },
                        {},
                        {},
                        {   title: t('Pay by Debit'),
                            icon: 'moneypig',
                            description: t('Put into Cart'),
                            action: l(cid, 'checkout2')
                        },
                        {   title: t('Pay Cash'),
                            icon: 'moneybag',
                            description: t('Pay Cash'),
                            action: l(cid, 'checkout_cash')
                        },
                    ],
                    form: {
                        id: 'checkout',
                        values: {},
                        fields: [
                            {   name: 'given',
                                caption: t('Given'),
                                type: 'keypad-currency'
                            }
                        ]
                    }
                };

                vars.cart = cart;

                response.render('cart_checkout', vars);

            });
        }
    );
}

/**
 * checkout by debit form
 * 
 * @http-method             GET
 * @path                    <prefix>:cid/checkout
 * @url-param               cid                     the cart-id
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function checkout2(request, response, next) {
    var t = request.t;

    var cid = request.params.cid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK'], 
        (err, permission) => {

            model.shop.get_cart(cid, (err, cart) => {

                vars = {
                    title: 'Checkout by Debit',

                    buttons: [
                        {   
                            title: t('Back'),
                            icon: 'back',
                            description: t('Go back to Cart'),
                            action: l(cid)
                        },
                        {},
                        {},
                        {   title: t('Pay Cash'),
                            icon: 'moneybag',
                            description: t('Pay Cash'),
                            action: l(cid, 'checkout')
                        },
                        {   title: t('Pay'),
                            icon: 'moneypig',
                            description: t('Pay by Debit'),
                            action: 'post-form',
                            default: true
                        }
                    ],
                    form: {
                        action: l(cid, 'checkout2'),
                        values: {},
                        fields: [
                            {   name: 'pin',
                                caption: 'Pin',
                                type: 'keypad-pin'
                            }
                        ]
                    }
                };

                vars.cart = cart;

                response.render('cart_checkout_debit', vars);

            });
        }
    );
}

/**
 * deprecated, should use adm/check-pin
 * 
 * @param {*} request 
 * @param {*} response 
 * @param {*} next 
 */
function checkout2_check(request, response, next) {
    var t = request.t;

    var cid = request.params.cid;
    var pin = request.body.pin;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK'], 
        (err, permission) => {
            
            model.coop.get_customer_by_pin(pin, (err, customer) => {
                if (err) {
                    response.send(
                        {   status: 'error', 
                            error: err.message,
                            stack: err.stack
                        }
                    );
                }
                model.shop.get_cart(cid, (err, cart) => {

                    if (customer == undefined) {
                        response.send(
                            {   status: 'failure', 
                                error: t('bad pin'),
                            }
                        );    
                    } else {
                        response.send(
                            {   status: 'success', 
                                customer: customer.name,
                                balance: t.format_currency(customer.balance) + '€'
                            }
                        );                            
                    }
                });
            });
        }
    );
}

/**
 * perform the checkout
 * 
 * - update product stock; 
 * - calculate & payout commissions
 * - TODO: mark cartitem as 'paid'
 * 
 * TODO: move functionality to model 
 * 
 * @param {*} items 
 * @param {*} callback 
 */
function perform_item_checkout(items, callback) {
    process_items();
    function process_items() {
        if (items.length>0) {

            var item=items.shift();

            model.shop.get_product(item.product, (err, product) => {
                if (err) 
                    return callback(new Error(err));
                if (product == undefined)
                    return callback(new Error('No such Product: '+item.product));

                product.stock = (product.stock || 0) - item.amount;
                
                // don't allow negative stock
                if (product.stock<0) product.stock=0;

                model.shop.update_product(product, (err) => {
                    if (err) 
                        return callback(new Error(err));
                    if (product.commission && product.commissioner) {
                        let price = Number(item.price) * Number(item.amount);
                        let commission = price*(Number(product.commission)/100);

                        model.coop.add_to_journal( {
                            type: 'commission',
                            description: 'Kommission fuer ' + product.name,
                            customer: product.commissioner,
                            amount: commission,
                            vat:0 
                        }, (err) => {
                            if (err) 
                                return callback(new Error(err));
                            
                            model.coop.add_to_journal( {
                                type: 'commision payout',
                                description: 'Kommission fuer ' + product.name,
                                customer: 'emma-shop',
                                amount: -commission,
                                vat: 0
                            }, (err) => {
                                if (err) 
                                    return callback(new Error(err));

                                // continue with next item
                                process_items();
                            });
                        });

                    } else {
                        // continue with next item
                        process_items();
                    }
                });
            });

        } else {
            callback(null);
        }
    }
}

/**
 * checkout by cash and redirect to cart list
 * 
 * @http-method             GET
 * @path                    <prefix>:cid/checkout_cash
 * @url-param               cid                     the cart-id
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function checkout_cash(request, response, next) {
    var t = request.t;

    var cid = request.params.cid;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK'], 
        (err, permission) => {

            model.shop.get_cart(cid, (err, cart, items) => {
                if (err) 
                    return next(new Error(err));
                if (cart == undefined)
                    return next(new Error('No such Cart: '+cid))

                perform_item_checkout(items, (err) => {
                    if (err) 
                        return next(new Error(err));

                    model.shop.add_to_journal(
                        {   cart: cart.id,
                            amount: cart.total,
                            vat: cart.vat_total,
                            type: 'cart payment'
                        }, (err) => {
                            if (err) 
                                return next(new Error(err));

                            //TODO: add main_journal entry

                            cart.state = 'paid cash';
                            cart.payment = 'cash';
                            model.shop.update_cart(cart, (err) => {
                                if (err) 
                                    return next(new Error(err));

                                response.redirect('/cart');
                            });
                        }
                    )
                });
            });
        }
    );
}

/**
 * checkout by debit and redirect to cart list
 * 
 * @http-method             POST
 * @path                    <prefix>:cid/checkout_cash
 * @url-param               cid                     the cart-id
 * @post-body                                       the customer pin
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function checkout_prepaid(request, response, next) {
    var t = request.t;

    var cid = request.params.cid;
    var pin = request.body.pin;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK'], 
        (err, permission) => {

            model.coop.get_customer_by_pin(pin, (err, customer) => {
                if (err) {
                    return response.send(
                        {   status: 'error', 
                            error: err.message,
                            stack: err.stack
                        }
                    );
                }
                model.shop.get_cart(cid, (err, cart, items) => {
                    if (err) {
                        return response.send(
                            {   status: 'error', 
                                error: err.message,
                                stack: err.stack
                            }
                        );
                    }
                    if (cart == undefined)
                        return next(new Error('No such Cart: '+cid))

                    perform_item_checkout(items, (err) => {
                        if (err) {
                            return response.send(
                                {   status: 'error', 
                                    error: err.message,
                                    stack: err.stack
                                }
                            );
                        }
            
                        model.coop.add_to_journal({
                            customer: customer.id,
                            amount: -cart.total,
                            vat: -cart.vat_total,
                            cart: cart.id,
                            type: 'prepaid payment',
                            description: 'Prepaid-Zahlung'
                        }, (err) => {
                            if (err) {
                                return response.send(
                                    {   status: 'error', 
                                        error: err.message,
                                        stack: err.stack
                                    }
                                );
                            }
    
                            model.coop.add_to_journal({
                                customer: 'emma-shop',
                                amount: cart.total,
                                vat: cart.vat_total,
                                type: 'prepaid payment',
                                description: 'Prepaid-Zahlung von ' + customer.name
                            }, (err) => {
                                if (err) {
                                    return response.send(
                                        {   status: 'error', 
                                            error: err.message,
                                            stack: err.stack
                                        }
                                    );
                                }
                        
                                cart.state = 'paid prepaid';
                                cart.customer = customer.id;
                                cart.payment = 'prepaid';
                                model.shop.update_cart(cart, (err) => {
                                    if (err) {
                                        return response.send(
                                            {   status: 'error', 
                                                error: err.message,
                                                stack: err.stack
                                            }
                                        );
                                    }
                            
                                    response.send(
                                        {   status: 'success', 
                                            redirect: l('list') 
                                        }
                                    );
                                });
                            });
                        });
                    });
                });
            });
        }
    );
}

/**
 * form to payout cash to customer
 * 
 * @http-method             GET
 * @path                    <prefix>/chashout
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function cashout(request, response, next) {
    var t = request.t;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE'], 
        (err, permission) => {
            if (err) return next(new Error(err));
            if (!permission.ESHOP_MANAGE)   return response.redirect(l('index'));

            var vars = {
                title: t('Cash-Out to Coop Member'),
                buttons: [
                    {   title: t('Back'),
                        icon: 'back',
                        action: l('list')
                    },
                    {},
                    {},
                    {   title: t('Cash-In'),
                        icon: 'cashin',
                        description: 'Rceive Cash from Member',
                        action: l('cashin')
                    },
                    {   title: t('Cash-Out'),
                        icon: 'cashout',
                        description: 'Payout Cash',
                        disabled: true,
                        action: 'post-form'
                    }
                ],
                form: {
                    id: 'cashout',
                    action: l('cashout'),
                    values: {},
                    fields: [
                        {   name: "pin",
                            caption: t('Pin'),
                            type: "pin",
                            required: true
                        },
                        {   name: "amount",
                            caption: t('Amount'),
                            type: "currency",
                            required: true
                        },
                        {   name: "remark", 
                            caption: t('Remark'),
                            type: 'text'
                        },
                    ]
                }

            }

            response.render('cart_cashout', vars);
        }
    );
}

/**
 * payout cash to customer and redirect to cart list
 * 
 * @http-method             POST
 * @path                    <prefix>/cashout
 * @post-body                                       the customer pin and amount
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function cashout_post(request, response, next) {
    var t = request.t;

    var pin=request.body.pin;
    var remark=request.body.remark;
    var amount=Number(request.body.amount);

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE'], 
        (err, permission) => {

            model.coop.get_customer_by_pin(pin, (err, customer) => {

                if (err) {
                    return response.send(
                        {   status: 'error', 
                            error: err.message,
                            stack: err.stack
                        }
                    );
                }
                if (customer==undefined) {
                    return response.send(
                        {   status: 'failure', 
                            redirect: 'bad customer pin' 
                        }
                    );

                }

                model.coop.add_to_journal({
                    customer: customer.id,
                    amount: -amount,
                    vat:0,
                    type: 'cash payout from shop',
                    remark: remark
                }, (err) => {

                    if (err) {
                        return response.send(
                            {   status: 'error', 
                                error: err.message,
                                stack: err.stack
                            }
                        );
                    }

                    model.coop.add_to_journal({
                        customer: 'emma-shop',
                        amount: amount,
                        vat:0,
                        type: 'cash payout to customer',
                        remark: remark 
                    }, (err) => {

                        if (err) {
                            return response.send(
                                {   status: 'error', 
                                    error: err.message,
                                    stack: err.stack
                                }
                            );
                        }

                        model.shop.add_to_journal({
                            amount: -amount, 
                            vat: 0,
                            type: 'cash payout to customer'
                        }, (err) => {

                            if (err) {
                                return response.send(
                                    {   status: 'error', 
                                        error: err.message,
                                        stack: err.stack
                                    }
                                );
                            }

                            response.send(
                                {   status: 'success', 
                                    redirect: l('list') 
                                }
                            );
            
                        })
                    })
                })
            });
        }
    );
}

/**
 * form to receive cash from customer
 * 
 * @http-method             GET
 * @path                    <prefix>/chashout
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function cashin(request, response, next) {
    var t = request.t;

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE'], 
        (err, permission) => {
            if (err) return next(new Error(err));
            if (!permission.ESHOP_MANAGE)   return response.redirect(l('index'));

            var vars = {
                title: t('Cash-In from Coop Member'),
                buttons: [
                    {   title: t('Back'),
                        icon: 'back',
                        action: l('list')
                    },
                    {},
                    {},
                    {   title: t('Cash-Out'),
                        icon: 'cashout',
                        description: 'Pay Cash to Member',
                        action: l('cashout')
                    },
                    {   title: t('Cash-In'),
                        icon: 'cashin',
                        description: 'Receive Cash from Member',
                        disabled: true,
                        action: 'post-form'
                    }
                ],
                form: {
                    id: 'cashin',
                    action: l('cashin'),
                    values: {},
                    fields: [
                        {   name: "pin",
                            caption: t('Pin'),
                            type: "pin",
                            required: true
                        },
                        {   name: "amount",
                            caption: t('Amount'),
                            type: "currency",
                            required: true
                        },
                        {   name: "remark", 
                            caption: t('Remark'),
                            type: 'text'
                        }
                    ]
                }
            }

            response.render('cart_cashin', vars);
        }
    );    
}

/**
 * receive cash from customer and redirect to cart list
 * 
 * @http-method             POST
 * @path                    <prefix>/cashout
 * @post-body                                       the customer pin and amount
 * 
 * @param {HttpRequest}     request 
 * @param {HttpResponse}    response 
 * @param {([function])}    next 
 */
function cashin_post(request, response, next) {
    var t = request.t;

    var pin=request.body.pin;
    var remark=request.body.remark;
    var amount=Number(request.body.amount);

    model.check_access(request.customer, 
        [ 'ESHOP_DESK', 'ESHOP_MANAGE'], 
        (err, permission) => {

            model.coop.get_customer_by_pin(pin, (err, customer) => {

                if (err) {
                    return response.send(
                        {   status: 'error', 
                            error: err.message,
                            stack: err.stack
                        }
                    );
                }
                if (customer==undefined) {
                    response.send(
                        {   status: 'failure', 
                            redirect: 'bad customer pin' 
                        }
                    );
                }

                model.coop.add_to_journal({
                    customer: customer.id,
                    amount: amount,
                    vat: 0,
                    type: 'cash deposit in shop',
                    remark: remark
                }, (er) => {

                    if (err) {
                        return response.send(
                            {   status: 'error', 
                                error: err.message,
                                stack: err.stack
                            }
                        );
                    }

                    model.coop.add_to_journal({
                            customer: 'emma-shop',
                            amount: -amount,
                            vat: 0,
                            type: 'cash deposit in shop',
                            remark: remark 
                        }, (err) => {

                            if (err) {
                                return response.send(
                                    {   status: 'error', 
                                        error: err.message,
                                        stack: err.stack
                                    }
                                );
                            }

                            model.shop.add_to_journal({
                                amount: amount,
                                vat: 0,
                                type: 'cash deposit in shop'
                            }, (err) => {
                                response.send(
                                    {   status: 'success', 
                                        redirect: l('list') 
                                    }
                                );                                    
                            });
                        }
                    );
                }
            );
        });
    });
}


