const express = require('express');
const model = require('../model');
const Url = require('url'); 

const uuid=require('uuid');


/**
 * install all routes
 */
function init_routes() {
    router.get('/login', login);
    router.post('/login', login_post);
    router.post('/login-check', login_check);
    router.use('/logout', logout);

    router.use('/customer',show_customers);
    router.use('/customers',get_customers);

//    router.use('/customer/new',create_customer);
//    router.use('/customer/:custid',edit_customer);

    router.use('/', admin_main);
    router.use('/overview', admin_main);
    
}

var routes = [
    [ 'login', login, login_post ],
    [ 'logout', logout ],
    [ 'login-check', login_check ],
    [ 'customer', [
        [ ':uid', [
        ]],
//        [ 'new', create_customer, create_customer_post ],
//        [ '*', show_customers ]
    ] ]
];

var router=express.Router();
init_routes();

module.exports = router;

/**
 * build a link, prefixed by module prefix
 * 
 * @param                   arguments       the path components
 */
function l() {
    var links=Array.from(arguments).join('/');
    return '/admin/' + links;
}

function login(request, response, next) {
    var t = request.t;

    var payload = request.query.payload || {};

    if (typeof payload.backlink == 'undefined') {
        var url = Url.parse(request.headers.referer || '/index');
        payload.backlink=url.path;
    }

    var vars = {
        lang: request.lang,
        theme: request.theme,
        is_admin: request.is_admin,
        is_login_page: true,
        title: t('Admin Login'),
        error_message: '',
        buttons: [
            {   title: t('Back'),
                icon: 'back',
                description: t('Go Back'),
                action: payload.backlink
            },
            {},
            {},
            {},
            {   title: t('Login'),
                icon: 'cart',
                description: t('Login into the System'),
                action: 'post-form'
            }
        ],
        form: {
            action: '/admin/login',
            values: payload,
            fields: [
                {
                    name: 'backlink',
                    type: 'const'
                },
                {
                    name: 'pin',
                    type: 'keypad-pin',
                    caption: 'Pin'
                }
            ]
        }
    };

    response.render('admin_login', vars);    
}

function login_post(request, response, next) {
    var pin=request.body.pin;

    model.coop.get_customer_by_pin(pin, (err, customer) => {
        if (err) {
            response.send(
                {   status: 'error', 
                    error: err.message,
                    stack: err.stack
                }
            );

        } else if (customer == undefined) {
            response.send(
                {   status: 'failure', 
                    message: 'bad pin'
                }
            );

        } else {
            response.cookie('adminpin', pin);
            response.send(
                {   status: 'success', 
                    redirect: request.body.backlink || '/index'
                }
            );
        }
    });
}

function login_check(request, response, next) {
    var pin=request.body.pin;

    model.coop.get_customer_by_pin(pin, (err, customer) => {
        if (err) {
            response.send(
                {   status: 'error', 
                    error: err.message,
                    stack: err.stack
                }
            );

        } else if (customer == undefined) {
            response.send(
                {   status: 'failure', 
                    message: 'bad pin'
                }
            );

        } else {
            response.send(
                {   status: 'success', 
                    message: 'pin ok',
                    customer: customer
                }
            );
        }
    });
}

function logout(request, response) {
    var t = request.t;
    response.cookie('adminpin', '');
    response.redirect(request.headers.referer);
}

function admin_main(request, response, next) {
    var t = request.t;

    model.check_access(request.customer, 
        [ 'EPOS_ADMIN'], 
        (err, permission) => {
            if (err) return next(new Error(err));

            var vars = {
                title: t('Administration'),
                buttons: [
                    {   title: t('Back'),
                        icon: 'back',
                        description: 'Back to Honme Screen',
                        action: '/index'
                    },
                    {   
                    },
                    {   
                    },
                    {   title: t('Sales'),
                        icon: 'order',
                        description: t('Show Sales'),
                        action: l('sales')
                    },
                    {   title: t('Coop Member'),
                        icon: 'customer',
                        description: t('Manage Coop Member'),
                        action: l('customer')
                    }
                ]
            }

            response.render('admin_overview', vars);    
        }
    );
}

function show_customers(request, response, next) {
    var t = request.t;

    model.check_access(request.customer, 
        [ 'EPOS_ADMIN'], 
        (err, permission) => {
            if (err) return next(new Error(err));

            var vars = {
                title: t('Coop Member'),
                buttons: [
                    {   title: t('Back'),
                        icon: 'back',
                        description: 'Back to Admin Screen',
                        action: l('')
                    },
                    {   
                    },
                    {   
                    },
                    {                       },
                    {   title: t('New Member'),
                        icon: 'customer',
                        description: t('Add Coop Member'),
                        action: l('customer/add')
                    }
                ],
                table: {
                    datasource: l('customers'),
                    paging: 'static',
                    fields: [
                        {   name: 'id',
                            caption: t('Id'),
                            type: 'text',
                            width: '20%'
                    },
                        {   name: 'name',
                            caption: t('Name'),
                            type: 'text',
                            width: '80%'
                        },
                        {   name: 'balance',
                            caption: t('Balance'),
                            type: 'currency',
                            width: '20%'
                        }
                    ]
                }
            }

            response.render('table_view.pug', vars);    
        }
    );
}

function get_customers(request, response, next) {
    var condition = 'offset ?,? order by ' + request.body.sort + (request.body.srt_dir=='desc' ? 'desc':'asc');
    model.coop.fetch_customers(
        query, [ request.body.from, request.body.to ],
        (err, result) => {
            response.send()
        }
    );
}

function create_customer(request, response, next) {
}

function edit_customer(request, response, next) {
}


