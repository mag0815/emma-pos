const express = require('express');
const i18n=require('../i18n');
const model=require('../model');
const uuid = require('uuid');

var router=express.Router();

var routes = [
    [ 'index', main_page ],
    [ 'release', release_notes ],

    [ 'message', [
        [ '*', message_list ],
        [ 'create', message_create, message_create_post ]
    ]],
    [ 'cart', require('./cart_route') ],
    [ 'cashdesk', require('./cashdesk_route') ],
    [ 'coop', require('./coop_route') ],
    [ 'admin',require('./admin_route') ],
    [ 'sql', require('./sql_route') ],

    [ 'form_test', form_test, form_test_post ],

    [ '/', main_page ],
];

router.install_routes(routes);

/* old uses()
router.use('/cart', require('./cart_route'));
router.use('/admin', require('./admin_route'));
router.use('/sql', require('./sql_route'));

router.get('/', main_page);
router.get('/index', main_page);
router.get('/release', release_notes);

router.get('/messages', message_list);
router.get('/message/create', message_create);
router.post('/message/create', message_create_post);
*/

function main_page(request, response, next) {
    var t = request.t;

    model.check_access(request.customer, 
        [ 'EPOS_ADMIN', 'ESHOP_MANAGE', 'CASHDESK_VIEW', 'CASHDESK_MANAGE'], 
        (err, permission) => {


        model.coop.get_messages(0, 5, (err, result) => {

            var vars = {
                title: t('Uncle Emma'),
                buttons: [
                    {   title: t('Message'),
                        icon: 'message',
                        description: t('Leave a Message'),
                        action: '/message/create'
                    },
                    {},
                    {},
                    {
                    },
                    {   title: t('Emma Shop'),
                        icon: 'cart',
                        description: t('Go to the Shop'),
                        action: '/cart/list'
                    }
                ],
                messages: result,
                menu: [
                    { title: 'sql',
                      action: '/sql/',
                      icon: '' }
                ]
            };

                vars.buttons[1] = {
                    title: t('Coop'),
                    icon: 'coop',
                    description: t('Manage the Coop'),
                    action: '/coop'
                };

                if (permission.CASHDESK_VIEW || permission.CASHDESK_MANAGE) {
                vars.buttons[2] = {
                    title: t('Cashdesk'),
                    icon: 'coop',
                    description: t('Manage the Cashdesk'),
                    action: '/cashdesk'
                };
            }

            response.render('mainpage', vars); 
        });
    });
}

function message_list(request, response, next) {
    var t = request.t;

    var count=request.query.count || 10;
    var from=request.query.from || 0;
    model.coop.get_messages(from, count, (err, result) => {
        response.render('message-list', {
            title: t('Uncle Emma'),
            buttons: [
                {   title: t('Back'),
                    icon: 'back',
                    description: t('Back to Home Screen'),
                    action: '/index'
                },
                {},
                {},
                {},
                {}
            ],
            messages: result,
            from: Number(from),
            count: Number(count)
        });
    
    });
}

function message_create(request, response, next) {
    var t = request.t;

    vars = {
        buttons: [
            {   title: t('Back'),
                icon: 'back',
                description: t('Go Back'),
                action: '/index'
            },
            {},
            {},
            {},
            {   title: t('Send'),
                icon: 'save',
                description: t('Leave Message'),
                action: 'post-form'
            }
        ],
        form: {
            id: 'create-message',
            action: "create",
            values: { id: uuid() },
            fields: [
                {   name: 'id',
                    type: 'const'
                },
                {   name: 'message',
                    type: 'textarea',
                    caption: t('Message'),
                    required: true
                }
            ]
        }
    };

    response.render('message-form', vars);
}

function message_create_post(request, response, next) {
    var t = request.t;

    model.coop.create_message(request.body, (err) => {
        if (err) {
            response.send(
                {   status: 'error', 
                    error: err.message,
                    stack: err.stack
                }
            );

        } else {
            response.send(
                {   status: 'success', 
                    redirect: '/index'
                }
            );
        }
    });
}

function release_notes(request, response, next) {
    var t = request.t;

    vars = {
        title: t('Release Notes'),
        buttons: [
            {   title: t('Back'),
                icon: 'back',
                description: t('Go Back'),
                action: '/index'
            },
            {},
            {},
            {},
            {}
        ]
    };

    response.render('releasenotes', vars);
}

function form_test(request, response, next) {
    var t = request.t;

    var vars = {
        title: t('Uncle Emma'),
        buttons: [
            {   title: t('Back'),
                icon: 'back',
                description: t('Back to Main Screen'),
                action: '/index'
            },
            {},
            {},
            {},
            {   title: t('Submit'),
                icon: 'save',
                description: t('Leave a Message'),
                action: '/form_test?'
            }
        ],

        form: {
            id: 'form_test',
            action: '/form_test_view?',
            values: {},
            title: 'Form Test',
            fields: [
                {   name: 'hidden',
                    type: 'constant'
                },
                {   type: 'description',
                    content: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'
                },
                {   name: 'textfield',
                    type: 'string',
                    caption: 'Textfield',
                    default: '',
                    maxlength: 100,
                    required: false
                },
                {   name: 'textfield',
                    type: 'string',
                    caption: 'Textfield',
                    default: '',
                    maxlength: 100,
                    required: false
                },
                {   name: 'textfield2',
                    type: 'string',
                    caption: 'Another Textfield',
                    default: '',
                    maxlength: 100,
                    required: false,
                    display: 'short'
                },
                {   name: 'select',
                    type: 'select',
                    caption: 'Selection',
                    default: '',
                    required: false,
                    values: [
                        { value: '1', caption: 'Item 1' },
                        { value: '2', caption: 'Item 2' },
                        { value: '3', caption: 'Item 3' },
                        { value: '4', caption: 'Item 4' },
                        { value: '5', caption: 'Item 5' },
                        { value: '6', caption: 'Item 6' }
                    ]
                },
                {   name: 'autocomplete',
                    type: 'autocomplete',
                    caption: 'Complete',
                    default: '',
                    required: false,
                    values: [
                        { value: '1', caption: 'Item 1' },
                        { value: '2', caption: 'Item 2' },
                        { value: '3', caption: 'Item 3' },
                        { value: '4', caption: 'Item 4' },
                        { value: '5', caption: 'Item 5' },
                        { value: '6', caption: 'Item 6' }
                    ]
                },
                {   name: 'texarea',
                    type: 'text',
                    caption: 'Textarea',
                    maxlength: 1000,
                    required: false
                },
                {   name: 'checkbox',
                    type: 'checkbox',
                    caption: 'some check option',
                    required: false
                },
                {   name: 'hidden',
                    type: 'constant'
                },
                {   type: 'description',
                    content: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'
                },
                {   name: 'textfield',
                    type: 'string',
                    caption: 'Textfield',
                    default: '',
                    maxlength: 100,
                    required: false
                },
                {   name: 'textfield',
                    type: 'string',
                    caption: 'Textfield',
                    default: '',
                    maxlength: 100,
                    required: false
                },
                {   name: 'textfield2',
                    type: 'string',
                    caption: 'Another Textfield',
                    default: '',
                    maxlength: 100,
                    required: false,
                    display: 'short'
                },
                {   name: 'select',
                    type: 'select',
                    caption: 'Selection',
                    default: '',
                    required: false,
                    values: [
                        { value: '1', caption: 'Item 1' },
                        { value: '2', caption: 'Item 2' },
                        { value: '3', caption: 'Item 3' },
                        { value: '4', caption: 'Item 4' },
                        { value: '5', caption: 'Item 5' },
                        { value: '6', caption: 'Item 6' }
                    ]
                },
                {   name: 'autocomplete',
                    type: 'autocomplete',
                    caption: 'Complete',
                    default: '',
                    required: false,
                    values: [
                        { value: '1', caption: 'Item 1' },
                        { value: '2', caption: 'Item 2' },
                        { value: '3', caption: 'Item 3' },
                        { value: '4', caption: 'Item 4' },
                        { value: '5', caption: 'Item 5' },
                        { value: '6', caption: 'Item 6' }
                    ]
                },
                {   name: 'texarea',
                    type: 'text',
                    caption: 'Textarea',
                    maxlength: 1000,
                    required: false
                },
                {   name: 'checkbox',
                    type: 'checkbox',
                    caption: 'some check option',
                    required: false
                }                
            ]
        }
    };

    response.render('form_test', vars); 
}

function form_test_post(request, response, next) {
    var t = request.t;
}

module.exports=router;
















