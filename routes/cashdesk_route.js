const express = require('express');
const uuid=require('uuid');

const model = require('../model');
const coop = require('../model/coop');

var router=express.Router();

var routes =
    [   [ '*', cashdesk_overview ],
        [ 'deposit', cashdesk_deposit, cashdesk_deposit_post ],
        [ 'withdraw', cashdesk_withdraw, cashdesk_withdraw_post ],
        [ 'cashin', cashdesk_cashin, cashdesk_cashin_post ],
        [ 'cashout', cashdesk_cashout, cashdesk_cashout_post ]
    ];


router.install_routes(routes);

module.exports=router;

/**
 * build a link, prefixed by module prefix
 * 
 * @param                   arguments       the path components
 */
function l() {
    var links=Array.from(arguments).join('/');
    return '/cashdesk/' + links;
}


/**
 * Cashdesk Overview Page
 * 
 * @param {*} request 
 * @param {*} response 
 * @param {*} next 
 */
function cashdesk_overview(request, response, next) {
    var t = request.t;

    model.check_access(request.customer, 
        [ 'CASHDESK_VIEW', 'CASHDESK_MANAGE' ], 
        (err, permission) => {

            if (err)
                return next(err);
            if (!permission.CASHDESK_VIEW && !permission.CASHDESK_MANAGE) {
                return response.redirect('/');
            }

            let buttons = [
                {   title: t('Back'),
                    icon: 'back',
                    description: t('Back to Home'),
                    action: '/'
                },
                { },
                { },
                { },
                { }
            ];

            if (permission.CASHDESK_MANAGE) {
                buttons[1] = {   title: t('Cash-Out'),
                    icon: 'cashout',
                    description: t(''),
                    action: l('cashout')
                };
                buttons[2] = {   title: t('Cash-In'),
                    icon: 'cashin',
                    description: t(''),
                    action: l('cashin')
                };
                buttons[3] = {   title: t('To Coop'),
                    icon: 'cashout',
                    description: t(''),
                    action: l('withdraw')
                };
                buttons[4] = {   title: t('From Desk'),
                    icon: 'cashin',
                    description: t(''),
                    action: l('deposit')
                };34
            }

            model.cashdesk.overview(
                (err, result) => {
                    if (err)
                        return next(err);

                    var vars = {
                        title: t('Cashdesk'),
                        buttons: buttons,
                        balance: result.balance,
                        table: {
                            columns: [
                                {   name: 'date', caption: t('Date'), width: '20%' },
                                {   name: 'amount', caption: t('Amount'), width: '10%' },
                                {   name: 'customer', caption: t('Customer'), width: '10%' }, 
                                {   name: 'remark', caption: t('Remark'), width: '60%' }
                            ],
                            values: result.values
                        }
                    }

                    response.render('cashdesk_overview', vars);    
                }
            );
        }
    );
}

function cashdesk_deposit(request, response, next) {
    var t = request.t;

    model.check_access(request.customer, 
        [ 'CASHDESK_MANAGE'], 
        (err, permission) => {
            if (err) 
                return next(new Error(err));
            if (!permission.CASHDESK_MANAGE)   
                return response.redirect(l());
            var vars = {
                title: t('Cashdesk: Deposit from Desk'),
                buttons: [
                    {   title: t('Back'),
                        icon: 'back',
                        action: l()
                    },
                    {},
                    {},
                    {
                    },
                    {   title: t('Deposit'),
                        icon: 'cashin',
                        description: 'Payout Cash to Member',
                        disabled: true,
                        action: 'post-form'
                    }
                ],
                form: {
                    id: 'deposit',
                    action: l('deposit'),
                    values: {},
                    fields: [
                        {   name: "amount",
                            caption: t('Amount'),
                            type: "currency",
                            required: true
                        },
                        {   name: "remark", 
                            caption: t('Remark'),
                            type: 'text'
                        }
                    ]
                }
            }

            response.render('cashdesk_deposit', vars);
        }
    ); 
}

function cashdesk_deposit_post(request, response, next) {
    var t = request.t;

    var amount=request.body.amount;
    var remark=request.body.remark;

    model.check_access(request.customer, 
        [ 'CASHDESK_MANAGE' ], 
        (err, permission) => {

            if (!permission.CASHDESK_MANAGE) 
                return response.send(
                    {   status: 'error',
                        error: 'permission denied',
                        stack: 'permission denied in new_category_post()',
                    }
                );

            model.cashdesk.deposit(amount, remark, (err) => {
                if (err) {
                    response.send(
                        {   status: 'error', 
                            error: err.message,
                            stack: err.stack
                        }
                    );
                } else {
                    response.send(
                        {   status: 'success', 
                            redirect: l('') 
                        }
                    );
                }
            });
        }
    );
}

function cashdesk_withdraw(request, response, next) {
    var t = request.t;

    model.check_access(request.customer, 
        [ 'CASHDESK_MANAGE'], 
        (err, permission) => {
            if (err) 
                return next(new Error(err));
            if (!permission.CASHDESK_MANAGE)   
                return response.redirect(l());
            var vars = {
                title: t('Cashdesk: Withdraw to Coop'),
                buttons: [
                    {   title: t('Back'),
                        icon: 'back',
                        action: l()
                    },
                    {},
                    {},
                    {
                    },
                    {   title: t('Withdraw'),
                        icon: 'cashout',
                        description: 'Withdraw Cash to Coop',
                        disabled: true,
                        action: 'post-form'
                    }
                ],
                form: {
                    id: 'withdraw',
                    action: l('withdraw'),
                    values: {},
                    fields: [
                        {   name: "amount",
                            caption: t('Amount'),
                            type: "currency",
                            required: true
                        },
                        {   name: "remark", 
                            caption: t('Remark'),
                            type: 'text'
                        }
                    ]
                }
            }

            response.render('cashdesk_withdraw', vars);
        }
    ); 
}

function cashdesk_withdraw_post(request, response, next) {
    var t = request.t;

    var amount=request.body.amount;
    var remark=request.body.remark;

    model.check_access(request.customer, 
        [ 'CASHDESK_MANAGE' ], 
        (err, permission) => {

            if (!permission.CASHDESK_MANAGE) 
                return response.send(
                    {   status: 'error',
                        error: 'permission denied',
                        stack: 'permission denied in new_category_post()',
                    }
                );

            model.cashdesk.withdraw(amount, remark, (err) => {
                if (err) {
                    response.send(
                        {   status: 'error', 
                            error: err.message,
                            stack: err.stack
                        }
                    );
                } else {
                    response.send(
                        {   status: 'success', 
                            redirect: l('') 
                        }
                    );
                }
            });
        }
    );
}

/**
 * Customer Cash-In
 * 
 * @param {*} request 
 * @param {*} response 
 * @param {*} next 
 */
function cashdesk_cashin(request, response, next) {
    var t = request.t;

    model.check_access(request.customer, 
        [ 'CASHDESK_MANAGE'], 
        (err, permission) => {
            if (err) 
                return next(new Error(err));
            if (!permission.CASHDESK_MANAGE)   
                return response.redirect(l());

            var vars = {
                title: t('Cashdesk: Cash-In from Coop Member'),
                buttons: [
                    {   title: t('Back'),
                        icon: 'back',
                        action: l()
                    },
                    {},
                    {},
                    {
                    },
                    {   title: t('Cash-In'),
                        icon: 'cashin',
                        description: 'Receive Cash from Member',
                        disabled: true,
                        action: 'post-form'
                    }
                ],
                form: {
                    id: 'cashin',
                    action: l('cashin'),
                    values: {},
                    fields: [
                        {   name: "pin",
                            caption: t('Pin'),
                            type: "pin",
                            required: true
                        },
                        {   name: "amount",
                            caption: t('Amount'),
                            type: "currency",
                            required: true
                        },
                        {   name: "remark", 
                            caption: t('Remark'),
                            type: 'text'
                        }
                    ]
                }
            }

            response.render('cashdesk_cashin', vars);
        }
    ); 
}

function cashdesk_cashin_post(request, response, next) {
    //TODO:
}

function cashdesk_cashout(request, response, next) {
    var t = request.t;

    model.check_access(request.customer, 
        [ 'CASHDESK_MANAGE'], 
        (err, permission) => {
            if (err) 
                return next(new Error(err));
            if (!permission.CASHDESK_MANAGE)   
                return response.redirect(l());
            var vars = {
                title: t('Cashdesk: Cash-Out to Coop Member'),
                buttons: [
                    {   title: t('Back'),
                        icon: 'back',
                        action: l()
                    },
                    {},
                    {},
                    {
                    },
                    {   title: t('Cash-Out'),
                        icon: 'cashout',
                        description: 'Payout Cash to Member',
                        disabled: true,
                        action: 'post-form'
                    }
                ],
                form: {
                    id: 'cashout',
                    action: l('cashout'),
                    values: {},
                    fields: [
                        {   name: "pin",
                            caption: t('Pin'),
                            type: "pin",
                            required: true
                        },
                        {   name: "amount",
                            caption: t('Amount'),
                            type: "currency",
                            required: true
                        },
                        {   name: "remark", 
                            caption: t('Remark'),
                            type: 'text'
                        }
                    ]
                }
            }

            response.render('cashdesk_cashout', vars);
        }
    ); 
}

function cashdesk_cashout_post(request, response, next) {
    //TODO:
}

