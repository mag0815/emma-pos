const fs = require('fs');
const path = require('path');

const execSync = require('./node-exec-sync');

class Template {

    constructor(xls, template_file, target_file) {
        this.xls = xls;
        this.path = template_file;
        
        this.read_template(template_file);
        this.open_target(target_file);
    }

    read_template(template_file) {
        // console.log('reading template '+template_file)

        let src = fs.readFileSync(template_file).toString();
        let lines = src.split('\n');
        let section = '';
        let content = '';
        this.sections = {};
        for (var n=0; n<lines.length; ++n) {
            let line=lines[n];
            let s = line.match(/\[\[([a-zA-Z0-9_$-]*)\]\]/);
            if (s) {
                // console.log('found section '+section);
                this.sections[section] = content;
                section = s[1];
                content = '';
            } else {
                content += line + '\n';
            }
        }
        // console.log('found section '+section);
        this.sections[section] = content;
    }

    open_target(target_file) {
        this.target = fs.openSync(target_file, 'w');
    }

    finish() {
        fs.closeSync(this.target);
    }

    emit(template, args) {
        if (this.sections[template]) {
            // console.log('emit [['+template+']]', args)
            let t = this.sections[template];
            let str = t.replace(/{{([a-zA-Z0-9_$-]*)}}/g, (regex,key) => {
                var v = args[key];
                //if (typeof v == 'string') {
                //    v = this.xls.atom(v);
                //}
                return v;
            });    
            fs.writeSync(this.target, str.replace('}}', ''));
            return this;
        } else
            throw new Error("non-defined section ('" + template + "' in template file " + this.path);
    }
}

function xsl_date(js_date) {
    if (typeof js_date == 'string' || typeof js_date == 'number') {
        js_date = new Date(js_date);
    }
    const msh = 60*60*1000;
    const msd = msh*24;
    const msy = msd*365;
    const offset = 70*msy-msd-msh
    let d = (js_date.valueOff()+offset) / msd;
    return d;
}

function js_date(xls_date) {
    const msh = 60*60*1000;
    const msd = msh*24;
    const msy = msd*365;
    const offset = 70*msy-msd-msh
    let d = js_date*msd - offset;
    return new Date(d);
}

function unescape(str) {
    return str.replace( /&amp;/g, '&')
              .replace( /&gt;/g, '>')
              .replace( /&lt;/g, '<')
}

function escape(str) {
    return str.replace( /&/g, '&amp;')
              .replace( />/g, '&gt;')
              .replace( /</g, '&lt;')
}

class Xlsx {
    constructor(template, target) {

        this.target = target;
        this.layout = template + '/';
        this.temp = target + '-temp/';

        execSync('rm -Rf '+this.temp);

        fs.mkdirSync(this.temp, { recursive: true });
        fs.mkdirSync(this.temp+'_rels', { recursive: true });
        fs.mkdirSync(this.temp+'docProps', { recursive: true });
        fs.mkdirSync(this.temp+'xl', { recursive: true });
        fs.mkdirSync(this.temp+'xl/_rels', { recursive: true });
        fs.mkdirSync(this.temp+'xl/worksheets', { recursive: true });

        this.content_types = this.template('[Content_Types].xml').emit('HEADER');

        this.copy_static('_rels/.rels');

        this.copy_static('docProps/app.xml');
        this.copy_static('docProps/core.xml');

        this.string_resources = this.template('xl/sharedStrings.xml');
        this.read_strings();

        this.copy_static('xl/styles.xml');
        this.workbook = this.template('xl/workbook.xml').emit('HEADER');
        this.workook_rels = this.template('xl/_rels/workbook.xml.rels').emit('HEADER');

        this.last_resid = 1;
        this.last_sheetid = 0;
    }

    finish() {
        this.content_types.emit('FOOTER').finish();
        this.write_strings();
        this.string_resources.emit('FOOTER').finish();
        this.workbook.emit('FOOTER').finish();
        this.workook_rels.emit('FOOTER', { resid: 'rId'+(++this.last_resid) }).finish();

        let cmd = 'cd "' + this.temp + '"; zip -r "../' + path.basename(this.target) + '" *';
        execSync(cmd);
        cmd = 'rm -Rf ' + this.temp;
        execSync(cmd);
    }

    write_strings() {
        this.string_resources.emit('HEADER', { count: this.strings.length });
        for (let n=0; n<this.strings.length; ++n) {
            this.string_resources.emit('STRING', {
                n: n,
                string: this.strings[n]
            });
        }

    }

    read_strings() {
        this.strings = [];
        this.string_map = {};
        let strings = this.string_resources.sections['CONTENT'];
        strings.replace( />([^<]*)<[/]t/g, (a,str) => {
            str = unescape(str);
            this.string_map[str] = this.strings.length;
            this.strings.push(str);
        })
    }

    atom(str) {
        str = str.replace(/[&]/g, '&amp;')
            .replace(/[<]/g, '&lt;')
            .replace(/[>]/g, '&gt;');

        if (typeof this.string_map[str] === 'undefined') {
            let n = this.strings.length;
            this.string_map[str] = n;
            this.strings.push(str);
            return n;
        } else return this.string_map[str];
    }

    date(js_date) {
        if (typeof js_date == 'string' || typeof js_date == 'number') {
            js_date = new Date(js_date);
        }

        const one_day = 86400000;
        const epoch = 2209161600000;
        const timezone = js_date.getTimezoneOffset()*60*1000;
        let v = (js_date.valueOf()+epoch-timezone) / one_day;

        return v;
    }
    
    copy_static(template, target) {
        target = target || template;
        let t = new Template(this, this.layout + template, this.temp + target);
        t.emit('').finish();
    }

    template(template, target) {
        target = target || template;

        let t = new Template(this, this.layout + template, this.temp + target);
        return t;
    }

    worksheet(template, title) {
        let resid = 'rId' + (++this.last_resid);
        let sheetid = ++this.last_sheetid;
        let filename = "sheet" + sheetid + '.xml';

        this.content_types.emit('WORKSHEET', {filename: filename } );
        this.workook_rels.emit('WORKSHEET', {filename: filename, resid: resid })
        this.workbook.emit('WORKSHEET', { title: title, resid: resid, sheetid: sheetid } );

        return this.template('xl/worksheets/'+template+'.xml', 'xl/worksheets/'+filename);
    }
        
}

module.exports = Xlsx;
