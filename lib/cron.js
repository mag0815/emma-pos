
const util = require('util');
const fs = require('fs');

var start_date;
var hourly_jobs = {};
var dayly_jobs = {};
var weekly_jobs = {};
var monthly_jobs = {};
var at_jobs = {};
var on_jobs = {};

const config = require('../config.json');

const INTERVAL = config.cron.interval;
const LOCKDIR = __dirname + '/../' + config.cron.lockdir;
const LOCKFILE = LOCKDIR + '/cron.locked';

//const SPEEDUP = 60*60*100;
const SPEEDUP = 1;

function unlink(path) {
    try {
        fs.unlinkSync(path);

    } catch(e) {}
}

function exists(path) {
    try {
        fs.existsSync(path);

    } catch(e) {}
}

var debug_timestamps = [];

function touch(path) {
    if (SPEEDUP!=1) {
        debug_timestamps[path] = now();
        return 
    }
    try {
        fs.unlinkSync(path);
        fs.writeFileSync(path, '');
    } catch(e) {}
}

function ctime(path) {
    if (SPEEDUP!=1) {
        return debug_timestamps[path];        
    }
    try {
        let s=fs.statSync(path);
        return s.ctime();
    } catch(e) {
        return null;
    }
}

function tm_tag(d) {
    let s=d.toString();
    return  s.substr(0,4) + '-' +
            s.substr(4,2) + '-' +
            s.substr(6,2) + '/' +
            s.substr(8,2) + ' ' +
            s.substr(10,2) + ' ' +
            s.substr(12,2) + ':00'
}

function log() {
    console.log( util.format.apply(this, arguments));
}

function now() {
    let t = new Date();
    t = t - start_date; // - 60*60*1000;
    t = new Date(start_date.valueOf() + t*SPEEDUP);
    return t;
}

function hourly(name, job) {
    hourly_jobs[name] =job;
    unlink(LOCKDIR+'/'+name+'.lock');
}
function dayly(name, job) {
    dayly_jobs[name] =job;
    unlink(LOCKDIR+'/'+name+'.lock');
}
function weekly(name, job) {
    weekly_jobs[name] =job;
    unlink(LOCKDIR+'/'+name+'.lock');
}
function monthly(name, job) {
    monthly_jobs[name] =job;
    unlink(LOCKDIR+'/'+name+'.lock');
}
function on(name, day, job) {
    on_jobs[name] = { v: day, job: job };
    unlink(LOCKDIR+'/'+name+'.lock');
}
function at(name, hour, job) {
    at_jobs[name] = { v: hour, job: job };
    unlink(LOCKDIR+'/'+name+'.lock');
}


function run_jobs(queue) {
    var n, jobs = [];
    for (n in queue) jobs.push(n);
    n=0;
    doit();

    function doit(err) {
        let j = jobs[n];
        if (arguments.length>0) {
            // log('result: %s() => %s', j, err);
            unlink(LOCKDIR + '/' + j + '.lock');
            if (!err) {
                touch(LOCKDIR + '/' + j + '.stamp');
            }
            n++;
        }
        if (n<jobs.length) {
            j = jobs[n];
            //log ('running %s', j);
            touch(LOCKDIR + '/' + j + '.lock');
            (queue[j])(doit);
        }
    }
}

Date.prototype.getWeek = function() {
    // Copy date so don't modify original
    let d = new Date(Date.UTC(this.getFullYear(), this.getMonth(), this.getDate()));
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
    // Get first day of year
    var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
    // Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
    // Return array of year and week number
    return weekNo;
}

function datestamp(date) {
    let d =  date.getFullYear()  * 10000000000
           + (date.getMonth()+1) * 100000000
           + date.getWeek()      * 1000000
           + (date.getDay()+7)%8 * 10000
           + date.getDate()      * 100
           + date.getHours()     * 1;
    return d;
}

function mdiv(date, mod) {
    if (typeof date != 'number' )
        date = datestamp(date);
    return date - date%mod;
}

function check_queue(queue, jobs, mod) {
    let tm = datestamp(now());

    for (let j in jobs) {
        if (!exists(LOCKDIR + '/' + j + '.lock')) {
            let ctm = ctime(LOCKDIR + '/' + j + '.stamp');
            if (ctm) {
                if ( mdiv(ctm, mod) < mdiv(tm, mod) ) {
                    //log('%s queuing job %s  (%s < %s)', tm_tag(tm), j, tm_tag(mdiv(ctm, mod)), tm_tag(mdiv(tm, mod)));
                    // log('%s < %s', tm_tag(ctm), tm_tag(tm) );
                    queue[j] = jobs[j];
                }
            } else {
                //log('%s queuing job %s', tm_tag(tm), j);
                queue[j] = jobs[j];
            }
        }
    }
}

function check_queue2(queue, jobs, mod, mul) {
    let tm = mdiv(datestamp(now()), mod);

    for (let j in jobs) {
        if (!fs.existsSync(LOCKDIR + '/' + j + '.lock')) {
            if (fs.existsSync(LOCKDIR + '/' + j + '.stamp')) {
                let s = fs.statSync(LOCKDIR + '/' + j + '.stamp');
                d = datestamp(s.ctime);
                d = mdiv(d, mod) + v*mul;
                if ( d < tm ) {
                    queue[j] = jobs[j];
                }
            } else {
                queue[j] = jobs[j];
            }
        }
    }

}

function timeout() {
    if (!fs.existsSync(LOCKFILE)) {
        //log('%s checking jobs', tm_tag(datestamp(now())) );
        let queue = {}

        touch(LOCKFILE);

        // check interval queues
        check_queue(queue, hourly_jobs,  1);
        check_queue(queue, dayly_jobs,   100);
        check_queue(queue, weekly_jobs,  1000000);
        check_queue(queue, monthly_jobs, 100000000);

        // check timed queues
        //check_queue2(queue, at_jobs, 1, 24);
        //check_queue2(queue, on_jobs, 10000, 700);

        run_jobs(queue);    
        unlink(LOCKFILE)
    }
}

unlink(LOCKFILE);
start_date = new Date();
setInterval(timeout, INTERVAL / SPEEDUP);
//log('cronjob started.');

module.exports = {
    hourly: hourly,
    dayly: dayly,
    weekly: weekly,
    monthly: monthly,
    on: on,
    at: at
};



if (false) {
    hourly('hourly', (cb) => { cb(null);} );
    dayly('dayly', (cb) => { cb(null);} );
    weekly('weekly', (cb) => { cb(null);} );
    monthly('monthly', (cb) => { cb(null);} );
}