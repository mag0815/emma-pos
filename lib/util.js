
JSON.old_parse=JSON.parse;
JSON.parse = function(str) {
    return JSON.old_parse(str, function(key, val) {
        if (val!==null && typeof val == 'object') {
            if (val['$date']) {
                var julian=val['$date'];
                return new Date( (julian-2440587.5)*86400000 );
            }
        }
        return val;
    });
}

Date.prototype.toJSON = function() {
    var julian = this.getTime()/86400000 + 2440587.5;
    return {"$date":  julian };
};

class Util {

    /** extend Object
     * 
     * stolen from jQuery..
     */
    static extend() {
        var src, copyIsArray, copy, name, options, clone,
            target = arguments[0] || {},
            i = 1,
            length = arguments.length,
            deep = false;
    
        // Handle a deep copy situation
        if ( typeof target === "boolean" ) {
            deep = target;
    
            // skip the boolean and the target
            target = arguments[ i ] || {};
            i++;
        }
    
        // Handle case when target is a string or something (possible in deep copy)
        if ( typeof target !== "object" && !jQuery.isFunction(target) ) {
            target = {};
        }
    
        // extend jQuery itself if only one argument is passed
        if ( i === length ) {
            target = this;
            i--;
        }
    
        for ( ; i < length; i++ ) {
            // Only deal with non-null/undefined values
            if ( (options = arguments[ i ]) != null ) {
                // Extend the base object
                for ( name in options ) {
                    src = target[ name ];
                    copy = options[ name ];
    
                    // Prevent never-ending loop
                    if ( target === copy ) {
                        continue;
                    }
    
                    // Recurse if we're merging plain objects or arrays
                    if ( deep && copy && ( typeof copy == 'object' || (copyIsArray = Array.isArray(copy)) ) ) {
                        if ( Array.isArray(copy) ) {
                            clone = src && Array.isArray(src) ? src : [];
    
                        } else {
                            clone = src;
                        }
    
                        // Never move original objects, clone them
                        target[ name ] = Util.extend( deep, clone, copy );
    
                    // Don't bring in undefined values
                    } else if ( copy !== undefined ) {
                        target[ name ] = copy;
                    }
                }
            }
        }
    
        // Return the modified object
        return target;
    };
    
}

module.exports = Util;
