const express = require('express');

/** 
  * short-hand to install routes; works for apps and routers
  * format: routes = [
  *                    [ 'mountpoint_fragment', get_and_post_handler ],
  *                    [ 'mountpoint_fragment', get_handler, post_handler ],
  *                    [ 'mountpint_fragment' nested_routes_array ],
  *                    [ '*', catch_all_handler ]
  *                  ]
  */
 express.application.install_routes = express.Router.install_routes = 
 function(prefix, routes) {


    function log(meth, path, func) {
        //console.log(meth, path, func)
    }

     if (arguments.length==1) {
         routes=prefix;
         prefix='';
     }
     var default_route=false;
     for (var r in routes) {
         var route=routes[r];
         if (typeof route != 'object') {
             //console.log('=>', route); 
             return;
         }
         
         if (route[0] == '*') {
             default_route=route;
             continue;
         }

         if (route.length==2) {
             if (typeof route[1]=='function') {
                 log('use', prefix+'/'+route[0], (route[1].name))
                 this.use(prefix+'/'+route[0], route[1]);
             } else {
                 this.install_routes(prefix+'/'+route[0], route[1]);
             }
         } else {
             log('get', prefix+'/'+route[0],(route[1].name))
             log('post', prefix+'/'+route[0],(route[2].name))
             if (route[1]) this.get(prefix+'/'+route[0], route[1]);
             if (route[2]) this.post(prefix+'/'+route[0], route[2]);
         }
     }
     if (default_route) {
         route=default_route;
         if (route.length==2) {
             if (typeof route[1]=='function') {
                log('use', prefix, (route[1].name))
                this.use(prefix, route[1]);
             } else {
                 this.install_routes(prefix+'/'+route[0], route[1]);
             }
         } else {
             log('get', prefix, (route[1].name))
             log('post', prefix, (route[2].name))

             if (route[1]) this.get(prefix, route[1]);
             if (route[2]) this.post(prefix, route[2]);
         }
     }
 };
