var fs = require('fs');
var pug = require('pug');

// Compile the template to a function string
var jsFunctionString = pug.compileFileClient('cart_view.pug', {name: "cart_view", compileDebug:false});

// Maybe you want to compile all of your templates to a templates.js file and serve it to the client
fs.writeFileSync("cart_view.js", jsFunctionString);
