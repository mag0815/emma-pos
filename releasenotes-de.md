
#### v0.0.5

Erweiterte Fassung:

- Bargeld-Ein/Auszahlungen im Shop
- Protokollierung der Bargeldentnahmen aus der Kasse

#### v0.0.3

Dies ist eine pre-beta Version des neuen Kassen-Systems, das heisst, 
vieles laeuft noch nciht so wie es soll, einiges ist unvollstaendig, etc.

Anregungen, Anmerkungen bitte gerne als Nachricht (der Knopf unten links) 
hinterlassen, oder direkt an Mario