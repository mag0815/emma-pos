= Emma-POS

A simple Point of Sale program for food coops.

== INSTALL

Emma-pos uses nodejs and sqlite. Make sure development libraries are installed, eg on a debianish system use
`bash
sudo apt install nodejs npm
sudo apt install libsqlite3 libsqlite3-dev
`

Then, just checkout the repo, and run
`bash
git clone https://gitlab.com/mag0815/emma-pos.git emma-pos
cd emma-pos && npm install
`

== DEVELOPMENT

For development one might use nodemon to monitor code changes during server runs.

`bash
npm install --dev
npm run-script monitor
`

== CONFIGURATION

Use `config.json` to configure the server.

- port: http-port
- tls: https section
  - enabled: true, when using tls
  - port: https-port
  - key: path to tls-key file in pem format
  - cert: path to tls-certificate file in pem format
- model:
  - driver: should be 'sqlite3' 
  - database: path to sqlite database file
- develelop:
  - compile_scss: when true, use on-the-fly translation of tls files

